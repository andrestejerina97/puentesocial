@extends('layouts.public')

@section('content')
    <div class="row">
        <br>		
        <div class="col-md-2 col-sm-2 col-lg-2">   
        </div>	 
        <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
                
        <br>
        <br>
        <h3><b>SOBRE PUBLICAR TUS CASOS</b></h3>
        <p>Podes publicar un caso o un proyecto propio o de tercero. Es importante que sepas que será evaluado antes de ponerlo on line. Los datos que consignes en cada uno deben ser reales, caso contrario no se publicará y eventualmente, bloquearemos tu cuenta.</p><br>
        <a href="{{route('case.create')}}" target="_blanck"> <button type="button" class="btn btn-success">ENVIAR UN CASO</button></a>
        <br>	
        <br>
        <a href="{{route('proyect.create')}}" target="_blanck"> <button type="button" class="btn btn-success">ENVIAR PROYECTO</button></a>
        <br>
        <br>
        <br>	
        <h4>SUBIR OPORTUNIDADES / TRABAJO</h4>	
        <p>Podes publicar algo que tengas para donar, o podes enviarnos una busqueda laboral.<br>
        Tu responsabilidad es la transparencia que necesita Puente Social.</p>	
        <br>
                            
        <a href="{{route('oportunity.create')}}" target="_blanck"> <button type="button" class="btn btn-success">OPORTUNIDAD / TRABAJO</button></a>					
        <br>
        <br>	
        <p>PUENTE SOCIAL ES UNA HERRAMIENTA QUE NECESITA DE MUCHA CONFIANZA PARA QUE FUNCIONE, Y VINCULE A LAS PERSONAS PARA QUE SE PRESTEN, AYUDA O UN INTERESANTE PROYECTO SOCIAL PUEDE SER APOYADO.</p>	
            
        </div>
        
        <div class="col-md-2 col-sm-2 col-lg-2">
             
        </div>	
        
        
        <br>               
        <br>	
   </div>	
    
    <br>               
        <br><br>               
        <br>
    <hr>  
@endsection