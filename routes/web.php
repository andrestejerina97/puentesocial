<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true,'--seed'=>true]);
});
*/

/*Route::get('/', function () {
    return view('welcome');
}); */


Route::get('/', 'HomeController@index')->name('index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/publicaciones', 'PublicationsController@index')->name('publication.index');


Route::middleware(['auth'])->group(function(){
  Route::get('/ver-publicacion/{id?}', 'PublicationsController@view')->name('publication.view');
  Route::post('/nuevo-me-gusta', 'PublicationsController@addLike')->name('public.add.like');
  Route::post('/nuevo-compartido', 'PublicationsController@addSharing')->name('public.add.sharing');
  
  Route::post('/me-gusta-comentario', 'CommentsController@addlike')->name('public.add.like.comment');
  
});
Route::get('/compartir-en-facebook', 'PublicationsController@view')->name('publication.sharing.facebook');

Route::get('/subir-caso',function () {
    return view('/subir-caso');
})->name('publication.upload'); 



Route::get('/nuevo-caso', 'PublicationsController@case')->name('case.create');
Route::get('/nuevo-proyecto', 'PublicationsController@proyect')->name('proyect.create');
Route::get('/nuevo-oportunidad', 'PublicationsController@oportunity')->name('oportunity.create');
Route::get('/ingreso-exitoso', 'HomeController@messageSuccess')->name('message.publication');

Route::post('/actualizar-publicacion', 'PublicationsController@update')->name('publicacion.update');
Route::post('/guardar-publicacion', 'PublicationsController@save')->name('publicacion.save');
Route::post('/buscar-publicacion', 'PublicationsController@search')->name('publicacion.search');
Route::post('/eliminar-publicacion', 'PublicationsController@delete')->name('publicacion.delete');

//proyecto
Route::post('/guardar-proyecto', 'ProyectsController@save')->name('proyect.save');

//Oportunidad
Route::post('/guardar-oportunidad', 'OportunitiesController@save')->name('oportunity.save');




Route::namespace('admin')->middleware(['administrador'])->group(function(){
    Route::get('/admin/home','HomeController@index')->name('admin');
    Route::get('/admin/users','UsersController@index')->name('users.index');
    Route::get('/admin/rankins','RankinsController@index')->name('rankins.index');
    Route::get('/admin/casos','PublicationsController@index')->name('cases.index');
    Route::get('/admin/oportunidades','OportunitiesController@index')->name('oportunities.index');
    Route::get('/admin/proyectos','ProyectsController@index')->name('proyects.index');
    Route::get('/admin/puentes/{id?}','BridgesController@index')->name('bridges.index');
    Route::get('/admin/publicacion/comentarios/{id?}','CommentsController@index')->middleware(['can:ver comentarios'])->name('comments.index');

    //admin- casos
    Route::get('/admin/editar-caso/{id?}','PublicationsController@edit')->name('admin.edit.case');
    Route::post('/admin/guardar-caso','PublicationsController@store')->middleware(['can:crearEditar casos'])->name('admin.store.case');
    Route::post('/admin/actualizar-caso','PublicationsController@update')->middleware(['can:crearEditar casos'])->name('admin.update.case');
    Route::get('/admin/eliminar-caso/{id?}','PublicationsController@delete')->middleware(['can:eliminar casos'])->name('admin.delete.case');
    Route::get('/admin/activar-caso/{id?}','PublicationsController@active')->middleware(['can:publicar casos'])->name('admin.active.case');
    Route::get('/admin/actualizar-foto-de-perfil/{id?}','PublicationsController@activePhoto')->name('admin.active.photo');
    Route::get('/admin/eliminar-foto-de-perfil/{id?}','PublicationsController@deletePhoto')->name('admin.delete.photo');
    Route::get('/admin/finalizar-caso/{id?}','PublicationsController@finalize')->middleware(['can:finalizar casos'])->name('admin.finalize.case');
    Route::post('/admin/caso-filtrado','PublicationsController@search')->name('admin.search.case');
    Route::get('/admin/eliminar-informe/{id}','PublicationsController@deleteReport')->name('admin.delete.report');
    Route::post('/admin/qr-descargado','PublicationsController@qrDowloaded')->name('admin.qr.case');

    //donantes
    Route::get('/admin/donantes/{id}','DonorsController@index')->name('donors.index');
    Route::get('/admin/editar-donante/{publications_id}/{users_id?}','DonorsController@edit')->name('admin.edit.donor');
    Route::post('/admin/guardar-donante','DonorsController@store')->name('admin.store.donor');
    Route::post('/admin/actualizar-donante','DonorsController@update')->name('admin.update.donor');
    Route::get('/admin/eliminar-donante/{id?}','DonorsController@delete')->name('admin.delete.donor');
    Route::post('/admin/donante-usuario-filtrado/{id}','DonorsController@search')->name('admin.search.donor');
    Route::post('/admin/donante-filtrado/{id}','DonorsController@searchDonor')->name('admin.search.donor.index');
    Route::post('/admin/actualizar-informacion-donante','DonorsController@updatePublications')->name('admin.update.publication');

    //Usuarios
    Route::get('/admin/usuarios','UsersController@index')->middleware(['can:crearEditar usuarios'])->name('users.index');
    Route::get('/admin/usuario/{users_id?}','UsersController@edit')->middleware(['can:crearEditar usuarios'])->name('admin.edit.user');
    Route::post('/admin/guardar-usuario','UsersController@store')->middleware(['can:crearEditar usuarios'])->name('admin.store.user');
    Route::post('/admin/actualizar-usuario','UsersController@update')->name('admin.update.user');
    Route::get('/admin/eliminar-usuario/{id?}','UsersController@delete')->middleware(['can:eliminar usuarios'])->name('admin.delete.user');
    Route::post('/admin/usuario-con-filtro','UsersController@search')->name('admin.search.user');
    

    //admin- proyectos
    Route::get('/admin/editar-proyecto/{id?}','ProyectsController@edit')->name('admin.edit.proyect');
    Route::post('/admin/guardar-proyecto','ProyectsController@store')->name('admin.store.proyect');
    Route::post('/admin/actualizar-proyecto','ProyectsController@update')->name('admin.update.proyect');
    Route::get('/admin/eliminar-proyecto/{id?}','ProyectsController@delete')->middleware(['can:eliminar proyectos'])->name('admin.delete.proyect');
    Route::get('/admin/activar-proyecto/{id?}','ProyectsController@active')->middleware(['can:publicar proyectos'])->name('admin.active.proyect');
    Route::get('/admin/actualizar-foto-de-perfil/{id?}','ProyectsController@activePhoto')->name('admin.active.photo');
    Route::get('/admin/eliminar-foto-de-perfil/{id?}','ProyectsController@deletePhoto')->name('admin.delete.photo');
    Route::get('/admin/finalizar-proyecto/{id?}','ProyectsController@finalize')->middleware(['can:finalizar proyectos'])->name('admin.finalize.proyect');
    Route::post('/admin/proyecto-filtrado','ProyectsController@search')->name('admin.search.proyect');
    

  //  Route::get('/admin/guardar-proyecto','ProyectsController@store')->name('admin.store.proyect');

    //admin- oportunidades
    Route::get('/admin/editar-oportunidad/{id?}','OportunitiesController@edit')->name('admin.edit.oportunity');
    Route::post('/admin/guardar-oportunidad','OportunitiesController@store')->name('admin.store.oportunity');
    Route::post('/admin/actualizar-oportunidad','OportunitiesController@update')->name('admin.update.oportunity');
    Route::get('/admin/eliminar-oportunidad/{id?}','OportunitiesController@delete')->middleware(['can:eliminar oportunidades'])->name('admin.delete.oportunity');
    Route::get('/admin/activar-oportunidad/{id?}','OportunitiesController@active')->middleware(['can:publicar oportunidades'])->name('admin.active.oportunity');
    Route::get('/admin/actualizar-foto-de-perfil/{id?}','OportunitiesController@activePhoto')->name('admin.active.photo');
    Route::get('/admin/eliminar-foto-de-perfil/{id?}','OportunitiesController@deletePhoto')->name('admin.delete.photo');
    Route::get('/admin/finalizar-oportunidad/{id?}','OportunitiesController@finalize')->middleware(['can:finalizar oportunidades'])->name('admin.finalize.oportunity');
    Route::post('/admin/oportunidad-filtrado','OportunitiesController@search')->name('admin.search.oportunity');
    Route::get('/admin/eliminar-foto-interna/{id?}','OportunitiesController@deletePhotoInternal')->name('admin.delete.photo.internal');
    Route::post('/admin/guardar-foto-interna','OportunitiesController@search')->name('admin.search.oportunity');
    Route::post('/admin/eliminar-foto-interna','OportunitiesController@search')->name('admin.search.oportunity');

    //Beneficiarios
    Route::get('/admin/Beneficiario/{id}','BenefController@indexHelped')->name('benef.index');
    Route::get('/admin/editar-beneficiario/{publications_id}/{users_id?}','BenefController@edit')->name('admin.edit.benef');
    Route::post('/admin/guardar-beneficiario','BenefController@store')->name('admin.store.benef');
    Route::post('/admin/actualizar-beneficiario','BenefController@update')->name('admin.update.benef');
    Route::get('/admin/eliminar-beneficiario/{id?}','BenefController@delete')->name('admin.delete.benef');
    Route::post('/admin/beneficiario-usuario-filtrado/{id}','BenefController@search')->name('admin.search.benef');
    Route::post('/admin/beneficiario-filtrado/{id}','BenefController@searchDonor')->name('admin.search.benef.index');
    Route::post('/admin/actualizar-informacion-','BenefController@updatePublications')->name('admin.update.publication');


    //admin- eventos
    Route::get('/admin/eventos/{publication_id}','EventsController@index')->name('events.index');
    Route::get('/admin/mi-evento/{publications_id}/{events_id?}','EventsController@edit')->name('admin.edit.event');    
    Route::post('/admin/guardar-evento/{id?}','EventsController@store')->name('admin.store.event');    
    Route::post('/admin/actualizar-evento','EventsController@update')->name('admin.update.event');
    Route::get('/admin/eliminar-evento/{id?}','EventsController@delete')->name('admin.delete.event');
    Route::post('/admin/evento-filtrado/{id?}','EventsController@search')->name('admin.search.event');
    Route::get('/admin/eliminar-adjunto/{id}','EventsController@deleteFile')->name('admin.delete.file');

    //rankink
    Route::get('/admin/rankin-filtrado/{id}','RankinsController@filter')->name('admin.filter.rankin');
    Route::get('/admin/ranking-ayudados','RankinsController@helped')->name('rankins.helped.index');
    Route::get('/admin/rankin-ayudados-filtrado/{id}','RankinsController@filterHelped')->name('admin.filter.rankin.helped');

    //puentes
    //Route::get('/admin/puentes/{id}','BridgesController@index')->name('bidges.index');

    //comentarios
  //  Route::get('/admin/comentarios/{id}','CommentsController@index')->name('comments.index');
  Route::get('/admin/editar-puente/{id?}','BridgesController@edit')->name('admin.edit.bridge');
  Route::post('/admin/guardar-puente','BridgesController@store')->name('admin.store.bridge');
  Route::post('/admin/actualizar-puente','BridgesController@update')->name('admin.update.bridge');
  Route::get('/admin/retroceder-caso/{id?}','BridgesController@finalize')->middleware(['can:volver puentes'])->name('admin.finalize.bridge');
  Route::post('/admin/puente-filtrado','BridgesController@search')->name('admin.search.bridge');
  
  Route::get('/admin/ver-comentarios/{id?}','CommentsController@view')->name('admin.view.comment');
  Route::get('/admin/eliminar-comentario/{id?}','CommentsController@delete')->middleware(['can:eliminar comentarios'])->name('admin.delete.comment');
  Route::post('/admin/comentarios-filtrados-','CommentsController@search')->name('admin.search.comment');

  //reportes
  Route::get('/usuarios-pdf','UsersController@exportPdf')->name('reports.export-pdf');

  Route::get('/usuarios-excel','UsersController@exportExcel')->name('reports.export-excel');

});

Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');


//Route::get('/home', 'HomeController@index')->name('home');

	/**login social link */
	Route::get('/callback-login','SocialLoginController@index')->name('social.login');
	Route::get('/terminos-y-condiciones',function()
	{
		return view('web.terminos');
	});
	Route::get('/politicas-de-privacidad',function()
	{
		return view('web.politicas');
	});
	Route::post('/social/registrarme', 'Auth\SocialLoginController@AddRole')->name('social.add.role');
	Route::get('/social/redirect/{provider}', 'Auth\SocialLoginController@redirectToProvider')->name('social.auth');
	Route::get('/social/handle/{provider}', 'Auth\SocialLoginController@handleProviderCallback')->name('callback-login');
    /** end social link */
    
    Route::get('/puente-social/{name}/{id}/{activate}', 'HomeController@previewPublication')->name('sharink.link');
    Route::get('/puente-social','HomeController@index')->name('sharing.bridge');

    /**
     * Profile
     */

    Route::get('/mi-perfil', function(){
      return view('profile');
  
    })->name('profile.index');
    Route::post('/actualizar-datos-de-perfil', 'ProfileController@store')->name('profile.save');

  /**
   * Emails
   */
  Route::post('/puente-social/enviar-materiales', 'HomeController@donarMateriales')->name('donar.materiales.send.email');
  Route::post('/puente-social/enviar-caso', 'HomeController@enviarCaso')->name('enviar.caso.send.email');
  Route::post('/puente-social/enviar-oportunidad', 'HomeController@enviarOportunidad')->name('enviar.oportunidad.send.email');
  Route::post('/puente-social/enviar-oportunidad-laboral', 'HomeController@enviarOportunidadLaboral')->name('enviar.oportunidad.laboral.send.email');
  Route::post('/puente-social/enviar-proyecto', 'HomeController@enviarProyecto')->name('enviar.proyecto.send.email');
  Route::post('/puente-social/formulario-de-contacto', 'HomeController@enviarContacto')->name('enviar.contacto.send.email');

    
    Route::get('/aportes',function()
	{
		return view('aportes');
    });
    Route::get('/donacion-materiales',function()
	{

		return view('donacion-materiales');
    });
    Route::get('/empresas',function()
	{
    $users= \App\User::role('business')->get(); 
		return view('empresas',compact('users'));
    });
    Route::get('/fundaciones',function()
	{
		return view('fundaciones');
    });
    
    Route::get('/enviar-caso',function()
	{

		return view('enviar-caso');
    });
    Route::get('/como-funciona',function()
	{
		return view('como-funciona');
    });
    Route::get('/enviar-proyecto',function()
	{

		return view('enviar-proyecto');
    });
    Route::get('/enviar-oportunidad',function()
	{

		return view('enviar-oportunidad');
    });
    Route::get('/enviar-oportunidad-laboral',function()
	{
		return view('enviar-oportunidad-laboral');
    });
    Route::get('/deseo-donar',function()
	{
		return view('deseo-donar');
    });
    Route::get('/nuestro-equipo',function()
    {
      return view('equipo');
      });
      Route::get('/contacto',function()
      {
        return view('contacto');
        });
        Route::get('/terminos-y-politicas',function()
        {
          return view('terminos');
          });
    Route::get('/login-admin',function()
	{
		return view('auth.admin.login');
    })->name('loginAdmin');
    Route::get('/login-empresas',function()
	{
		return view('auth.login-empresas');
    });
    Route::get('/registro-empresas',function()
	{
		return view('auth.registro-empresas');
  });

  Route::get('/muchas-gracias',function()
	{
		return view('muchas-gracias');
  });
  
 