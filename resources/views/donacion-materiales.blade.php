@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->
@if(Session::has('successMail'))
<br><br>
<div class="input-field text-center">
    <h2><strong>GRACIAS POR ENVIAR TU MENSAJE!</strong> </h2>
    <h3><br> NOS COMUNICAREMOS CONTIGO <br> A LA BREVEDAD</h3>

</div>
@else
        <div class="row">
		 
			
			<br>		

		
		<div class="col-md-2 col-sm-2">
			
			
		</div>
		
		<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
				
				<br>
			<br>
					<h4><b>APORTE DE MATERIALES / OTROS</b></h4>
					
					
								<p>(*) Campos requeridos</p>
								<form action="{{route('donar.materiales.send.email')}}"  method="post" class="m-t-15" enctype="multipart/form-data">
								@csrf
									<div class="input-field">
										<textarea class="form-control" name="texarea" id="comments" rows="4" placeholder="Detalle de las cosas para donar..."></textarea>
									</div>
									<br>
									<div class="input-field">
										<input type="text" name="direccion" class="form-control" placeholder="Dirección de donde se retiran..." required>
									</div>
									<br>
									<div class="input-field">
										<input type="text" name="ciudad" class="form-control" placeholder="Ciudad...">
									</div>
									<br>
									<div class="input-field">
										<input type="text" name="provincia" class="form-control" placeholder="Provincia...">
									</div>
									<br>
									<div class="input-field">
										<input type="mail" name="mail" class="form-control" placeholder="* Email..." required>
									</div>
									<br>
									<div class="input-field">
										<input type="text" name="telefono" class="form-control" placeholder="Celular de contacto...">
									</div>										
									<br>
									<div class="input-field">
										<label for="">Enviar fotos: <span></span></label>
									  <input type="file" name="photos[]" id="photo" class="form-control" multiple>
									</div>														
									<br>
									<button type="submit" id="submit" class="btn btn-lg btn-success">Enviar</button>
									<br>
									<br>
								</form>
					
					
					
			
		</div>
		
		<div class="col-md-2 col-sm-2">
		
		
		</div>
		<br>               
		<br>	
	
	</div>
	<div class="row">
		
		<hr>
		<div class="col-md-6 col-sm-6 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
			
			<br>
					<a href="https://wa.me/5491121646003" target="_blanck"> <button type="button" class="btn btn-success"> <i class="fa fa-whatsapp"></i> WHATSAPP 11 2164 6003  </button></a>
					<br>
		
		
		
		</div>
		
		<div class="col-md-6 col-sm-6 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
			<br>
			<a href="mailto:info@puentesocial.org" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-envelope-o"></i> info@puentesocial.org  </button></a>
				<br>
			
		</div>
		
		<div class="col-md-12 col-sm-12 text-center">
			<br>
		<p><small>(*) Nuestra Fundación posee el Certificado de Exención vigente, en el marco de lo dispuesto en la Resolución General Nº2681 (AFIP), lo que determina la procedencia de la exención aludida - en los casos en que se autorice la deducción de donaciones (Art. 81 c) - Validará para el donante la deducción en el </small></p>
		</div>
	
	</div>
		<div class="col-md-12 col-sm-12 col-xs-12 text-center">	
		<div class="row">
			
			<div class="col-md-1 col-sm-1 col-xs-1">
				
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-2">
				<p><a href="tel:541147861798 "><i class="fa fa-phone fa-2x" aria-hidden="true"></i></a></p>							 	
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-2">
				<p><a href="mailto: info@puentesocial.org?subject=Deseo%20Recibir%20más%20información&body=Quiero%20recibir%20mas%20información%20de%20donar%20materiales" target="_blank"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></p>						 	
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-2">
				 <a onclick="sharing_link()"><i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></a>		
												 
			</div>

			<div class="col-md-2 col-sm-2 col-xs-2">
				 <a onclick="load_fb();"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>	
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-2">
				<a onclick="sharing_button()" id="btn_twitter"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>							 	
			</div>
			
			<div class="col-md-1 col-sm-1 col-xs-1">
				
			</div>
			
		</div>				

			
		</div>
	
    
	
		
		
		@endif

<!--FIN DE MODAL PARA COMPARTIR LINK-->

<div id="fb-root"></div>
@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
    $("#link_mp").val(sessionStorage.getItem('link_mp'));
    $("#link_paypal").val(sessionStorage.getItem('link_paypal'));

</script>
@endsection
