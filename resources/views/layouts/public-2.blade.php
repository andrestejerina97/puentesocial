<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="PuenteSocial es una plataforma creada con el objetivo de unir partes para lograr un mundo más equitativo. Una de los grandes problemas sociales que enfrentamos es la desigualdad, lo que provoca que millones de personas no puedan desarrollarse. Este es un derecho universal que vemos vulnerado en personas que quedan marginadas y sin ninguna posibilidad." /> 
	<meta name="author" content="Hamilton-group"> 
	<title>PUENTE SOCIAL</title> 
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?v=2">
	<!------ Include the above in your HEAD tag ---------->


	<link href="{{asset('css/main.css?v2')}}" rel="stylesheet">	
	<link href={{asset('css/responsive.css')}} rel="stylesheet"> 
	<link href="{{asset('css/mapa.css')}}" rel="stylesheet"> 
      
  
  <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
  <link href="{{asset('css/infinite-slider.css')}}" rel="stylesheet">
  <link href="{{asset('css/social.css')}}" rel="stylesheet"> 
  <link href="{{asset('css/social2.css')}}" rel="stylesheet"> 

  <link href="{{asset('css/animate.css')}}" rel="stylesheet"> 
  
  <script src="{{asset('js/wow.min.js')}}"></script>
  
<link rel="stylesheet" href="{{asset("css/whatsapp.css")}}">

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="{{asset('favicon.ico')}}"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
		
	<style>
		.btn-active{
			color: #024b19;
		}
	</style>
	@yield('css')
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-3NE892RZZC"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-3NE892RZZC');
</script>
		<script>
		 new WOW().init();
		</script>
	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		
		
		
	</head><!--/head-->
<body>
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner"> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
		
			</div>
		</div>
	</div><!--/.preloader-->
	
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a href="https://api.whatsapp.com/send?phone=5491121646003&text=Hola%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20Puente%20Social." class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>
	


	<header id="navigation"> 
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner"> 
			<div class="container"> 
				<div class="navbar-header"> 
					<button type="button" class="navbar-toggle" style="background-color: transparent !important; border:none;" data-toggle="collapse" data-target=".navbar-collapse"> 
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
					</button> 
					@if (Auth::check())
					<br>
					<a href="{{url('/mi-perfil')}}"> <img data-toggle="tooltip" data-placement="left"  title="Nombre: {{Auth::user()->name}} email: {{Auth::user()->email}} " loading="lazy" id="photo"
						src="{{asset('storage/users/'.Auth::user()->id."/".Auth::user()->photo )}}"  style="max-width: 30px; min-width:30px; max-height: 30px; border-radius: 20px;margin-left: 20px !important; " alt="Mi perfil"><small><strong style="color: #ffffff !important;">
							{{Auth::user()->name}} 
						</strong></small> </a>
					 	
						@else
						<a class="navbar-brand" href="{{url('/')}}"><h1><span style="color: #fff;"><img src="{{asset('images/iconos/logo-puente.svg')}}"></span></h1></a> 

						@endif
				</div> 
				<div class="collapse navbar-collapse"> 
			
					<ul class="nav navbar-nav navbar-right"> 
					<li class="scroll"><a class="page-scroll" href="{{url('/')}}">HOME</a></li>							
                        <li><a class="scroll" href="{{url('/como-funciona')}}">¿COMO FUNCIONA?</a></li>
						<li><a class="scroll" href="{{url('/subir-caso')}}">CARGAR UN CASO</a></li>
						 <li><a class="scroll" href="{{url('/nuestro-equipo')}}">NUESTRO EQUIPO</a></li>
						 <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">EMPRESAS Y ONG<span class="caret"></span></a>
							<ul class="dropdown-menu">
								   <li><a href="{{url('/empresas')}}"> EMPRESAS QUE NOS ACOMPAÑAN</a></li>
									 <li role="separator" class="divider"></li>
									 <li><a href="{{url('/fundaciones')}}"> FUNDACIONES Y ONGS</a></li>
									 <li role="separator" class="divider"></li>
							<li><a href="{{url('registro-empresas')}}"> QUIERO SER PARTE</a></li>
									 <li role="separator" class="divider"></li>
								 </ul>
						 </li>
						 <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" >DESEO DONAR<span class="caret"></span></a>
                            <ul class="dropdown-menu">
							<li><a href="{{url('/deseo-donar')}}"> PARA LA FUNDACIÓN</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="{{url('donacion-materiales')}}"> MATERIALES</a></li>
								<li role="separator" class="divider"></li>
                            </ul>
                        </li>
                        <li><a class="scroll" href="{{url('contacto')}}">CONTACTENOS</a></li>
						
						@if (Auth::check())
			
					<li><a href="{{url('/mi-perfil')}}"><i class="fa fa-user"></i> Mi perfil</a></li>
						<li role="separator" class="divider"></li>
						<li>
							<a class="scroll" href="javascript:;" onclick="document.getElementById('form_logout').submit();" ><p>SALIR</p>
							</a>
						</li>
						@endif
												
					</ul>	
				</div>				
			</div>			
		</div><!--/navbar--> 
	</header> <!--/#navigation--> 
	<form id="form_logout" action="{{route('logout')}}" method="POST">
		@csrf
		</form>

		<section id="home">
			<br>
			<br>
	
		</section><!--/#home-->

  <!-- Start Service area -->
  <section id="about-us" class="about-area area-padding">
    <div class="container">
        @yield('content')
    </div>

    	
</section>

<footer id="footer"> 
	<div class="container"> 
		<div class="text-center"> 
				<div class="social-icons">
					<a href="https://www.facebook.com/puentesocial.org/" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="https://www.instagram.com/puentesocial/" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="https://twitter.com/"><i class="fa fa fa-twitter"></i></a>
					<a href="https://anchor.fm/fundacion-espiga" target="_blank"><i class="fa fa fa-podcast"></i></a>
				</div>
			<br>
			 <p>&copy; Copyright <strong>Puente Social</strong>. Todos los derechos reservados.</p>
		</div> 
	</div> 
</footer> <!--/#footer--> 

<script src="js/wow.min.js"></script>

 <script src="js/owl.carousel.min.js"></script>
	
<!-- carousel logos empresas -->   	
<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/smoothscroll.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/jquery.isotope.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.prettyPhoto.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/jquery.parallax.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/main.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script>

 <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    
<!-- carousel logos empresas -->   	
<script type="text/javascript" src="{{asset('js/carousel.js')}}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <!-- end - carousel logos empresas -->  
<script>
	jQuery(window).load(function(){'use strict';
	$(".preloader").delay(600).fadeOut("slow").remove();
});

</script>
@yield('scripts')

</body>
</html>
		
		
	

  
