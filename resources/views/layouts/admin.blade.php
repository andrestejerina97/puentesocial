<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="PuenteSocial es una plataforma creada con el objetivo de unir partes para lograr un mundo más equitativo. Una de los grandes problemas sociales que enfrentamos es la desigualdad, lo que provoca que millones de personas no puedan desarrollarse. Este es un derecho universal que vemos vulnerado en personas que quedan marginadas y sin ninguna posibilidad." /> 
	<meta name="author" content="Hamilton-group">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>PUENTE SOCIAL</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->


	<link href="{{asset('css/admin.css')}}" rel="stylesheet">	
	<link href={{asset('css/responsive.css')}} rel="stylesheet"> 
	<link href="{{asset('css/mapa.css')}}" rel="stylesheet"> 
    

<link href="{{asset('css/infinite-slider.css')}}" rel="stylesheet">
<link href="{{asset('css/social.css')}}" rel="stylesheet"> 
    
<link href="{{asset('css/animate.css')}}" rel="stylesheet"> 
<link href="{{asset('css/style.css')}}" rel="stylesheet"> 
@yield('css')
<script src="{{asset('js/wow.min.js')}}"></script>

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="favicon.ico"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-3NE892RZZC"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-3NE892RZZC');
		</script>
	
	<script>
	 new WOW().init();
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	
</head><!--/head-->
<body>
    <header id="navigation"> 
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner"> 
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-6 col-4">
						<div class="navbar-header"> 
							<a href="{{route('admin')}}"><h3 class="page-scroll" >ADMINISTRACIÓN</h3>  </a>		 
						</div> 
					</div>
					<div class="col-lg-6 col-md-6 col-xs-6 col-8">
						<div class="text-right">
							@if(Auth::user()->photo == 'avatar.png')
							<div class="row">
								<img src="{{asset('images/avatar.png')}}" class="img-thumbnail" style="max-width: 50px; min-width:50px; max-height: 50px; " alt="">
		
								<span style="color: #ffffff; font-size: 110%;">Usuario: {{Auth::user()->name}}</span>
		
								<form id="form_logout" action="{{route('logout')}}" method="POST">
									@csrf
								<a href="javascript:;" onclick="document.getElementById('form_logout').submit();" style="color: #ffffff; font-size: 110%;">Cerrar sesión</a>
								</form>
							</div>
							@else
							<div class="row">
								<img src="{{asset('storage/users/'.Auth::user()->id."/".Auth::user()->photo)}}" class="img-thumbnail" style="max-width: 50px; min-width:50px; max-height: 50px; " alt="sin foto"> 
		
								<span style="color: #ffffff; font-size: 110%;">Usuario: {{Auth::user()->name}}</span>
		
								<form id="form_logout" action="{{route('logout')}}" method="POST">
									@csrf
								<a href="javascript:;" onclick="document.getElementById('form_logout').submit();" style="color: #ffffff; font-size: 110%;">Cerrar sesión</a>
								</form>
							</div>
		
							@endif
		
						</div>  
					</div>
				</div>
				
			
			</div>			
		</div><!--/navbar--> 
	</header> <!--/#navigation--> 
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner"> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
		
			</div>
		</div>
	</div><!--/.preloader-->
    <section id="home">
        <br>
        <br>
   

	</section><!--/#home-->
  
    <section id="admin" class="about-area area-padding">
  
         
        <div class="row">
            <div class="">
				
                <div class="sidebar">
					<a href="#" class="img-side"><h1><span ><img src="{{asset('images/iconos/puenteblanco.jpeg')}}" width="140" ></span></h1></a>

                    <div class="mt-1 ">
                    <a  href="{{route('users.index')}}"><i class="fa fa-users fa-2x"></i> <span class="nav-text">Usuarios</span></a>
                    <a href="{{route('rankins.index')}}"><i class="fa fa-bookmark fa-2x"></i> <span class="nav-text">Rankins</span></a>
					<a href="{{route('rankins.helped.index')}}"><i class="fa fa-star fa-2x"></i> <span class="nav-text">Ayudados</span></a>
					<a href="{{route('cases.index')}}"><i class="fa fa-heart-o fa-2x "></i> <span class="nav-text">Casos</span></a>
					<a href="{{route('proyects.index')}}"><i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i> <span class="nav-text">Proyectos</span></a>
                    <a href="{{route('oportunities.index')}}"><i class="fa fa-diamond fa-2x"></i> <span class="nav-text">Oportunidades</span></a>
                    <a href="{{route('bridges.index')}}"><i class="fa fa-heart fa-2x"></i> <span class="nav-text">Puentes</span></a>
                    <a href="{{route('comments.index')}}"><i class="fa fa-comment fa-2x"></i> <span class="nav-text">Comentarios</span></a>

                    </div>
                  </div>
                @yield('content')
            </div>          
        
		  </div>
		  
		  
		  
        </section>
 

	

<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/smoothscroll.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.isotope.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.prettyPhoto.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.parallax.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/main.js')}}"></script> 
	<script src="{{asset('js/wow.min.js')}}"></script>
    @yield('scripts')
    
</body>
</html>