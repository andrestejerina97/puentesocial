<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="PuenteSocial es una plataforma creada con el objetivo de unir partes para lograr un mundo más equitativo. Una de los grandes problemas sociales que enfrentamos es la desigualdad, lo que provoca que millones de personas no puedan desarrollarse. Este es un derecho universal que vemos vulnerado en personas que quedan marginadas y sin ninguna posibilidad." /> 
	<meta name="author" content="Hamilton-group"> 
	<title>PUENTE SOCIAL</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->


	<link href="{{asset('css/main.css')}}" rel="stylesheet">	
	<link href={{asset('css/responsive.css')}} rel="stylesheet"> 
	<link href="{{asset('css/mapa.css')}}" rel="stylesheet"> 
    

<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link href="{{asset('css/infinite-slider.css')}}" rel="stylesheet">
<link href="{{asset('css/social.css')}}" rel="stylesheet"> 
    
<link href="{{asset('css/animate.css')}}" rel="stylesheet"> 

<script src="{{asset('js/wow.min.js')}}"></script>

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="favicon.ico"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-3NE892RZZC"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-3NE892RZZC');
		</script>
	<script>
	 new WOW().init();
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	
</head><!--/head-->
<body>
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner"> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
		
			</div>
		</div>
	</div><!--/.preloader-->
	<header id="navigation"> 
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner"> 
			<div class="container"> 
				<div class="navbar-header"> 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
					</button> 
					
				<a class="navbar-brand" href="{{url('home')}}"><h1><span style="color: #fff;"><img src="{{asset('images/iconos/logo-puente.png')}}"></span></h1></a> 
				</div> 
				<div class="collapse navbar-collapse"> 
			
						<ul class="nav navbar-nav navbar-right"> 
				        <li><a class="page-scroll" href="index.html"><i class="fa fa-reply" aria-hidden="true"></i></a></li>                        
						<li><a class="scroll" href="subir-caso.html">SUBIR CASO</a></li>  
                        <li><a class="scroll" href="contacto.html">CONTACTO</a></li>
                        <li class="scroll"><a href="https://www.facebook.com/puentesocial.org/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="scroll"><a href="https://www.instagram.com/puentesocial/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li class="scroll"><a href="https://wa.me/541166701732" target="_blanck"><i class="fa fa-whatsapp"></i></a></li>
											
					</ul>	
				</div>				
			</div>			
		</div><!--/navbar--> 
	</header> <!--/#navigation--> 
    
    

	<section id="home">
        <br>
        <br>
   

	</section><!--/#home-->
  

  <!-- Start Service area -->
  <section id="about-us">
    <div class="container">

        @yield('content')
    </div>
		
		
		
</section>
	<footer id="footer"> 
		<div class="container"> 
			<div class="text-center"> 
				 <p>&copy; Copyright <strong>Fundación Espiga</strong>. Todos los derechos reservados.</p>
			</div> 
		</div> 
	</footer> <!--/#footer--> 
	

<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/smoothscroll.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.isotope.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.prettyPhoto.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.parallax.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/main.js')}}"></script> 
	<script src="{{asset('js/wow.min.js')}}"></script>
    
     <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        
    <!-- carousel logos empresas -->   	
    <script type="text/javascript" src="{{asset('js/carousel.js')}}"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
      <!-- end - carousel logos empresas -->  
    
    @yield('scripts')
    
</body>
</html>