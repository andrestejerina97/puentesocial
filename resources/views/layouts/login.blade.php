<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="PuenteSocial es una plataforma creada con el objetivo de unir partes para lograr un mundo más equitativo. Una de los grandes problemas sociales que enfrentamos es la desigualdad, lo que provoca que millones de personas no puedan desarrollarse. Este es un derecho universal que vemos vulnerado en personas que quedan marginadas y sin ninguna posibilidad." /> 
	<meta name="author" content="Hamilton-group">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>PUENTE SOCIAL</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->


	<link href="{{asset('css/main.css')}}" rel="stylesheet">	
	<link href={{asset('css/responsive.css')}} rel="stylesheet"> 
	<link href="{{asset('css/mapa.css')}}" rel="stylesheet"> 
    

<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link href="{{asset('css/infinite-slider.css')}}" rel="stylesheet">
<link href="{{asset('css/social.css')}}" rel="stylesheet"> 
    
<link href="{{asset('css/animate.css')}}" rel="stylesheet"> 

<script src="{{asset('js/wow.min.js')}}"></script>

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="favicon.ico"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-3NE892RZZC"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-3NE892RZZC');
		</script>
	
	<script>
	 new WOW().init();
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	
</head><!--/head-->
<body>
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner"> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
		
			</div>
		</div>
	</div><!--/.preloader-->


  <!-- Start Service area -->
  <section id="login" class="about-area area-padding">
    <div class="container">

        @yield('content')
    </div>
		
		
		
</section>
	<footer id="footer"> 
		<div class="container"> 
			<div class="text-center"> 
				 <p>&copy; Copyright <strong>Fundación Espiga</strong>. Todos los derechos reservados.</p>
			</div> 
		</div> 
	</footer> <!--/#footer--> 
	

<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/smoothscroll.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.isotope.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.prettyPhoto.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.parallax.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/main.js')}}"></script> 
	<script src="{{asset('js/wow.min.js')}}"></script>
    
     <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        
    <!-- carousel logos empresas -->   	
    <script type="text/javascript" src="{{asset('js/carousel.js')}}"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
      <!-- end - carousel logos empresas -->  
    
    @yield('scripts')
    
</body>
</html>