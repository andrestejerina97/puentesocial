@extends('layouts.public-2')
@section('css')

@endsection
@section('content')
<div class="row">
		
    <br>		
    
    <div class="col-md-3 col-sm-3 col-lg-3">
         
    </div>	 
    
    
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
            
    <br>
    
    <h4><b>PUBLICAR EN PUENTE SOCIAL</b></h4>
    <br>
    <br>
    <a href="enviar-caso.html" target="_blanck"> <button type="button" class="btn btn-success">DESEO PUBLICAR UN CASO</button></a>
    <p>Puede ser un caso propio a de un tercero que conozcas. Te pedimos compromiso y responsabilidad. La información de contacto será verificada antes de su publicación.</p>	
    <br>				
    <a href="enviar-proyecto.html" target="_blanck"> <button type="button" class="btn btn-success">DESEO PUBLICAR UN PROYECTO</button></a>
    <p>El proyecto debe ser de caracter social. En caso de ONGs o empresas deben acreditar identidad, asumiendo todas las responsabilidades por lo publicado.</p>
    <br>		
    <a href="enviar-oportunidad-donacion.html" target="_blanck"> <button type="button" class="btn btn-info">DESEO OFRECER UNA DONACIÓN</button></a>
    <p>Podes publicar algo que tengas para compartir como donación. Nuestra Fundación se ocupara de encontrar quien necesite lo que estas publicando.</p>
    <br>	
    <a href="enviar-oportunidad-laboral.html" target="_blanck"> <button type="button" class="btn btn-info">PUBLICAR UNA OFERTA TRABAJO</button></a>
    <p>Podes publicar una oferta de empleo formal o temporal, haciendo la descripción pertinente y asumiendo las responsabilidades directas con los interesados.</p>
    <br>		
    <br>	

        
    </div>
    
    <div class="col-md-3 col-sm-3 col-lg-3">
         
    </div>	
    
              
    
</div>	

@endsection
@section('scripts')

</script>
@endsection

