<!DOCTYPE html>
<html lang="en">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="author" content="Hamilton-group"> 
	<!-- META PARA FACEBOOK--->
	<meta property="og:url"                content="{{url()->current()}}" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="{{$publication->category->name}}-{{$publication->name_case}}" />
	<meta property="og:description"        content="{{$publication->text_facebook}}" />
	<meta property="og:image"              content="{{asset('storage/publicaciones/'.$publication->id.'/'.$publication->avatar)}}" />
	<meta property="fb:app_id" content="2564677387125919" />
	<!-- END META FACEBOOK-->
	<title>PUENTE SOCIAL</title> 
   

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="favicon.ico"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	
	
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-SWGHXDL85E"></script>


	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	
	
</head><!--/head-->
<body onload="location.href='{{route('home')}}'+'?puente='+{{$publication->code}} ">
	<a href="https://puentesocial.org/app/public">Cargando....</a>
    
    </body>
</html>