@extends('layouts.public-2')
@section('css')
    <style>
        .frame-photo{
            border: solid;
            border-radius: 5px;
            padding: 4px;
            background-color: aliceblue;
            
    }
    </style>
@endsection
@section('content')
<!-- Start Service area -->
        <div class="row">
            <div class=" hidden-md hidden-sm hidden-xs">
                <br>  
            </div>
            <div class=" visible-md visible-sm">
                <br> <br><br> <br><br>
            </div>
            @foreach ($publications as $publication)
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 text-left wow animated bounceInLeft box1"
                data-wow-delay="0.2s">
                <div class="hidden-xs"><br></div>
                <a href="#" data-toggle="modal" data-target="#Modal-fotos"> 
                  <div class="frame-photo">
                    <img width="100%" loading="lazy"
                    src="{{asset('storage/publicaciones/'.$publication->id.'/'.$publication->avatar)}}"
                    data-lazy="{{asset('storage/publicaciones/'.$publication->id.'/'.$publication->avatar)}}">
                <div class="text-center">
                <small style="color: #0c8843">VER MÁS FOTOS Y VIDEOS</small>
                </div>
            </div>    
                </a>
            </div>
            <div class="col-md-8 col-lg-8 col-sm-8 col-xs-6 wow animated bounceInLeft box2" data-wow-delay="0.6s">
                <div class="hidden-xs"><br></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3><b>{{$publication->name_case}}</b></h3>
                    <p>
                        @foreach ($case_categories as $case_category)
                        <i class="{{$case_category->icono}}" aria-hidden="true"></i> {{$case_category->name}} |
                        @endforeach
                    </p>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{$publication->city_province}}</p>

                    @switch($publication->categories_id)
                    @case(1)  <!--CASOS-->
                    <p>Estado caso: {{$publication->progress}}%</p>

                        @break
                    @case(2)  <!--PROYECTOS-->
                    <p> Avance proyecto: {{$publication->progress}}%</p>

                        @break
                    @case(3) <!--OPORTUNIDADES -->
                  
                    @break

                    @default  <!---->
                         
                @endswitch

					<div class="col-md-12 col-sm-12 col-lg-12 hidden-xs text-left">
						<div class="col-md-6 col-sm-6">
							<br>
							<a href="" data-toggle="modal" data-target="#Modal-fotos"><h4><button type="button" class="btn btn-secondary"><i class="fa fa-camera" aria-hidden="true"></i> FOTO Y VIDEO</button></h4></a>	
                            
                            @empty($publication->link_podcast)
                            @else
                            <a href="" data-toggle="modal" data-target="#Modal-podcast"><h4><button type="button" class="btn btn-secondary"><i class="fa fa-podcast" aria-hidden="true"></i> SU HISTORIA</button></h4></a>
                            @endempty
                  
                            							<br>
							<a href="javascript:;" class="add_like" data-entity="publication" id="btn_like" >
                                <p><strong ><i class="fa fa-2x fa-thumbs-up" aria-hidden="true"></i> ME GUSTA
                                    (<span class="likes">{{$publication->likes}}</span>)</strong></p>
                            </a>
                            <br>
                            <a href="javascript:;" onclick="shareFacebook();">  <p><strong><i class="fa fa-2x fa-share-alt-square" aria-hidden="true"></i> COMPARTIDO
                                (<span class="shared">{{$publication->shared}}</span>)</strong></p></a>
						</div>
					</div>
									
				<div class="col-sm-12">
				</div>

                    <div class="col-md-12 col-sm-12 col-lg-12 hidden-xs text-left">

                        <div class="col-md-6 col-sm-6">
                            <br>
             
                        </div>



                    </div>

                    <div class="col-sm-12">



                    </div>


                </div>
            </div>

            <div class="hidden-md hidden-sm hidden-lg col-xs-12 text-center" style="margin-top: 10px;">
                <div class="col-xs-12 text-left">
                   
                    <div class="col-xs-6">
                        <a href="javascript:;" class="add_like" data-entity="publication" id="btn_like" >
                            <p ><strong><i class="fa fa-2x fa-thumbs-up" aria-hidden="true"></i> ME GUSTA
								(<span class="likes">{{$publication->likes}}</span>)</strong></p>
						</a>
                    </div>
                    <div class="col-xs-6">
                          <a href="javascript:;" onclick="shareFacebook();">  <p><strong><i class="fa fa-2x  fa-share-alt-square" aria-hidden="true"></i> COMPARTIDO
                                (<span class="shared">{{$publication->shared}}</span>)</strong></p></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">

                @switch($publication->categories_id)
                    @case(1)  <!--CASOS-->
                    <div class="col-md-12 single1">
                    <a href="#" data-toggle="modal" data-target="#detalle"><h4>  <span data-toggle="tooltip" title="Información detallada sobre este caso."><i class="fa fa-info-circle" aria-hidden="true"></i>  DETALLES DEL CASO:</span></h4></a>
                    <p>{{$publication->details}}</p>
                    <br>
					<a href="#" data-toggle="modal" data-target="#informe"><h4> <span data-toggle="tooltip" title="Detalles del caso e historial del mismo."><i class="fa fa-info-circle" aria-hidden="true"></i> INFORME:</span></h4></a>
                    <p>{{$publication->report}}</p>
                    </div>
                    
                <div class="col-md-12 single2">

					<a href="#" data-toggle="modal" data-target="#objetivo"><h4><span data-toggle="tooltip" title="Descripción del objetivo que perseguimos en este caso."><i class="fa fa-info-circle" aria-hidden="true"></i>  OBJETIVO:</span></h4></a>
                    <p>{{$publication->objetive}}</p>
                    <br>
					<a href="#" data-toggle="modal" data-target="#duracion"> <h4><span data-toggle="tooltip" title="El tiempo que estimamos para cumplir los objetivos en este caso."><i class="fa fa-info-circle" aria-hidden="true"></i> DURACIÓN:</span></h4></a>
                    <p>{{$publication->duration}}</p>
                    </div>
                    
                <div class="col-md-12 single3">
					<a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Esta es la manera en que queremos ayudar en este caso.."><i class="fa fa-info-circle" aria-hidden="true"></i> PLAN DE AYUDA:</span></h4></a>
                    <p>{{$publication->help_plan}}</p>
					<br>
					<a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Cada puente conlleva un acuerdo. Aquí el detalle."><i class="fa fa-info-circle" aria-hidden="true"></i>  ACUERDO</span></h4></a>
                    <p>{{$publication->agreement}}</p>

				</div>
                        @break
                    @case(2)  <!--PROYECTOS-->
                    <div class="col-md-12 single1">
                    <a href="#" data-toggle="modal" data-target="#detalle"><h4>  <span data-toggle="tooltip" title="Información detallada sobre este proyecto."><i class="fa fa-info-circle" aria-hidden="true"></i>  DETALLES DEL PROYECTO:</span></h4></a>
                    <p>{{$publication->details}}</p>
                    <br>
					<a href="#" ><h4> <span data-toggle="tooltip" title="Detalle de las necesidades exactas de este proyecto."><i class="fa fa-info-circle" aria-hidden="true"></i> NECESIDADES:</span></h4></a>
                    <p>{{$publication->report}}</p>
                </div>
                
                <div class="col-md-12 single2">

					<a href="#" data-toggle="modal" data-target="#objetivo"><h4><span data-toggle="tooltip" title="Encontrarás cual es el objetivo que se quiere alcanzar."><i class="fa fa-info-circle" aria-hidden="true"></i>  OBJETIVO:</span></h4></a>
                    <p>{{$publication->objetive}}</p>
                    <br>
					<a href="#" data-toggle="modal" data-target="#duracion"> <h4><span data-toggle="tooltip" title="El tiempo que estimamos para cumplir los objetivos en este proyecto. ."><i class="fa fa-info-circle" aria-hidden="true"></i> DURACIÓN:</span></h4></a>
                    <p>{{$publication->duration}}</p>

                </div>
                
                <div class="col-md-12 single3">
					<a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Esta es la manera en que queremos ayudar en este proyecto."><i class="fa fa-info-circle" aria-hidden="true"></i> PLAN DE AYUDA:</span></h4></a>
                    <p>{{$publication->help_plan}}</p>
					<br>
					<a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Cada puente conlleva un acuerdo. Aquí el detalle."><i class="fa fa-info-circle" aria-hidden="true"></i>  ACUERDO</span></h4></a>
                    <p>{{$publication->agreement}}</p>

				</div>
                        @break
                    @case(3) <!--OPORTUNIDADES -->
                    <div class="col-md-12 single1">
                        <a href="#" data-toggle="modal" data-target="#detalle"><h4>  <span data-toggle="tooltip" title="Información detallada sobre la oportunidad."><i class="fa fa-info-circle" aria-hidden="true"></i>  DETALLES DE LA OPORTUNIDAD:</span></h4></a>
                        <p>{{$publication->details}}</p>
                        <br>
                        </div>
                        
                    <div class="col-md-12 single2">
    
                        <a href="#" data-toggle="modal" data-target="#objetivo"><h4><span data-toggle="tooltip" title="Descripción del objetivo que perseguimos con esta oportunidad."><i class="fa fa-info-circle" aria-hidden="true"></i>  A QUIENES VA DIRIGIDO:</span></h4></a>
                        <p>{{$publication->objetive}}</p>
                        <br>
                        <a href="#" data-toggle="modal" data-target="#duracion"> <h4><span data-toggle="tooltip" title="El tiempo que estimamos dure esta oportunidad."><i class="fa fa-info-circle" aria-hidden="true"></i> DURACIÓN:</span></h4></a>
                        <p>{{$publication->duration}}</p>
                        </div>
                        
                    <div class="col-md-12 single3">
                        <a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Contacto"><i class="fa fa-info-circle" aria-hidden="true"></i> CONTACTO:</span></h4></a>
                        <p>{{$publication->web}}</p>
                        <br>
                        <a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Mail de contacto"><i class="fa fa-info-circle" aria-hidden="true"></i>  MAIL:</span></h4></a>
                        <p>{{$publication->email}}</p>
                    </div>
                   
                    <div class="col-md-12 single3">
                        
                        <a href="#" data-toggle="modal" ><h4><span data-toggle="tooltip" title="Número de Whatsapp"><i class="fa fa-info-circle" aria-hidden="true"></i>  WHATSAPP:</span></h4></a>
                        <p>{{$publication->cellphone}}</p>
                    </div>
                    @break

                    @default  <!---->
                         
                @endswitch
				

    

            


				
									
			<div class="col-md-12">
				<br>
					<h4><i class="fa fa fa-comments-o" aria-hidden="true"></i> DEJAR UN COMENTARIO</h4>
						<br>
						<div class="input-field">
										<textarea class="form-control" name="texarea" id="message" rows="3" placeholder="Comentar..."></textarea>
										</div>
										<br>
										<button type="submit" id="btn_send_comment" class="btn btn-success">COMENTAR</button>
						<br>
						<br>
						<table class="table">
													
							<tbody id="body_messages">
                              
                                @foreach($publication->comments as $comment_id => $comments)
                                {{-- Process parent nodes --}}
                               @if($comments->child_id =='')
                               {{$comments->user_id}}
                               <tr style="margin-top: 0px;">
                                <td style="width: 10%"> <img  loading="lazy" 
                                    src="{{asset('storage/users/'.$comments->commenter->id."/".$comments->commenter->photo)}}"  style="max-width: 30px; min-width:30px; max-height: 30px; border-radius: 20px;" alt=""></td>
                                <td id="comment-{{$comments->id}}"><p><b>{{$comments->commenter->name}}:</b> {{$comments->comment}}</p>
                                <small><span style="color: #e5e5e5;"> <a href="javascript:;" class="add_like  @if($comments->user_id == Auth::user()->id) btn-active @endif" data-id="{{$comments->id}}" data-entity="comment"> 
                                    <small ><i class="fa fa-thumbs-up" aria-hidden="true"></i> me gusta</small> 
                                    <small id="comment_likes_{{$comments->id}}">{{$comments->likeCount}}</small></a> 
                                    <small> - <a href="javascript:;" class="add_comment" data-id="{{$comments->id}}" data-entity="comment">  responder </a>
                                    - Mensaje {{\Carbon\Carbon::parse($comments->created_at)->diffForHumans() }}</small></span></small>	<hr> 
                                    </td> 								 
                                </tr>
                               @endif
                              @if (count($comments->children) >0)
                         
                              @foreach ($comments->children as $children)
                            
                              <tr class="ml-4">
                                  <td style="width: 10%"></td>
                                  <td>
                                    <table>
                                        <tbody id="body_childrens">

                                        <tr >
                                            <td > <img  loading="lazy" 
                                                src="{{asset('storage/users/'.$children->commenter->id."/".$children->commenter->photo)}}"  style="max-width: 30px; min-width:30px; max-height: 30px; border-radius: 20px;" alt=""></td>
                                            <td id="children-{{$children->id}}"><p><b>{{$children->commenter->name}}:</b> {{$children->comment}}</p>
                                                <p><span style="color: #e5e5e5;"><a href="javascript:;" class="add_like  @if($children->liked()==1) btn-active @endif" data-id="{{$comments->id}}" data-entity="comment"> <small ><i class="fa fa-thumbs-up" aria-hidden="true"></i> me gusta</small> <small id="comment_likes_{{$children->id}}">{{$children->likeCount}}</small></a> <small> - <a href="javascript:;" class="add_comment" data-id="{{$children->id}}" data-entity="comment">  responder </a>- Mensaje {{\Carbon\Carbon::parse($children->created_at)->diffForHumans() }}</small></span></p>	<hr>
                                            </td>								 
                                              </tr>
                                              </tbody>
                                    </table>
                                  </td>

                              </tr>
                                 
                            @endforeach
                              @endif
                                      
                             
                            @endforeach
                                		
                        		</tbody>
						</table>
				<br>
			</div>
			
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 hidden-xs">
				<br>
				<br>
				<div class="hidden-xs col-md-12 col-sm-12 col-lg-12 center-block">
					<br>
					<h4><i class="fa fa-users" aria-hidden="true"></i> DONANTES </h4>
                        @foreach ($donors as $donor)
                        @if ($donor->users->hasRole('business'))
                        @else
                        @if ($donor->users->photo != '')
                        <img  loading="lazy" src="{{asset('storage/users/'.$donor->users->id.'/'.$donor->users->photo)}}" id="photo"
                         style="max-width: 30px; min-width:30px; max-height: 30px; border-radius: 20px;" alt="">
                        @endif
                        @endif

                        @endforeach				
                    <br>
                    <h4>EMPRESAS QUE PARTICIPAN </h4>

                    @foreach ($donors as $donor)
                    @if ($donor->users->hasRole('business'))
                    @if ($donor->users->photo != '')
                    <img  loading="lazy" src="{{asset('storage/users/'.$donor->users->id.'/'.$donor->users->photo)}}" id="photo"
                     style="max-width: 30px; min-width:30px; max-height: 30px; " alt="">
                    @endif
                    @endif
                 
                    @endforeach		
					<br>
				<br>
				<br>			
				</div>
				<hr>
                <br>
                @switch($publication->categories_id)
                    @case(1)
                    <h4><b>COMO AYUDAR EN ESTE CASO</b></h4>
                    <p class="text-center">Donando cosas materiales o dinero para implementar el plan de ayuda.</p>
                    <div class="text-center">
                        <a href="{{url('donacion-materiales')}}"> <button type="button" class="btn btn-success"> DONAR
                            MATERIALES</button></a>
                        <br>
                        <br>
                        <a href="{{url('aportes')}}"> <button type="button" class="btn btn-success"> DONAR
                                DINERO</button></a>
                    </div>
                    <br>
                    <br>
                        @break
                    @case(2)
                    <h4><b>COMO AYUDAR EN ESTE PROYECTO</b></h4>

                    <p class="text-center">Donando cosas materiales o dinero para implementar el plan de ayuda.</p>
                    <div class="text-center">
                        <a href="{{url('donacion-materiales')}}"> <button type="button" class="btn btn-success"> DONAR
                            MATERIALES</button></a>
                        <br>
                        <br>
                        <a href="{{url('aportes')}}"> <button type="button" class="btn btn-success"> DONAR
                                DINERO</button></a>
                    </div>
                    <br>
                    <br>
                        @break
                    @default
                        
                @endswitch
               
                <hr>
                <br>
                <div class="row">
                    <div class="hidden-xs col-md-12 col-sm-12 col-lg-12 center-block">
                        <div class="col-md-3 col-sm-3 col-lg-3 hidden-xs">
                            <a href="https://wa.me/541166701732" target="_blanck"> <button type="button"
                                    class="btn btn-secondary"><i class="fa fa-whatsapp"></i> ENVIAR
                                    WHATSAPP</button></a>
                        </div>
                    </div>

                </div>
            </div>

            <br>
            <br>
        </div>


        <div class="hidden-md hidden-sm hidden-lg col-xs-12 text-center">
            <br>
            @switch($publication->categories_id)
            @case(1)
            <h4><b>COMO AYUDAR EN ESTE CASO</b></h4>
            <p class="text-center">Donando cosas materiales o dinero para implementar el plan de ayuda.</p>
            <a href="{{url('donacion-materiales')}}"> <button type="button" class="btn btn-success"> DONAR
                MATERIALES</button></a>
            <br>
            <br>
            <a href="{{url('aportes')}}"> <button type="button" class="btn btn-success"> DONAR
                    DINERO</button></a>
            <br>
            <br>
                @break
            @case(2)
            <h4><b>COMO AYUDAR EN ESTE PROYECTO</b></h4>

            <p class="text-center">Donando cosas materiales o dinero para implementar el plan de ayuda.</p>
            <a href="{{url('donacion-materiales')}}"> <button type="button" class="btn btn-success"> DONAR
                MATERIALES</button></a>
            <br>
            <br>
            <a href="{{url('aportes')}}"> <button type="button" class="btn btn-success"> DONAR
                    DINERO</button></a>
            <br>
            <br>
                @break
            @default
                
        @endswitch
          
            <hr>
            <br>



            <div class="row">
                <div class=" hidden-md hidden-sm col-xs-12 center-block">
                    <div class="col-xs-4">
                    <a href="{{route('home')}}"><button type="button" class="btn btn-ligth  btn-sm"><i class="fa fa-reply"
                                    aria-hidden="true"></i> VOLVER</button></a>
                    </div>
                    <div class="col-xs-4">
                        <a href="https://wa.me/541166701732" target="_blanck"> <button type="button"
                                class="btn btn-ligth  btn-sm"><i class="fa fa-whatsapp"></i> WHATSAPP</button></a>
                    </div>
                    <div class="col-xs-4">
                        <a href="{{url('contacto')}}"><button type="button"
                                class="btn btn-ligth  btn-sm">CONTACTO</button></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 text-center">

            <div class="row">
                <br>
                <br>
                <div class="col-md-1 col-sm-1 col-xs-1">

                </div>

                <div class="col-md-2 col-sm-2 col-xs-2">
                    <p><a href="tel:541147861798 "><i class="fa fa-phone fa-2x" aria-hidden="true"></i></a></p>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-2">
                <p><a href="mailto: info@puentesocial.org?subject=Deseo%20Recibir%20más%20información&body=Quiero%20recibir%20mas%20información%20de {{$publication->name_case}}"
                            target="_blank"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></p>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-2">
                    <a onclick="sharing_link()"><i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></a>

                </div>

                <div class="col-md-2 col-sm-2 col-xs-2">
                    <a onclick="shareFacebook();"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-2">
                    <a onclick="shareTwitter()" id="btn_twitter"><i class="fa fa-twitter-square fa-2x"
                            aria-hidden="true"></i></a>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-1">

                </div>




            </div>
            @endforeach
        </div>

	<!-- Modal for portfolio item 1 -->
    <div class="modal fade" id="Modal-fotos" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1" style="color: #333;">{{$publication->code}} |<b style="color: #43484E;"> {{$publication->name_case}}</b></h4>
                </div>
                <div class="modal-body">
                                            
                        <p class="modal-title text-left"  style="color: #333;">{{$publication->details}}</p>
                        <br>
                        <div class="carousel slide text-center" data-ride="carousel" id="myCarousel1">
                             <!-- Indicators -->
                             <ol class="carousel-indicators">

                             @foreach ($photos as $photo)
                                <li data-target="#myCarousel" @if($photo->photo == $publication->avatar) class="active" @endif" data-slide-to="{{$photo->number}}" style="border-color: #333"></li>                         
                             @endforeach	
                         
                             </ol>
                                                <!-- Carousel items -->                                            
                                                <div class="carousel-inner">
                                                 
                                                    @foreach ($photos as $photo)
                                                    <div class="item @if($photo->photo == $publication->avatar) active @endif" data-slide-number="{{$photo->number}}">												
                                                        <img lazy="loading" src="{{asset("storage/publicaciones/".$publication->id."/".$photo->photo)}}" alt="img02"  style="width:100%; height:auto"/>
                                                    </div>
                                                
                                                    @endforeach			
                                                </div>
                                            <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev" style="font-size: 250%; color: #ffffff ; background: transparent !important;">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Anterior</span>
                                            </a>
                                            <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next" style="font-size: 250%; color: #ffffff;background: transparent !important;">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Siguiente</span>
                                            </a> 
                                           
                                        </div>
                                       
                        <div class="row">
                            <p class="text-center text-dark-puenteSocial" style="color: #333">
                                VER FOTOS</p> 

                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        </div>			
</div>

<!-- Modal for portfolio item 1 -->
    <div class="modal fade" id="Modal-podcast" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1" style="color: #333;">{{$publication->code}} |<b style="color: #43484E;"> {{$publication->name_case}}</b></h4>
                </div>
                <div class="modal-body">                    
                            <iframe id="podcast" src="{{$publication->link_podcast}}" height="102px" width="100%" frameborder="0" scrolling="no"></iframe>
                        <div class="row">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        </div>			
    </div>
<!-- MODAL PARA COMPARTIR LINK-->
<!-- Modal for portfolio item 1 -->
<div class="modal fade" id="Modal-compartir" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="Modal-label-1" style="color: #383737">COMPARTIR LINK</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12" id="slider">
                        <input type="text" class="form-control" id="input_sharing_link">
                        <button onclick="copy_link();" class="btn btn-primary">COPIAR</button>
                        <p id="msg_copy"></p>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--FIN DE MODAL PARA COMPARTIR LINK-->

<div id="fb-root"></div>
<input type="hidden" id="publication" value="{{$publication->id}}">
@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
    sessionStorage.setItem('link_paypal','{{$publication->link_paypal}}');
    sessionStorage.setItem('link_mp','{{$publication->link_mercado_pago}}');
    sessionStorage.setItem('link_donar_online','{{$publication->link_donar_online}}');

	var publication_id="{{$publication->id}}";
    window.fbAsyncInit = function () {
        FB.init({
            appId: '2564677387125919',
            cookie: true,
            xfbml: true,
            version: 'v7.0'
        });

        FB.AppEvents.logPageView();

    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/es_AR/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(document).on('click','#btn_send_comment',function(){
        let msj= $("#message").val();
        $.ajax({
                url         : '{{url("comments")}}',
                data        :  'commentable_id='+publication_id+'&commentable_type='+escape('App/publications')+'&message='+msj+'&_token='+'{{ csrf_token() }}',
                type        : 'POST',
                success: function(data, textStatus, jqXHR){
                    if (data != '') {
                      $("#body_messages").append(data);
                    }
                  },
                  error:function(data,message,res){
					  console.log("error");
                  },
                  statusCode:{
                    422:function(data) {
						console.log("error");
                    }
                  }
            });
    });

    function shareFacebook() {
        var url="{{route('sharink.link',['name'=>$publication->name_case,'id'=>$publication->id,'activate'=>'xcvasd'])}}";
        addShared();

        FB.ui({
            method: 'share',
            href: url,
        }, function (response) {});
    }

    window.twttr = (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function (f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));

    function shareTwitter() {
        addShared();

        let url = encodeURIComponent("{{route('sharink.link',['name'=>$publication->name_case,'id'=>$publication->id,'activate'=>'xcvasd'])}}");
        let texto ="{{$publication->text_twitter}}" //no olvidar que son 255 caracteres	
        let hashtags = "@puenteSocial, #{{$publication->name_case}}" //opcional
        window.open('https://twitter.com/intent/tweet?url=' + url + '&text=' + texto + '&hashtags=' + hashtags,
            '_blank');
    }

    function sharing_link() {
        let aux = document.getElementById("input_sharing_link");
        aux.value = "{{route('sharink.link',['name'=>$publication->name_case,'id'=>$publication->id,'activate'=>'xcvasd'])}}";

        try {
            let successful = document.execCommand('copy');
            let msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }

        $("#Modal-compartir").modal("show");
        aux.focus();
        aux.select();
    }

    function copy_link() {
        addShared();
        aux = document.getElementById("input_sharing_link");
        aux.focus();
        aux.select();
        try {
            let successful = document.execCommand('copy');
            let msg = successful ? 'successful' : 'unsuccessful';
            $("#msg_copy").text("Copiado al portapapeles!");
            console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }
    }

$(document).on('click','.add_like',function(){
    this.blur();
    var url,data=''
    let element=$(this);
    let id=$(this).data('id');
    let optionSelected=$(this).data('entity');
    if(optionSelected =='comment'){
         url="{{route('public.add.like.comment')}}";
         data='comment='+$(this).data('id')+'&_token='+'{{ csrf_token() }}';
    }else{
         url="{{route('public.add.like')}}";
         data='publication='+publication_id+'&_token='+'{{ csrf_token() }}';
    }
	$.ajax({
                url         : url,
                data        : data,
                type        : 'POST',
                dataType:"JSON",
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) { 
                    if(optionSelected=='comment'){
                        if(data.result==1){
                            element.addClass('btn-active');
                        }else{
                            element.removeClass('btn-active');

                        }
                        $("#comment_likes_"+id).text(data.result);
                    }else{
                        $(".likes").text(data.result);
                    }

                    }
                  },
                  error:function(data,message,res){
					  console.log("error");
                  },
                  statusCode:{
                    422:function(data) {
						console.log("error");
                    }
                  }
            });
});
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
function addShared(){
            $.ajax({
                url         : '{{route('public.add.sharing')}}',
                data        :  'publication='+publication_id+'&_token='+'{{ csrf_token() }}',
                type        : 'POST',
                dataType:"JSON",
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      $(".shared").text(data.result);
                    }
                  },
                  error:function(data,message,res){
					  console.log("error");
                  },
                  statusCode:{
                    422:function(data) {
						console.log("error");
                    }
                  }
            });
}

$(document).on('click','.add_comment',function(a) {
    let child=$(this).data('id');
 if($("#message_child_"+child).length  ==0){
    let html=` 	<div class="input-field">
				<textarea class="form-control" name="texarea" id="message_child_${child}" rows="3" placeholder="Comentar..."></textarea>
			    </div>
				<br>
				<button type="submit" data-child="${child}" class="btn btn-success btn_send_comment_in_child">COMENTAR</button>
				<br>`;
$(this).closest('tbody').append('<tr class="row_added"><td></td><td>'+html+'</td></tr>');

 }
});

$(document).on('click','.btn_send_comment_in_child',function(){
    var url,data=''
    let id=$(this).data('child');
    let optionSelected=$(this).data('entity');
    if(optionSelected =='comment'){
         url="{{route('public.add.like.comment')}}";
         data='comment='+$(this).data('id')+'&_token='+'{{ csrf_token() }}';
    }else{
         url="{{route('public.add.like')}}";
         data='publication='+publication_id+'&_token='+'{{ csrf_token() }}';
    }
	$.ajax({
                url         : url,
                data        : data,
                type        : 'POST',
                dataType:"JSON",
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) { 
                     
                    if(optionSelected=='comment'){
                        $("#comment_likes_"+id).text(data.result);
                    }else{
                        $(".likes").text(data.result);
                    }

                    }
                  },
                  error:function(data,message,res){
					  console.log("error");
                  },
                  statusCode:{
                    422:function(data) {
						console.log("error");
                    }
                  }
            });
});
$('#Modal-podcast').on('hidden.bs.modal', function (e) {
  let podcast=$("#podcast").attr('src');
  $("#podcast").attr('src',podcast);

})
</script>
@endsection
