@extends('layouts.public')

@section('content')
<div class="row">
		
    <br>		

    <div class="col-md-2 col-sm-2">
    
    
    </div>
    
    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
            
            
        <br>
        <h3><b>ENVIAR UN CASO</b></h3>						
                
            
        <form action="{{route('publicacion.save')}}" method="POST" enctype="multipart/form-data">
           @csrf
            @include('publicaciones.form-caso')
            </form>
        <br>
        <br>
        <p><small>Art 236. Falsificación material en documento público, por funcionario público. El funcionario público que ejerciendo un acto de su función, hiciere un documento falso o alterare un documento verdadero, será castigado con tres diez años de penitenciaría. Quedan asimilados a los documentos, las copias de los documentos inexistente y las copias infieles de documentos existentes. Art 237. Falsificación o alteración de un documento público, por un particular o por un funcionario, fuera del ejercito de sus funciones, hiciere un documento público flaso o alterare un documento público verdadero, será castigado con dos a seis años de penitenciaría.</small></p>
        
    </div>
    
    <div class="col-md-2 col-sm-2">
    
    
    </div>
    <br>               
    <br>	
</div>	

<div class="row">
    
    <hr>
    <div class="col-md-6 col-sm-6 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
        
        <br>
                <a href="https://wa.me/541130718986" target="_blanck"> <button type="button" class="btn btn-success">WHATSAPP 11 6670 1732   <i class="fa fa-whatsapp"></i></button></a>
                <br>
    
    
    
    </div>
    
    <div class="col-md-6 col-sm-6 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
        <br>
        <a href="mailto:info@puentesocial.org" target="_blanck"> <button type="button" class="btn btn-success">info@puentesocial.org  <i class="fa fa-envelope-o"></i></button></a>
            <br>
        
    </div>
    
    <div class="col-md-12 col-sm-12 text-center">
        <br>
    <p><small>(*) Nuestra Fundación posee el Certificado de Exención vigente, en el marco de lo dispuesto en la Resolución General Nº2681 (AFIP), lo que determina la procedencia de la exención aludida - en los casos en que se autorice la deducción de donaciones (Art. 81 c) - Validará para el donante la deducción en el </small></p>
    </div>

</div>
    <div class="row wow animated bounceInLeft box2 text-center" data-wow-delay="1.2s">
        <br>
        <br>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><i class="fa fa-phone fa-2x" aria-hidden="true"></i>							 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><a href="mailto: info@puentesocial.org?subject=Deseo%20Recibir%20más%20información&body=Quiero%20recibir%20mas%20información%20de%20Catriel" target="_blank"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></p>						 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><i class="fa fa-share-alt fa-2x" aria-hidden="true"></i>							 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i>							 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>							 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><i class="fa fa-instagram fa-2x" aria-hidden="true"></i>							 	
        </div>

    </div>


</div>  
@endsection