<br>
<div class="input-field">
    <input type="text" name="name" class="form-control" placeholder="Nombre y Apellido..." required>
</div>
<br>
<div class="input-field">
    <input type="text" name="email" class="form-control" placeholder="Email..." required>
</div>		
<br>
<div class="input-field">
    <input type="text" name="cellphone" class="form-control" placeholder="Teléfono celular..." required>
</div>	
<br>
<div class="input-field">
    <input type="text" name="city_province" class="form-control" placeholder="Ciudad y provincia...">
</div>										
<br>
<div class="input-field">
    <p><label name="oportunidad">Oportunidad ofrecida:</label>										
        <select type="text" class="form-control" name="oportunity_type">        
            @foreach ($oportunity_types as $oportunity_type)
    <option value="{{$oportunity_type->id}}">{{$oportunity_type->name}}</option> 
        @endforeach
    </select>
</div>
<br>
<div class="input-field">
    <textarea class="form-control" name="initial_details" id="initial_details" rows="6" placeholder="Detalle de la oportunidad que desea publicar (máximo 300 palabras)..."></textarea>
</div>
<br>
<div class="col-lg-12 col-md-12 col-12">
    <div class="input-field">
        <label for="">Fotografías: <span></span></label>
      <input type="file" name="photos[]" id="photo" class="form-control" multiple>
    </div>
  </div>
  <br>
    <p><small>Enviando éste formulario autorizo a Fundación Espiga a utilizar la información que por este medio proveo para publicar en sus redes sociales y gestionar.</small></p>
    <br>
    
    <button type="submit" id="submit" class="btn btn-lg btn-success">Enviar</button>
<br>
<br>