@extends('layouts.public')
@section('content')
<div class="row">
				
    <div class="col-md-2 col-lg-2 col-sm-2">
    
    </div>
    
    
  <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">  

            <div class="section-title text-center">
                <br>
                <br>						
                
            <h3><b>¡GRACIAS POR ENVIAR TU @if(Session::has('categoria_publicacion')) {{Session::get('categoria_publicacion')}} @endif!</b></h3>
                <p>Te estamos mandando un mail para confirmar toda la información que nos mandaste. Respondelo a la brevedad así nuestro equipo verifica toda para poder publicarlo.</p>
                <br>
                <br>
                
                <p><small><ul>Por cualquier duda ponete en contacto con nosotros, estamos a disposición</ul></small></p>
                
                <br>
                <br>
                <a href="https://wa.me/541130718986" target="_blanck"> <button type="button" class="btn btn-success"><b>11 6670 1732</b> <i class="fa fa-whatsapp"></i></button></a>
                <br>
                <br>
                <a href="#" target="_blanck"> <button type="button" class="btn btn-success"> 11 4786 1786   <i class="fa fa-phone"></i></button></a>
                <br>
                <br>
                <a href="mailto:info@puentesocial.org" target="_blanck"> <button type="button" class="btn btn-success">info@puentesocial.org  <i class="fa fa-envelope-o"></i></button></a>
        <br>
                
            
            </div>
        </div>
    </div>
  
    <div class="col-md-2 col-lg-2 col-sm-2">
    
    </div>
@endsection