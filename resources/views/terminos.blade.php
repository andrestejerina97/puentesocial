<!DOCTYPE html>
<html lang="en">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="PuenteSocial es una plataforma creada con el objetivo de unir partes para lograr un mundo más equitativo. Una de los grandes problemas sociales que enfrentamos es la desigualdad, lo que provoca que millones de personas no puedan desarrollarse. Este es un derecho universal que vemos vulnerado en personas que quedan marginadas y sin ninguna posibilidad." /> 
	<meta name="author" content="Hamilton-group"> 
	<title>PUENTE SOCIAL</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/prettyPhoto.css" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link href="css/main.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet"> 
	<link href="css/mapa.css" rel="stylesheet"> 
    

    <link rel="stylesheet" href="css/owl.carousel.css">
    <link href="css/infinite-slider.css" rel="stylesheet">
    <link href="css/social.css" rel="stylesheet"> 
    
    <link href="css/animate.css" rel="stylesheet"> 
	<script src="js/wow.min.js"></script>

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="favicon.ico"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-SWGHXDL85E"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-SWGHXDL85E');
	</script>
	
	<script>
	 new WOW().init();
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	
	
</head><!--/head-->
<body>
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner"> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
		
			</div>
		</div>
	</div><!--/.preloader-->
	<header id="navigation"> 
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner"> 
			<div class="container"> 
				<div class="navbar-header"> 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
					</button> 
					
                    <a class="navbar-brand" href="index.html"><h1><span style="color: #fff;"><img src="images/iconos/logo-puente.png"></span></h1></a> 
				</div> 
				<div class="collapse navbar-collapse"> 
			
						<ul class="nav navbar-nav navbar-right"> 
				        <li><a class="page-scroll" href="index.html"><i class="fa fa-reply" aria-hidden="true"></i></a></li>                        
						<li><a class="scroll" href="subir-caso.html">SUBIR CASO</a></li>  
                        <li><a class="scroll" href="contacto.html">CONTACTO</a></li>
                        <li class="scroll"><a href="https://www.facebook.com/puentesocial.org/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="scroll"><a href="https://www.instagram.com/puentesocial/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li class="scroll"><a href="https://wa.me/541166701732" target="_blanck"><i class="fa fa-whatsapp"></i></a></li>
											
					</ul>	
				</div>				
			</div>			
		</div><!--/navbar--> 
	</header> <!--/#navigation--> 
    
    

	<section id="home">
        <br>
        <br>
   

	</section><!--/#home-->
  

  <!-- Start Service area -->
  <section id="about-us" class="about-area area-padding">
    <div class="container">	
		
			
		
			<div class="col-md-12">	
				
				<h2><b>LEGALES</b></h2>
				<h4>DATOS GENERALES</h4>
				<p>FUNDACIÓN ESPIGA es una Entidad de Bien Público sin fines de lucro, con Personería Jurídica obtenida por Resolución de la Inspección General de Justicia Nº 000986, con fecha 22 de Septiembre de 1998. Inscripta en la Administración Federal de Ingresos Públicos (AFIP); en el REPOC, Registro de Organizaciones Comunitarias de la Provincia de Buenos Aires; en el R.U.O., Registro Único de Organizaciones de la Sociedad Civil de la Provincia de Buenos Aires y en el Registro de Entidades de Bien Público de la Municipalidad de Vicente López. </p>
				<br>
				<h4>CONDICIONES DE USO</h4>
				<p>Las condiciones de acceso y uso del presente sitio web y app se rigen por la legalidad vigente y por el principio de buena fe comprometiéndose el usuario a realizar un buen uso de la web. No se permiten conductas que vayan contra la ley, los derechos o intereses de terceros.
					Ser usuario de la red Puente Social implica que reconoce haber leído y aceptado las presentes condiciones y lo que las extienda la normativa legal aplicable en esta materia. Si por el motivo que fuere no está de acuerdo con estas condiciones no continúe usando esta red.
					Cualquier tipo de notificación y/o reclamación solamente será válida por notificación escrita y/o correo certificado a la dirección: <a href="mailto: info@puentesocial.org"><span style="color: #8ae58a;">info@puentesocial.org</span></a></p>
               	<br>
				<h4>TERMINOS Y CONDICIONES</h4>
				<p>PUENTE SOCIAL (en adelante “La Plataforma”) es una red social de propiedad de Fundación Espiga. Tiene como propósito brindar una herramienta para todas aquellas personas y/o empresas que desean brindar su ayuda a diferentes problemáticas de impacto social, cultural, laboral o de impacto ambiental. Se ofrecen diferentes propuestas y oportunidades para la promoción del desarrollo sustentable y crecimiento de la equidad social. En la Plataforma, los usuarios podrán interactuar entre sí a fin de obtener los datos de contactos y requerimientos necesarios para brindar la colaboración ofrecida.
					<br>
					El Usuario garantiza y declara ser mayor de 18 años, y/o en su caso, que tiene la expresa autorización de sus padres o de sus tutores para poder acceder al sitio web y que es competente para entender y aceptar sin reservas las obligaciones, afirmaciones, representaciones y garantías establecidas en estos Términos y Condiciones. </p>
				<br>
				<h4>POLÍTICA DE PRIVACIDAD</h4>
				<p>En cumplimiento de lo dispuesto en Ley 25.326 sobre Protección de Datos Personales se informa al usuario que todos los datos que nos proporcione serán incorporados a un fichero, creado y mantenido bajo la responsabilidad de la Fundación de acuerdo a lo dispuesto por el art. 9 de la citada ley. Vulnerar los derechos establecidos en la Ley N° 25.326 de Protección de Datos Personales</p>
				<br>
				<p>Siempre se va a respetar la confidencialidad de sus datos personales que sólo serán utilizados con la finalidad de gestionar los servicios ofrecidos, atender a las solicitudes que nos plantee, realizar tareas administrativas controladas y dirigidas por Fundación Espiga.</p>
				<br>	
				<p>Para ejercer sus derechos de oposición, rectificación o cancelación deberá dirigirse a: <a href="mailto: info@puentesocial.org"><span style="color: #8ae58a;">info@puentesocial.org</span></a></p>
				<br>
				<h4>RESPONSABILIDADES</h4>
				<p>Cada organización, empresa y/o usuario individual posee y es responsable de las contribuciones que publica en o a través de nuestras ofertas, incluyendo las contribuciones que publica y hace disponibles para que otros usuarios. </p>
				<br><p>
				FUNDACION ESPIGA no se hace responsable de la información y contenidos almacenados en foros, redes sociales o cualesquiera otros medios que permita a terceros publicar contenidos de forma independiente en la plataforma.</p>
				<p>Sin embargo, Puente Social se compromete a la retirada o en su caso bloqueo de aquellos contenidos que pudieran afectar   derechos de terceros o la moral y el orden público cuando los mismos sean denunciados por quien tenga interés legitimo y/o cuando lo considere menester.</p>
				<p>Tampoco se responsabilizará de los daños y perjuicios que se produzcan por fallos o malas configuraciones del software instalado en el ordenador del internauta. Se excluye toda responsabilidad por alguna incidencia técnica o fallo que se produzca cuando el usuario se conecte a internet. Igualmente, no se garantiza la inexistencia de interrupciones o errores en el acceso al sitio.</p>
				<p>Así mismo, Fundación Espiga se reserva el derecho a actualizar, modificar o eliminar la información contenida en su plataforma, así como la configuración o presentación de este, en cualquier momento sin asumir alguna responsabilidad por ello.</p>
				<p>Muchos de los servicios puestos a disposición permiten que los Usuarios compartan información entre sí. A su vez, el Usuario puede vincular su cuenta en el sistema de identificación único con sus perfiles en redes sociales, asumiendo con ello la privacidad y veracidad de los datos declarados en dichas suscripciones.</p> 
				<p>Especialmente:</p>
				
				<p><b>Google</b></p>
				<p>•	El Usuario puede crear su cuenta en el sistema de identificación único y relacionarla con su cuenta de Google.
				<br>•	Google podrá proporcionar al Administrador información sobre el Usuario según lo definido en la política de Privacidad de Google.
				<br>•	El Usuario puede eliminar la relación entre su cuenta en el sistema de identificación único y su Cuenta de Google a través de una opción en la configuración del sistema en cualquier momento.
					<br>•	El Administrador acepta y cumple con la Política de Privacidad definida por Google, a la se puede acceder a través del siguiente enlace: <a href="https://www.google.com/intl/es_ar/policies/privacy" target="_blank"><span style="color: #8ae58a;"> https://www.google.com/intl/es_ar/policies/privacy.</span></a></p>
					<br>
				<p><b>Facebook</b></p>
					<p>•	El Usuario puede crear su cuenta en el sistema de identificación único y relacionarla con su cuenta de Facebook.
					<br>•	Facebook podrá proporcionar al Administrador información sobre el Usuario según lo definido en la política de Privacidad de Facebook.
					<br>•	El Usuario puede eliminar la relación entre su cuenta en el sistema de identificación único y su cuenta de Facebook a través de una opción en la configuración del sistema en cualquier momento.
					<br>•	El Administrador cumple con la Política de Privacidad definida por Facebook, a la que se puede acceder a través del siguiente enlace: <a href="https://www.facebook.com/about/privacy" target="_blank"><span style="color: #8ae58a;"> https://www.facebook.com/about/privacy.</span></a></p>
					<p>Fundación Espiga no controla ni es responsable por accionar de otros sitios web y otros productos y servicios a los cuales se puede acceder por medio de los Servicios Digitales, por lo que recomendamos la lectura de la Política de Privacidad de cada uno de ellos. 
					<br>Los usuarios expresan su consentimiento por la publicación de imágenes en las cuales aparezcan individualmente o en grupo que con carácter promocional se puedan realizar dentro de La Plataforma con el objeto de difundir las diferentes propuestas ofrecidas.</p>

				<br>
				<h4>PROPIEDAD INTELECTUAL E INDUSTRIAL</h4>
				<p>Puente Social es titular de todos los derechos sobre el software de la publicación digital, así como de los derechos de propiedad industrial e intelectual referidos a los contenidos que se incluyan, a excepción de los derechos sobre productos y servicios de carácter público que no son de su propiedad.</p>
				<br>
				<h4>LEY APLICABLE Y JURISDICCIÓN</h4>
				<p>Las presentes condiciones generales se rigen por la legislación de la República Argentina. Para cualquier litigio que pudiera surgir relacionado con el sitio web o la actividad que en él se desarrolla serán competentes Juzgados de la Ciudad Autónoma de Buenos Aires, renunciando expresamente el usuario a cualquier otro fuero que pudiera corresponderle.</p>
			</div>
	  </div>

			
	</section>

		
		
		

	<footer id="footer"> 
		<div class="container"> 
			<div class="text-center"> 
					<div class="social-icons">
						<a href="https://www.facebook.com/puentesocial.org/" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="https://www.instagram.com/puentesocial/" target="_blank"><i class="fa fa-instagram"></i></a>
						<a href="https://twitter.com/"><i class="fa fa fa-twitter"></i></a>
						<a href="https://anchor.fm/fundacion-espiga" target="_blank"><i class="fa fa fa-podcast"></i></a>
					</div>
				<br>
				 <p>&copy; Copyright <strong>Puente Social</strong>. Todos los derechos reservados.</p>
			</div> 
		</div> 
	</footer> <!--/#footer--> 
	
	<!-- Modal for portfolio item 1 -->
		<div class="modal fade" id="Modal-1" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="Modal-label-1">TRANSFERENCIA BANCARIA:</h4>
					</div>
					<div class="modal-body">						
						<br>						
						<p>Podes hacerlo por tu home banking o cajero automático. Los datos que necesitas son: Fundación Espiga CUIT.: 30-69939101-4
							<br>Banco Provincia Cuenta Corriente CBU: 0140046501517402127426</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

	<script type="text/javascript" src="js/jquery.js"></script> 
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script> 
	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script> 
	<script type="text/javascript" src="js/jquery.parallax.js"></script> 
	<script type="text/javascript" src="js/main.js"></script> 
	<script src="js/wow.min.js"></script>
    
     <script src="js/owl.carousel.min.js"></script>
        
    <!-- carousel logos empresas -->   	
    <script type="text/javascript" src="js/carousel.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
      <!-- end - carousel logos empresas -->  
    
    
    
</body>
</html>