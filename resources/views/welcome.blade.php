@extends('layouts.public-2')
@section('css')
  <style>
  .color-green{
      color:rgb(4, 143, 50);
  }
  #Modal-label-1{
    display: block;
    margin-block-start: 1.33em;
    margin-block-end: 1.33em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    font-weight: bold;
    color:#43484E !important;
  }
  .flex-row-mobile{
    display: flex;
    justify-content: end;
    color: black;
    margin-bottom: 10px;
    width: 100%;
    align-content: space-between;
    align-items: end;
    flex-direction: row;
    font-size: 75%;
  }
  .flex-row{
    display: flex;
    justify-content: end;
    color: black;
    text-transform: uppercase;
    margin-bottom: 8px;
    width: 90%;
    align-content: space-between;
    align-items: end;
    flex-direction: row;
    font-size: 110%;
  }
  .flex-row span{
    margin-left: 20px;
  }
  .flex-row-mobile span{
    margin-left: 20px;
  }
  </style>  
@endsection
@section('content')
<div class="social hidden-xs hidden-sm">
    <ul>
        <li class="scroll"><a href="https://www.facebook.com/puentesocial.org/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li class="scroll"><a href="https://www.instagram.com/puentesocial/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li class="scroll"><a href="https://wa.me/5491121646003" target="_blanck"><i class="fa fa-whatsapp"></i></a></li>
    </ul>
</div>
<div class="sub-filter">
    @php
$html_cases='';
$html_proyects='';
$html_oportunities='';
@endphp
<div class="social hidden-lg hidden-md ">
    <ul class="casos-subfilter">
        @foreach ($case_categories as $case_category)
        <li class="scroll">
            <a href="javascript:;" data-id="{{$case_category->id}}" data-category="casos" class="subfilter"><i class="{{$case_category->icon}}" aria-hidden="true"></i></a>
        </li>
  
        @php
         $html_cases.='<td >
        <a href="javascript:;" data-id="'.$case_category->id.'" style="margin-right: 25px;"  data-category="casos" class="subfilter"><i class="'.$case_category->icon.'" aria-hidden="true"></i></a>
        </td>';
        @endphp
        @endforeach
        <li class="scroll">
            <a href="javascript:;" data-id="{{$case_category->id}}" data-category="casos"  class="subfilter_back"><i class="fa fa-reply" aria-hidden="true"></i></a>
    
        </li>
    </ul>
  
</div>

<div class="social hidden-lg hidden-md ">
    <ul class="proyectos-subfilter">
@foreach ($proyect_categories as $case_category)
<li class="scroll">
    <a href="javascript:;" data-category="proyectos" data-id="{{$case_category->id}}" class="subfilter" ><i class="{{$case_category->icon}} " aria-hidden="true"></i></a>
</li>

@php
$html_proyects.=' <td >
    <a href="javascript:;" data-category="proyectos" data-id="'.$case_category->id.'" style="margin-right: 25px;" class="subfilter"><i class="'.$case_category->icon.'" aria-hidden="true"></i></a>
    </td>';
@endphp

@endforeach
<li class="scroll">
    <a href="javascript:;" data-category="proyectos" data-id="{{$case_category->id}}"  class="subfilter_back"><i class="fa fa-reply" aria-hidden="true"></i></a>

</li>
    </ul>
</div>
<div class="social hidden-lg hidden-md ">
    <ul class="oportunidades-subfilter">
@foreach ($oportunity_categories as $case_category)
<li class="scroll">
    <a href="javascript:;" class="subfilter" data-category="oportunidades" data-id="{{$case_category->id}}" ><i class="{{$case_category->icon}}" aria-hidden="true"></i></a>
</li>

@php
 $html_oportunities.='
 <td >
<a href="javascript:;" class="subfilter" data-category="oportunidades" data-id="'.$case_category->id.'" style="margin-right: 25px;"><i class="'.$case_category->icon.'" aria-hidden="true"></i></a>
</td>
 '   
@endphp
@endforeach
<li class="scroll">
    <a href="javascript:;" data-category="oportunidades" data-id="{{$case_category->id}}" class="subfilter_back"><i class="fa fa-reply" aria-hidden="true"></i></a>

</li>
</ul>
</div>
</div>
@include('partials.row-categories')

<div class="row sub-filter" style="margin-bottom: 10px; margin-top: 10px;">
    <div class="col-md-6 col-lg-6 col-xs-12  col-sm-12 text-center">
        <input id="input_search_publication" style="background-color: rgb(4, 143, 50); border:none; color: #ffffff !important; font-size:90%;" type="text" value="PROVINCIAS O CIUDADES" placeholder="PROVINCIAS O CIUDADES">
        <button type="button" id="btn_search_publication" style=" 
        border-width: 0;
        border-radius: 2px;
        background-color: rgb(4, 143, 50) !important;
        box-shadow: 0 1px 4px rgba(5, 151, 71, 0.6);
        border: none;
          color: #ffffff">
          <i class="fa fa-search"></i></button>
    </div>
   

    <div class="col-md-6 col-lg-6 hidden-xs  hidden-sm">
<table >
    <tr class="casos-subfilter">
         {!!$html_cases!!}   
      
    <td >
        <a href="javascript:;" data-id="{{$case_category->id}}" data-category="casos" style="margin-right: 25px;" class="subfilter_back"><i class="fa fa-reply" aria-hidden="true"></i></a>
        </td>
</tr>
<tr class="proyectos-subfilter">
      {!!$html_proyects!!}
        <td >
            <a  href="javascript:;" style="margin-right: 25px;" data-category="proyecto" class="subfilter_back"><i class="fa fa-reply" aria-hidden="true"></i></a>
            </td>
    </tr>
    <tr class="oportunidades-subfilter">
           {!!$html_oportunities!!}
            <td >
                 <a href="javascript:;" style="margin-right: 25px;" data-category="oportunidades" class="subfilter_back"><i class="fa fa-reply" aria-hidden="true"></i></a>
                </td>
        </tr>
</table>
</div>
</div>
    
<div class="row row-0-gutter wow animated fadeIn text-center " data-wow-delay="0.6s">	
    <!-- start portfolio item -->
   @foreach ($publications as $publication)
   @php
    $places[]=['prov'=>strtolower($publication->city_province),'id'=>$publication->id];   
   @endphp
   <div data-id="{{$publication->id}}" class="col-md-2 col-xs-4 col-0-gutter publication  {{$publication->category->class}}
    @switch($publication->categories_id) 
        @case(1)
            @foreach ($publication->subcases as $subcase)
            {{$publication->category->class}}_{{$subcase->case_categories_id}} 
            @endforeach
            @break
        @case(2)
        @foreach ($publication->subproyects as $subproyect)
        {{$publication->category->class}}_{{$subproyect->proyect_categories_id}} 
        @endforeach
            @break
        @case(3)
        @foreach ($publication->suboportunities as $suboportunity)
        {{$publication->category->class}}_{{$suboportunity->oportunity_categories_id}} 
        @endforeach
        @break
        @default
            
    @endswitch
    ">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
        <img loading="lazy" src="{{asset("storage/publicaciones/".$publication->id."/".$publication->avatar)}}" alt="img02" class="img-responsive" />
        <figcaption>
                <h2>{{$publication->name_case}}</h2>
                <p>{{$publication->opening_sentence}}</p>
                <a href="" data-toggle="modal" data-target="#modal-{{$publication->id}}">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
	<!-- Modal for portfolio item 13-->

   @endforeach
</div>

@foreach ($publications as $publication)

<div class="modal fade" id="modal-{{$publication->id}}" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom: 0px">						
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="Modal-label-1" style="color: #333; ">{{$publication->code}}<b style="color: #43484E;"> {{$publication->name_case}}</b></h4>
            </div>
            <div class="modal-body text-center" style="padding-top: 0px">
                <img lazy="loading" src="{{asset("storage/publicaciones/".$publication->id."/".$publication->avatar)}}" alt="img02" controls style="width:100%; height:auto"/>
                <br>
                <h3 style="color: #333;">{{$publication->opening_sentence}}</h3>
                @if($publication->categories_id ==4)
                <h5 style="color: #333; margin-top:10px;">{{$publication->details}}</h5>
                <div class="row ">
                    <br>
                    <br>
                    <div class="col-md-1 col-sm-1 col-xs-1">
    
                    </div>
    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <p><a href="tel:541147861798 "><i class="fa fa-phone fa-2x color-green" aria-hidden="true"></i></a></p>
                    </div>
    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                    <p><a href="mailto: info@puentesocial.org?subject=Deseo%20Recibir%20más%20información&body=Quiero%20recibir%20mas%20información%20de {{$publication->name_case}}"
                                target="_blank"><i class="fa fa-envelope fa-2x color-green" aria-hidden="true"></i></a></p>
                    </div>
    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <a onclick="sharing_link('{{route('sharing.bridge')}}','{{$publication->code}}');"><i class="fa fa-share-alt-square fa-2x color-green" aria-hidden="true"></i></a>
    
                    </div>
    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <a onclick="shareFacebook('{{route('sharink.link',['name'=>$publication->name_case,'id'=>$publication->id,'activate'=>'xcvasd'])}}','{{$publication->code}}');" class="color-green" id="btn_facebook"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                    </div>
    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        
                        <a onclick='shareTwitter("{{$publication->code}}","{{$publication->details}}","{{route("sharing.bridge")}}","{{$publication->name_case}}")' class="color-green" id="btn_twitter"><i class="fa fa-twitter-square fa-2x"
                                aria-hidden="true"></i></a>
                    </div>
    
                    <div class="col-md-1 col-sm-1 col-xs-1">
    
                    </div>

                </div>
                @endif
                <br>
                @if($publication->categories_id !=4)
                <a href="{{route('publication.view',$publication->id)}}"><button type="button" class="btn btn-success">VER </button></a>
                @endif
                
            </div>
            
        </div>
    </div>			
</div>
@endforeach

	<!-- Modal for portfolio item casos-->
	<div class="modal fade" id="casos" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content2">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                
                <div class="modal-body text-center" style="margin-bottom: 0px; padding-top: 0px">
                    <h3 class="modal-title text-center"  style="color: #ffffff !important; font-weight: bold; "> <i class="fa fa-heart-o " aria-hidden="true"></i> PUENTES PARA CONSTRUIR</h3>
                    <br>
                    <span>Los casos se clasifican asi:</span><br>
                    <small>(Tocá sobre cada ícono para ver todos los casos)</small>
                    <br><br>
                    @foreach ($case_categories as $case_category)
                    <div class="row text-center m-1">
                        <a href="javascript:;" data-id="{{$case_category->id}}"
                            data-category="casos" class="subfilter">
                            <i class="{{$case_category->icon}}"></i>
                        </a>
                           {{$case_category->name}} 
                    </div>                                          
                    @endforeach

                    <hr>
                    <button type="button" class="btn btn-success" class="close" data-dismiss="modal" aria-label="Close"><h4> VER CASOS</h4></button>

                                        
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item PROYECTOS-->
<div class="modal fade" id="proyectos" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content2">
                <div class="modal-header">	
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center" style="margin-bottom: 0px; padding-top: 0px">
                    <h3 class="modal-title text-center"  style="color: #ffffff !important; font-weight: bold; "> <i class="fa fa-bullhorn " aria-hidden="true"></i> PROYECTOS SOCIALES</h3>
                    <br>
                    <h5>IDEAS QUE PUEDEN TRANSFORMAR REALIDADES <br> Y CONSTRUIR UN MUNDO MEJOR</h5> 					
                    <br>
                    <span>Los proyectos se clasifican asi:</span><br>
                    <small>(Tocá sobre cada ícono para ver todos los proyectos)</small>
                    <br>
                   <br>
                   
                    @foreach ($proyect_categories as $case_category)

                    <div class="row text-center m-1">
                        <a href="javascript:;" data-id="{{$case_category->id}}"
                         data-category="proyectos" class="subfilter">
                         <i class="{{$case_category->icon}}"></i>
                        
                        </a>
                        {{$case_category->name}} 

                       
                    </div>                                          
                    @endforeach
                   
                
                <hr>

                <button type="button" class="btn btn-success" class="close" data-dismiss="modal" aria-label="Close"><h4>VER PROYECTOS</h4></button>
                <br>
                 <!--   <hr>
                    <p>Todos podemos ayudar de diferentes maneras con cosas materiales o un aporte económico para implementar el plan de ayuda (*)</p>
                    <hr>
                    <psmall>(*) Las donaciones a FUNDACION ESPIGA (CUIT: 30-69939101-4, Certificado AFIP #412020058801) son deducibles del Impuesto a las Ganancias según el Art. 81 de la Ley N° 20.628, con vigencia desde 01/01/2020 hasta el 31/12/2021.</psmall>
                    <br>
                    <h4><b>MAS DE 500 PUENTES REALIZADOS!</b></h4>-->
                                        
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item OPORTUNIDAD-->
<div class="modal fade" id="oportunidad" tabindex="-1" role="dialog" aria-labelledby="Modal-label-2">
        <div class="modal-dialog" role="document">
            <div class="modal-content2">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center" style="margin-bottom: 0px; padding-top: 0px">
                    <h3 class="modal-title text-center"  style="color: #ffffff !important; font-weight: bold; "> <i class="fa fa-diamond" aria-hidden="true"></i> DONACIONES Y OPORTUNIDADES</h3>
                    <br> 
                    <h5>PERSONAS, EMPRESAS Y ORGANIZACIONES <br>QUE COMPARTEN POR UN MUNDO MEJOR</h5> 					
                    <br>
                    <span>Las oportunidades se clasifican asi:</span><br>
                    <small>(Tocá sobre cada ícono para ver todas las oportunidades)</small>
                    <br>
                    <br>
                    @foreach ($oportunity_categories as $case_category)
                    <div class="row text-center m-1">
                        <a href="javascript:;" data-id="{{$case_category->id}}"
                            data-category="oportunidades" class="subfilter">
                            <i class="{{$case_category->icon}}"></i>
                           
                           </a>
                           {{$case_category->name}}

                    </div>                                          
                    @endforeach
                    
                    <hr>
                    <button type="button" class="btn btn-success" class="close" data-dismiss="modal" aria-label="Close"><h4>VER OPORTUNIDADES</h4></button>              
                </div>
            </div>
        </div>	
</div>

<!-- Modal presentación-->
<div class="modal fade" id="presentacion" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center" style="margin-bottom: 0px; padding-top: 0px">
                <div style="position: relative !important;top: -2% !important;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>

                </div>
 
                <h3 class=" text-center " style="" id="Modal-label-1" >PUENTE SOCIAL   </h3>
                <p style="color: #333; margin-bottom: 4%" >
                    Una red social que tiene como objetivo hacer vínculos que serán puentes, para lograr un mundo mejor.
                </p>
     
                 
               <div class=" hidden-xs  ">
                <div class="flex-row  ">
                    <i class="fa fa-heart-o fa-2x " ></i>
                    <span class="text-left">Personas, familias o comunidades que necesitan y merecen ayuda.</span>
                    </div>
                   
                    <div class="flex-row ">
                    <i class="fa fa-bullhorn fa-2x " ></i>
                    <span class="text-left">Proyectos sociales, comunitarios, solidarios, ecológicos y ambientales.</span>
                    </div>
                    <div class="flex-row  ">
                        <i class="fa fa-diamond fa-2x " ></i>
                        <span class="text-left">Oportunidades, empleos, donaciones, capacitaciones y voluntariado.</span>
                    </div>
                    <div class="flex-row  ">
                        <i class="fa fa-heart fa-2x " ></i>
                        <span class="text-left">Todos los puentes construidos estan aqui y entre todos iremos sumando.</span>
                    </div> 
                    <div class="flex-row  ">
                        <i class="fa fa-cloud-upload fa-2x " ></i>
                        <span class="text-left">Todos pueden publicar sus casos, proyectos o donaciones para compartir.</span>
                    </div> 
                </div>    
               <div class="hidden-lg hidden-md hidden-sm">
                <div class="flex-row-mobile  ">
                    <i class="fa fa-heart-o fa-2x " ></i>
                    <span class="text-left">Personas, familias o comunidades que necesitan y merecen ayuda.</span>
                    </div>
                   
                    <div class="flex-row-mobile ">
                    <i class="fa fa-bullhorn fa-2x " ></i>
                    <span class="text-left">Proyectos sociales, comunitarios, solidarios, ecológicos y ambientales.</span>
                    </div>
                    <div class="flex-row-mobile ">
                        <i class="fa fa-diamond fa-2x " ></i>
                        <span class="text-left">Oportunidades, empleos, donaciones, capacitaciones y voluntariado.</span>
                    </div>
                    <div class="flex-row-mobile  ">
                        <i class="fa fa-heart fa-2x " ></i>
                        <span class="text-left">Puentes construidos que se irán sumando con el trabajo de todos.</span>
                    </div> 
                    <div class="flex-row-mobile  ">
                        <i class="fa fa-cloud-upload fa-2x " ></i>
                        <span class="text-left">Publicar casos, proyectos o donaciones. Todos podemos.</span>
                    </div> 
                </div>    
               
                <p style="color: #333; margin-bottom: 0px; margin-top: 5%;">
                    Puente Social es un proyecto independiente, sin fines de lucro, sesgo político o religioso alguno. Su principio y final está en la concepción esencial del ser humano, promoviendo la igualdad de oportunidades
                    para todas las personas como derecho universal
                </p>
                <br>
                <button type="button" class="btn " class="close" data-dismiss="modal" aria-label="Close"><h4>BIENVENIDOS</h4></button>              
            </div>
        </div>
    </div>	
</div>


    <!-- Modal for portfolio item OPORTUNIDAD-->
<div class="modal fade" id="puente" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content2">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center">	
                    <h3 class="modal-title text-center" id="Modal-label-1" style="color: #ffffff !important"> <i class="fa fa-heart" aria-hidden="true"></i> PUENTES REALIZADOS</h3>
                    <br>
                    <h5>CADA PUENTE REALIZADO ES LA <br> TRANSFORMACIÓN DE UNA O VARIAS VIDAS</h5> 					
                    <br>
                    
                    <h5></h5> 					

                    <button type="button" class="btn btn-success" class="close" data-dismiss="modal" aria-label="Close"><h4>VER PUENTES</h4></button>
                    <br>
                    <hr>
               
                    <h4><b>EN SOLO 5 AÑOS PUDIMOS CONSTRUIR MÁS<br> DE 500 PUENTES.</b></h4>
                                        
                </div>
                
            </div>
        </div>			
</div>


   <!-- Modal for portfolio item OPORTUNIDAD-->
   <div class="modal fade" id="modal_error" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content2">
            <div class="modal-header">						
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-center" id="Modal-label-1">=( Ups!</h3>
            </div>
            <div class="modal-body text-center">	

                @if (session()->has('error'))
                  <h3 class="text-center">{{session()->get('error')}}</h3>
                @endif               
            </div>
            
        </div>
    </div>			
</div>


<!-- MODAL PARA COMPARTIR LINK-->
<!-- Modal for portfolio item 1 -->
<div class="modal fade" id="Modal-compartir" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="Modal-label-1" style="color: #383737">COMPARTIR LINK</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center" id="slider">
                        <input type="text" class="form-control" id="input_sharing_link">
                        <button onclick="copy_link();" class="btn btn-primary">COPIAR</button>
                        <p id="msg_copy" class="color-green"></p>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--FIN DE MODAL PARA COMPARTIR LINK-->
@endsection
@section('scripts')
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
     window.fbAsyncInit = function () {
        FB.init({
            appId: '2564677387125919',
            cookie: true,
            xfbml: true,
            version: 'v7.0'
        });

        FB.AppEvents.logPageView();

    };
        $(function(){
        $(".sub-filter").hide();
    });

    $(document).on('click','.subfilter',function(){
        let id=$(this).data('id');
        let category=$(this).data('category');
        ocultar('publication');
        mostrar(category+'_'+id);

    });
    $(document).on('click','.subfilter_back',function(){
        let category=$(this).data('category');
        ocultar('publication');
        mostrar(category);

    });
    $(document).on('click','#input_search_publication',function(){
        $(this).val('');
       $(this).focus();
    });
    $(document).on('click','#btn_search_publication',function(){
        ocultar('publication');
        let search=$("#input_search_publication").val();
        search=search.toLowerCase();
        @isset($places)
        var places=@json($places);
        @else
        var places=[{}];

        @endisset
        $("."+sessionStorage.getItem('publication')).each(function(){
         
           for (obj of places) { //alg de búsqueda
                   // para buscar la palabra hacemos
                let posicion = obj.prov.indexOf(search);
                if (posicion !== -1){
                    console.log($(this).data('id')+' '+obj.id)

                   if(obj.id==$(this).data('id')){

                    $(this).css('display','block');
                   }
                
           } 
           }
        });
    });
    @if(Session::has('firstLogin'))
    @if(Session::get('firstLogin')==1)
    $("#presentacion").modal("show");

    @endif

    @endif
    @if(session()->has('slugPuente'))
    console.log("{{session()->get('slugPuente')}}")
    $("#modal-"+"{{session()->get('slugPuente')}}").modal("show");
    @endif
        @if (session()->has('error'))
         $("#modal_error").modal("show");
        @endif  

    	function filter_caso() {
		mostrar('casos');
        mostrar('casos-subfilter');
        sessionStorage.setItem('publication','casos');

		ocultar('proyectos');
		ocultar('oportunidades');
		ocultar('puentes');
        ocultar('proyectos-subfilter');
		ocultar('oportunidades-subfilter');
	//	ocultar('puentes-subfilter');
        $(".sub-filter").show();

	}
		
	function filter_proyecto() {
		mostrar('proyectos');
        mostrar('proyectos-subfilter');
		ocultar('casos');
		ocultar('oportunidades');
		ocultar('puentes');
        sessionStorage.setItem('publication','proyectos');

        ocultar('casos-subfilter');
		ocultar('oportunidades-subfilter');
        $(".sub-filter").show();

	}

		
	function filter_oportunidad() {
		mostrar('oportunidades');
        mostrar('oportunidades-subfilter');
        sessionStorage.setItem('publication','oportunidades');

		ocultar('casos');
		ocultar('proyectos');
		ocultar('puentes');

		ocultar('casos-subfilter');
		ocultar('proyectos-subfilter');
        $(".sub-filter").show();
	
	}	

		
	function filter_puente() {
        sessionStorage.setItem('publication','puentes');
		mostrar('puentes');
		ocultar('proyectos');
		ocultar('casos');
		ocultar('oportunidades');
        $(".sub-filter").hide();

	}
		
				
	function filter_todos() {
		mostrar('proyectos');
		mostrar('casos');
		mostrar('oportunidades');
		mostrar('puentes');
        
	}
    function ocultar(nombreDeLaClase) {
		let section=document.getElementsByClassName(nombreDeLaClase);
        
		let i;
			for (i = 0; i < section.length; i++) {
				section[i].style.display = 'none';
				
			}
	}
	function mostrar(nombreDeLaClase) {
		let section=document.getElementsByClassName(nombreDeLaClase);
		let i;
			for (i = 0; i < section.length; i++) {
				section[i].style.display = 'block';
				
			}
	}
    function shareFacebook(urlContent,bridge) {
        var url=urlContent+"?puente="+bridge;

        FB.ui({
            method: 'share',
            href: url,
        }, function (response) {});
    }


    function shareTwitter(code,text,urlContent,name_case) {
        let url = encodeURIComponent(urlContent+"?&puente="+code);
        let texto =text; //no olvidar que son 255 caracteres	
        let hashtags = "@puenteSocial,@"+name_case //opcional
        window.open('https://twitter.com/intent/tweet?url=' + url + '&text=' + texto + '&hashtags=' + hashtags,
            '_blank');
    }

    function sharing_link(urlContent,code) {
        let aux = document.getElementById("input_sharing_link");
        aux.value = urlContent+"?puente="+code;

        try {
            let successful = document.execCommand('copy');
            let msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }

        $("#Modal-compartir").modal("show");
        aux.focus();
        aux.select();
    }

    function copy_link() {
        aux = document.getElementById("input_sharing_link");
        aux.focus();
        aux.select();
        try {
            let successful = document.execCommand('copy');
            let msg = successful ? 'successful' : 'unsuccessful';
            $("#msg_copy").text("Copiado al portapapeles!");
            console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }
    }
</script>
@endsection