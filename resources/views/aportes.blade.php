@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->

        <div class="row">
            <br>		
		
			<div class="col-md-2 col-sm-2 col-lg-2">
		
			</div>
		
			<div class="col-md-8 col-sm-8 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
					
					<div class="col-md-12">
						<h3><b>DESEO DONAR DINERO</b></h3>
						<p>Donando dinero permitirás que nuestra fundación pueda implementar el programa de ayuda. Una vez logrando el objetivo se le enviará a todos los donantes un informe</p>
						<br>	
						 <h4>LAS OPCIONES SON:</h4>						  
						  <br>
						<a href="" data-toggle="modal" data-target="#Modal-1"><button type="button" class="btn btn-light"><img src="{{asset('images/pagos/bp.png')}}"  width="90%" height="90%" alt="TRANSFERENCIA BANCARIA"></button></a>
						<br>
						<br>
						<a id="link_mp" > <button type="button" width="240px" class="btn btn-light"><img src="{{asset('images/pagos/mp2.png')}}" alt="MERCADO PAGO"></button></a>
						<br>
						
						<br>
						
					<a id="link_paypal"> <button type="button" class="btn btn-light"><img src="{{asset('images/pagos/paypal.png')}}" alt="PAYPAL"></button></a>
						<br>
						<br>
						<p><b>DONARONLINE:</b>
							<p>Es una plataforma para ONGs, para recibir donaciones con tarjeta de forma simple y segura, podes ingresar el importe que desees, eventualmente o todos los meses.<br><br><a id="link_donar_online" ><small><button type="button" class="btn btn-light"><img src="{{asset('images/pagos/do.png')}}"  width="70%" alt="DONARONLINE"></button></small></a></p>
						  <hr>
						<h5><b>Una vez hecho tu aporte informanos haciendo referencia al número de caso y por este medio recibirás el recibo.</b></h5>
						<br>
						 <p><small>(*) Nuestra Fundación posee el Certificado de Exención vigente, en el marco de lo dispuesto en la Resolución General Nº2681 (AFIP), lo que determina la procedencia de la exención aludida - en los casos en que se autorice la deducción de donaciones (Art. 81 c) - Validará para el donante la deducción en el impuesto a las ganancias.</small></p>
							
						<br>
						
					</div>
				
		
			</div>
		
		<div class="col-md-2 col-sm-2 col-lg-2">
		
			</div>
		<br>
                 
				<br>	
       </div>	
		
		<div class="col-md-12 col-sm-12 col-xs-12 text-center">	
					
			<div class="row">
				<br>
				<br>
				<div class="col-md-1 col-sm-1 col-xs-1">
					
				</div>
				
				<div class="col-md-2 col-sm-2 col-xs-2">
					<p><a href="tel:541147861798 "><i class="fa fa-phone fa-2x" aria-hidden="true"></i></a></p>							 	
				</div>
				
				<div class="col-md-2 col-sm-2 col-xs-2">
					<p><a href="mailto: info@puentesocial.org?subject=Deseo%20Recibir%20más%20información&body=Quiero%20recibir%20mas%20información%20de%20Fernando%20Repetto" target="_blank"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></p>						 	
				</div>
				
				<div class="col-md-2 col-sm-2 col-xs-2">
					 <a onclick="sharing_link()"><i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></a>		
												 	
				</div>

				<div class="col-md-2 col-sm-2 col-xs-2">
					 <a onclick="load_fb();"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>	
				</div>
				
				<div class="col-md-2 col-sm-2 col-xs-2">
					<a onclick="sharing_button()" id="btn_twitter"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>							 	
				</div>
				
					<div class="col-md-1 col-sm-1 col-xs-1">
					
				</div>
				
				
		
			
			</div>
		
		</div>
	
 
	<!-- Modal for portfolio item 1 -->
	<div class="modal fade" id="Modal-1" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center" id="Modal-label-1"  style="color: #505050 !important;">TRANSFERENCIA BANCARIA:</h4>
				</div>
				<div class="modal-body">						
					<br>		
		
					<p style="color: #505050 !important;">Podes hacerlo por tu home banking o cajero automático. Los datos que necesitas son:</p> 
									
					<ul>
						<li style="color: #505050 !important;">Fundación Espiga, CUIT.: 30-69939101-4, Banco Provincia. Cuentas:</li>
						<br>
						<li style="color: #505050 !important;"><b>Corriente Pesos Nº:</b> 5174-21274/2 | <b>CBU:</b> 0140046501517402127426</li>
						<li style="color: #505050 !important;"><b>Caja de Ahorro en U$S Nº:</b> 5174-503620/8 | <b>CBU:</b> 140046504517450362085</li>
					</ul>
												
														
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

<!--FIN DE MODAL PARA COMPARTIR LINK-->
<!--MODAL INICIAL DONACIONES -->
<!-- Modal for portfolio item 1 -->
<div class="modal fade" id="Modal-donaciones" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="Modal-label-1"  style="color: #3b3a3a !important; font-weight: bold">ACERCA DE LAS DONACIONES:</h4>
			</div>
			<div class="modal-body">						
					
				<p style="color: #505050 !important;text-align:center" class="p-2">Las donaciones a Puente Social son deducibles del Impuesto a las Ganancias según el Artículo 81 de la Ley N° 20.628, con vigencia hasta el 31/12/2021</p> 		

				<p style="color: #505050 !important; text-align:center" class="p-2">
					Nuestra fundación no recibe donaciones o subvenciones de 
					ninguna organización política, gubernamental o religiosa. No
					profesa partidismo, ni credo. Nuestro trabajo es la promoción,
					con total independencia, del “ser humano”
				</p>
				
											
													
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!--FIN MODAL -->

<div id="fb-root"></div>
@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
	$("#Modal-donaciones").modal("show");
    $("#link_mp").attr('href',sessionStorage.getItem('link_mp'));
    $("#link_paypal").attr('href',sessionStorage.getItem('link_paypal'));
    $("#link_donar_online").attr('href',sessionStorage.getItem('link_donar_online'));

</script>
@endsection
