@extends('layouts.public-2')
@section('css')

@endsection
@section('content')
<div class="row">
		
    <br>		
    
    <div class="col-md-3 col-sm-3 col-lg-3">
         
    </div>	 
    
    
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
            
    <br>
    
    <h4><b>PUBLICAR EN PUENTE SOCIAL</b></h4>
    <br>
    <br>
    <p>Publicar un caso propio o de alguien que conozcas que
        necesita ayuda. Serás el nexo entre la ayuda y esa persona.</p>	
    <a href="{{url('enviar-caso')}}" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-heart " aria-hidden="true"></i> CONSTRUIR UN PUENTE</button></a>
    <br>
    <br>		
    <p>Si tenes o conoces un proyecto social, solidario o comunitario,
        que pueda ayudar a otras personas, comunidades o mejorar el
        medio ambiente y necesitas ayuda para llevarlo adelante.</p>				
    <a href="{{url('enviar-proyecto')}}" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-bullhorn " aria-hidden="true"></i> INVITAR A PROYECTO SOCIAL</button></a>
    <br>
    <br>
    <p>Si tenes algo para donar, que ya no usas, que te sobró
        y que a otro le puede hacer falta compartilo.</p>
	
    <a href="{{url('enviar-oportunidad')}}" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-diamond " aria-hidden="true"></i>  COMPARTIR UNA DONACIÓN</button></a>
    <br>
    <br> 
    <p>Si estas ofreciendo trabajo, de cualquier tipo.</p>
  
    <a href="{{url('enviar-oportunidad-laboral')}}" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-diamond " aria-hidden="true"></i>  OFRECER UN  TRABAJO</button></a>
    <br>		
    <br>	

    <p>Puente Social es un proyecto desarrollado por Yeah!
        (Fundación Espiga) con el ambicioso objetivo de lograr
        un mundo más equitativo, en donde las personas tengan
        más oportunidades y las empresas se involucren en
        aspectos sociales. Para poder llevar adelante todo
        esto, necesitamos tu ayuda:</p>

        <a href="{{url('deseo-donar')}}" target="_blanck"> <button type="button" class="btn btn-success">DONAR A PUENTE SOCIAL</button></a>

        
    </div>
    
    <div class="col-md-3 col-sm-3 col-lg-3">
         
    </div>	
    
              
    
</div>	
<!-- Modal for portfolio item 1 -->
<div class="modal fade" id="Modal-subir-caso" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="Modal-label-1"  style="color: #ffffff !important; font-weight: bold"><i class="fa fa-cloud-upload " aria-hidden="true"></i> &nbsp CARGAR CASOS, PROYECTOS
                    O OPORTUNIDADES:</h4>
			</div>
			<div class="modal-body">						
					
				<p style="color: #ffffff !important;text-align:center" class="p-2"><strong>
                    Puente Social es una herramienta para vincularnos
                    y entre todos trabajar por un mundo mejor</strong></p> 		

				<p style="color: #ffffff !important; text-align:center" class="p-2">
					Aquí todos podemos cargar casos, proyectos sociales
                    u oportunidades, pero es importante hacerlo con
                    responsabilidad y con todos los datos con la mayor exactitud
                    posible, junto con a los datos de contacto.
                    TODOS LOS CASOS SE VERIFICARAN ANTES
                    DE SER PUBLICADOS.
				</p>
				
											
													
				
			</div>
			<div class="modal-footer ">
                <div class="text-center">
                    <button type="button" class="btn btn-success" data-dismiss="modal">PUBLICAR</button>

                </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
    $("#Modal-subir-caso").modal("show");
</script>
@endsection

