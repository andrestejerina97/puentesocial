
<!DOCTYPE html>
<html lang="es">

<body>
    <table hidden id="tabla_usuarios" class="table table-hover">
        
        <thead class="">
        <tr>
        <th class="text-left">Nombre</th>
        <th class="text-left" >Rol</th>
        <th class="text-left" >email</th>
        <th class="text-left" >Fecha de alta</th>
        <th class="text-left" >Proveedor</th>
        <th class="text-left" >Último acceso</th>

    </tr>
        </thead>

    <tbody>
        @foreach($users as $user)
        <tr>
       
        <td class="text-left">{{$user->name}}</td>
        <td class="text-left">
          <ul>
            @foreach ($user->roles as $role)
            @switch($role->id)
                @case(1)
                    <li>Editor</li>
                    @break
                @case(2)
                <li>Empresas</li>
                    @break
                @case(3)
                <li>Administrador</li>
                    @break
                @case(4 )
                <li>Usuario</li>
                    @break
                @default
                    
            @endswitch
            @endforeach
          </ul>
        </td>
        <td class="text-left">{{$user->email}}</td>
        <td class="text-left">{{$user->created_at}}</td>
        <td class="text-left">{{$user->provider}}</td>
        <td class="text-left">{{$user->last_login}}</td>
        <td>
        </td>
        </tr>
        @endforeach

    </tbody>
    </table>
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 

<script type="text/javascript" src="{{asset('js/tableExport/tableExport.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
<script type="text/javascript" src="{{asset('js/autotable/jspdf.plugin.autotable.min.js')}}"></script>

<script type="text/javascript">
     var doc = new jsPDF()

doc.autoTable({ html: '#tabla_usuarios' })
doc.save('Listado de usuarios.pdf')

</script>
</body>
</html>