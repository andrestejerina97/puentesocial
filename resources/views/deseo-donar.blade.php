@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->
<div class="col-md-12 text-center">	
				
    <h3><b>DESEO DONAR</b></h3>
    <p><span style="color: #fff;"><p><b>Para poder llevar adelante los puentes necesitamos sumar voluntades y también aportes.</b> 
        <br>Si crees, podes hacerlo desde cualquiera de las plataformas. www.puentesocial.org. 
        <br>IMPORTANTE: Tus aportes podés descontarlos del pago ganancias (*)</p></span></p>
    <br>
    <p>Por cualquier duda podes llamarnos al 11 4786 1798, por <a href="https://wa.me/5491121646003" target="_blanck">Whatsapp 11 2164 6003</a> o por mail a <a href="mailto: info@puentesocial.org"><span style="color: #293984;">info@puentesocial.org</span></a></p>
   

    <br>
</div>

<div class="container">
        <div class="col-sm-8 col-sm-offset-2">	
            
            <img src="images/iconos/bancoprovincia.png" alt="img-responsive"/>
                    <td><p><b>TRANSFERENCIA BANCARIA:</b> 
                        <p>Podes hacerlo por tu home banking o cajero automático. Los datos que necesitas son:
                            
                            <br>Fundación Espiga, CUIT.: 30-69939101-4, Banco Provincia. Cuentas:
                            <li><b>Corriente Pesos Nº:</b> 5174-21274/2 | <b>CBU:</b> 0140046501517402127426</li>
                            <li><b>Caja de Ahorro en U$S Nº:</b> 5174-503620/8 | <b>CBU:</b> 140046504517450362085</li>
                            </p>
                      <hr>
                     
                
                 <img src="images/iconos/mercadopago.png" alt="img-responsive"/>
                    <p><b>MERCADO PAGO:</b>
                        <p>Todas las opciones de pago, toca sobre el botón correspondiente.</p>
                            
                        
                            <a mp-mode="dftl" href="https://www.mercadopago.com/mla/checkout/start?pref_id=271160148-f25fd25b-2d07-4f3e-adc2-739464d59ac4" target="_blank" name="MP-payButton"><button type="button" class="btn btn-secondary btn-sm">$500</button></a>
                            
                        
                             <a mp-mode="dftl" href="https://www.mercadopago.com.ar/checkout/v1/redirect?preference-id=271160148-9116163f-f261-49a4-8651-d1e4bfe3b5ff" target="_blank" name="MP-payButton"><button type="button" class="btn btn-secondary btn-sm">$750</button></a>
                            
                            
                            <a mp-mode="dftl" href="https://www.mercadopago.com/mla/checkout/start?pref_id=271160148-01a751ca-2a9b-442c-9b40-77c1aecc80df" target="_blank" name="MP-payButton"><button type="button" class="btn btn-secondary btn-sm">$1000</button></a>
                            
                            
                            <a mp-mode="dftl" href="https://www.mercadopago.com/mla/checkout/start?pref_id=271160148-01a751ca-2a9b-442c-9b40-77c1aecc80df" target="_blank" name="MP-payButton"><button type="button" class="btn btn-secondary btn-sm">$2500</button></a>
                        
                                                        
                            <a mp-mode="dftl" href="https://www.mercadopago.com/mla/checkout/start?pref_id=271160148-4d3ce335-4dff-42b1-8e37-63b5af3ff60b" target="_blank" name="MP-payButton"><button type="button" class="btn btn-secondary btn-sm">$5000</button></a>
                            <br>
                            <br><p>MERCADO PAGO DONACIÓN MENSUAL:</p>
                        <a mp-mode="dftl" href="http://mpago.la/2mM9C5L" target="_blank" name="MP-payButton"><button type="button" class="btn-secondary btn-sm">Otro importe</button></a>
                    
                    <hr>
                    
                            
                            
                
            <a href="https://donaronline.org/fundacion-espiga/donacion-para-fundacion-espiga" target="_blank"><img src="images/iconos/donaronline.png" alt="img-responsive"/></a>
                    <p><b>DONARONLINE:</b>
                        <p>Es una plataforma para ONGs, para recibir donaciones con tarjeta de forma simple y segura, podes ingresar el importe que desees, eventualmente o todos los meses.<br><br><a href="https://donaronline.org/fundacion-espiga/donacion-para-puente-social" target="_blank"><small><button type="button" class="btn btn-success">DONARONLINE</button></small></a></p>
                      <hr>
                     
                        <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2YUYBVHBHLY5N" target="_blank"><img src="images/iconos/paypal.png" alt="img-responsive"></a></td>
                    <td><p><b>DONAR POR PAYPAL:</b>
                        <p>Un medio seguro, ideal para aportes desde el exterior. Si no tienes saldo en tu cuenta PayPal, asociá tu tarjeta de crédito y paga al instante, sin tener que ingresar nuevamente los datos de tu tarjeta. <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RKJ57EPSN22X6&source=url" target="_blank"> <br><br><small><button type="button" class="btn btn-success">DONAR POR PAYPAL</button></small></a></p>
                      <hr>
                      
                        <a href="http://espiga.org/formulario-donaciones.html" target="_blank"><img src="images/iconos/bt-formulario.png" alt="img-responsive"></a></td>
                    <td><p><b>FORMULARIO DE DONACIONES:</b>
                        <p>Para hacer consultas para ser donante de alguna de nuestras campañas activas o si tenes algo para donar, podes completar el siguiente formulario. <a href="{{url('contacto')}}" target="_blank"> <br><br><small><button type="button" class="btn btn-success">DESEO DONAR</button></small></a></p>
                         
                        
                <br>							
            
            <br>
            <br>
            
        </div>
    
    <div class="col-md-12 text-center">	
        
                    <p><small>(*) Nuestra Fundación posee el Certificado de Exención vigente, en el marco de lo dispuesto en la Resolución General N° 2681 (AFIP), lo que determina la procedencia de la exención dispuesta en el artículo 20 de la Ley de Impuesto a las Ganancias. El reconocimiento de la exención aludida -en los casos en que se autorice la deducción de donaciones (Art.81 c)- validará para el donante la deducción en el impuesto a las ganancias de las donaciones efectuadas a las entidades beneficiadas desde la fecha que conste en el Certificado de Exención. 
                        <a href="https://www.afip.gob.ar/genericos/exentas/rg2681/" target="_blank"><span style="color: #293c81;">Para tener el certificado online ingrese aquí </span></a> colocá nuestro CUIT: 30699391014 y tendrás el comprobante actualizado para imprimir</small></p>
                    <p><a href="https://www.afip.gob.ar/genericos/exentas/rg2681/" target="_blank"> <img src="images/iconos/bt-afip.png" alt="img-responsive"/></a></p>
    </div>
        

</div>	

<br>
<br>
<br>
<div class="col-md-12 col-sm-12 col-xs-12 text-center">	
<div class="row">
    
    <div class="col-md-1 col-sm-1 col-xs-1">
        
    </div>
    
    <div class="col-md-2 col-sm-2 col-xs-2">
        <p><a href="tel:541147861798 "><i class="fa fa-phone fa-2x" aria-hidden="true"></i></a></p>							 	
    </div>
    
    <div class="col-md-2 col-sm-2 col-xs-2">
        <p><a href="mailto: info@puentesocial.org?subject=Deseo%20Recibir%20más%20información&body=Quiero%20recibir%20mas%20información%20de%20Subir%20caso" target="_blank"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></p>						 	
    </div>
    
    <div class="col-md-2 col-sm-2 col-xs-2">
         <a onclick="sharing_link()"><i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></a>		
                                         
    </div>

    <div class="col-md-2 col-sm-2 col-xs-2">
         <a onclick="load_fb();"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>	
    </div>
    
    <div class="col-md-2 col-sm-2 col-xs-2">
        <a onclick="sharing_button()" id="btn_twitter"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>							 	
    </div>
    
    <div class="col-md-1 col-sm-1 col-xs-1">
        
    </div>
    
</div>			

</div>
 
<!--FIN DE MODAL PARA COMPARTIR LINK-->

<div id="fb-root"></div>
@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
    $("#link_mp").val(sessionStorage.getItem('link_mp'));
    $("#link_paypal").val(sessionStorage.getItem('link_paypal'));

</script>
@endsection
