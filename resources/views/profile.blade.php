
@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->
<div class="row text-center clearfix">
    <div class="col-sm-8 col-sm-offset-2 wow animated fadeInDown box1" data-wow-delay="0.3s">
        <br>
        <br>
        @if(Session::has('successProfile'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Muy bien!</strong>{{Session::get('successProfile')}}
          </div>   
        @endif
        @if(Session::has('erroProfile'))
        <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Ups!</strong>{{Session::get('erroProfile')}}
          </div>   
        @endif
        @error('photo')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
            <h3><b>Mi perfil</b></h3> <br>
            <img data-toggle="tooltip" data-placement="left"  title="Nombre: {{Auth::user()->name}} email: {{Auth::user()->email}} " loading="lazy" id="photo"
						src="{{asset('storage/users/'.Auth::user()->id."/".Auth::user()->photo )}}"  style="max-width: 90px; min-width:90px; max-height: 90px; border-radius: 50px;" alt="Mi perfil"><small><strong style="color: #ffffff !important;">
    </div>			
</div>	
<br>		
<div class="col-sm-8 col-sm-offset-2 wow animated fadeInDown box1" data-wow-delay="0.6s">
    <p><small></small></p>
<form action="{{route('profile.save')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="input-field">
                <label for="">Nombre:</label>
            <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}" placeholder="* Nombre y apellido..." required>
            </div>
            <br>
            <div class="input-field">
            <label for="">Email:</label>

            <input type="email" disabled value="{{Auth::user()->email}}" name="mail" class="form-control" placeholder="* Email..." required>
            </div>
            <br>
            <div class="input-field">
                <label for="">Edad:</label>

                <input type="text" value="{{Auth::user()->age}}" name="age" class="form-control" placeholder="Edad...">
            </div>
            <br>
            <div class="input-field">
                <label for="">Fecha de nacimiento:</label>
                <input type="date" value="{{Auth::user()->birthdate}}" name="birthdate" class="form-control" placeholder="Fecha de nacimiento...">
            </div>
            <br>
            <div class="input-field">
                <label for="">Dirección:</label>
                <input type="text" value="{{Auth::user()->home}}" name="home" class="form-control" placeholder="Dirección...">
            </div>
            <br>
            <div class="input-field">
                <label for="">Cambiar mi foto de perfil (3MB MAX): <span></span></label>
              <input type="file" name="photo" id="photo" class="form-control" accept="image/*">
            </div>	
            <br>
            <div class="text-center">
                <button type="submit" id="submit" class="btn btn-lg btn-success">Guardar cambios</button>

            </div>
            <br>
            <br>
        </form>
</div>	



<br>
</div>
<br>
<br>

<div class="container wow animated fadeInDown box1" data-wow-delay="0.9s">
<div class="row">
  <!-- Start contact icon column -->
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="contact-icon text-center">
      <div class="single-icon ">
        <i class="fa fa-mobile fa-2x"></i>
          
          <p>Tel.: (011) 4786-1798<br><a href="https://wa.me/541166701732" target="_blanck">Cel.: (+54 9 11) 2164-6003</a><br>
          <span>Lunes a Viernes (9am-17pm)</span>
        </p>
      </div>
    </div>
  </div>
  <!-- Start contact icon column -->
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="contact-icon text-center">
      <div class="single-icon">
        <i class="fa fa-envelope-o fa-2x"></i>
        <p><span style="color: #fff;"><a href="mailto: info@puentesocial.org"> info@puentesocial.org</a></span>                                    
            <br>
            <span style="color: #fff;"><a href="http://puentesocial.org/"> www.puentesocial.org</a></span>
        </p>
      </div>
    </div>
  </div>
  <!-- Start contact icon column -->
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="contact-icon text-center">
      <div class="single-icon">
        <i class="fa fa-map-marker  fa-2x"></i>
        <p>3 de Febrero 2856, Pasaje Ushuaia.
            <br>Nuñez (1429) CABA, BUENOS AIRES, ARGENTINA<br>

        </p>
      </div>
    </div>
  </div>
    
<div class="col-md-12 col-sm-12 col-lg-12 text-center">
    <br>
    <br>
    <p><small>DATOS INSTITUCIONALES La FUNDACIÓN ESPIGA es una Entidad de Bien Público sin fines de lucro, con Personería Jurídica obtenida por Resolución de la Inspección General de Justicia Nº 000986, con fecha 22 de Septiembre de 1998. Inscripta en la Administración Federal de Ingresos Públicos (AFIP); en el REPOC, Registro de Organizaciones Comunitarias de la Provincia de Buenos Aires; en el R.U.O., Registro Único de Organizaciones de la Sociedad Civil de la Provincia de Buenos Aires y en el Registro de Entidades de Bien Público de la Municipalidad de Vicente López. Nuestra Fundación es Miembro de la RELAF, Red Latinoamericana de Acogimiento Familiar.</small></p>
    
    <br>
    <br>
</div>	
    
</div>  
<!--FIN DE MODAL PARA COMPARTIR LINK-->

<div id="fb-root"></div>
@endsection


