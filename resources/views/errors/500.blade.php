@extends('errors::minimal')

@section('message')
    <h1 class="text-center">500</h1>
    <h2 class="text-center">Error interno del servidor, comúniquese con el personal técnico</h2>
<h5 class="text-center"><a href="{{url('/')}}">Volver a inicio</a></h5>

@endsection
