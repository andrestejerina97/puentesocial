@extends('errors::minimal')

@section('message')
    <h1 class="text-center">419</h1>
    <h2 class="text-center">Página expirada, recargue el contenido y vuelva a intentar</h2>
<h5 class="text-center"><a href="{{url('/')}}">Volver a inicio</a></h5>

@endsection
