@extends('errors::minimal')

@section('message')
    <h1 class="text-center">404</h1>
    <h2 class="text-center">Página no encontrada</h2>
<h5 class="text-center"><a href="{{url('/')}}">Volver a inicio</a></h5>

@endsection
