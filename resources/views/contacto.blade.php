
@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->
@if(Session::has('successMail'))
<br><br>
<div class="input-field text-center">
    <h2><strong>GRACIAS POR ENVIAR TU MENSAJE!</strong> </h2>
    <h3><br> NOS COMUNICAREMOS CONTIGO <br> A LA BREVEDAD</h3>

</div>
@else
<div class="row text-center clearfix">
    <div class="col-sm-8 col-sm-offset-2 wow animated fadeInDown box1" data-wow-delay="0.3s">
        <br>
        <br>
            <h3><b>CONTACTO</b></h3>
            <p>Por cualquier consulta podes escribirnos vía email o completar el formulario de contacto, <br>llamarnos por teléfono o WhatsApp <br>¡Muchas gracias!</p>
    </div>			
</div>				
<div class="col-sm-8 col-sm-offset-2 wow animated fadeInDown text-center box1" data-wow-delay="0.6s">
    <p><small>(*) Campos requeridos</small></p>
        <form action="{{route('enviar.contacto.send.email')}}" method="POST">
          @csrf
            <div class="input-field">
                <input type="text" name="nombre" class="form-control" placeholder="* Nombre y apellido..." required>
            </div>
            <br>
            <div class="input-field">
                <input type="mail" name="mail" class="form-control" placeholder="* Email..." required>
            </div>
            <br>
            <div class="input-field">
                <input type="text" name="telefono" class="form-control" placeholder="Teléfono...">
            </div>
            <br>
            <div class="input-field">
                <input type="text" name="asunto" class="form-control" placeholder="Asunto...">
            </div>
            <br>
            <div class="input-field">
                <textarea class="form-control" name="texarea" id="comments" rows="4" placeholder="Mensaje..."></textarea>
            </div>
            <br>
            <button type="submit" id="submit" class="btn btn-lg btn-success">Enviar</button>
            <br>
            <br>
        </form>
</div>	



<br>
</div>
<br>
<br>

<div class="container wow animated fadeInDown box1" data-wow-delay="0.9s">
<div class="row">
  <!-- Start contact icon column -->
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="contact-icon text-center">
      <div class="single-icon ">
        <i class="fa fa-mobile fa-2x"></i>
          
          <p>Tel.: (011) 4786-1798<br><a href="https://wa.me/541166701732" target="_blanck">Cel.: (+54 9 11) 2164-6003</a><br>
          <span>Lunes a Viernes (9am-17pm)</span>
        </p>
      </div>
    </div>
  </div>
  <!-- Start contact icon column -->
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="contact-icon text-center">
      <div class="single-icon">
        <i class="fa fa-envelope-o fa-2x"></i>
        <p><span style="color: #fff;"><a href="mailto: info@puentesocial.org"> info@puentesocial.org</a></span>                                    
            <br>
            <span style="color: #fff;"><a href="http://puentesocial.org/"> www.puentesocial.org</a></span>
        </p>
      </div>
    </div>
  </div>
  <!-- Start contact icon column -->
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="contact-icon text-center">
      <div class="single-icon">
        <i class="fa fa-map-marker  fa-2x"></i>
        <p>3 de Febrero 2856, Pasaje Ushuaia.
            <br>Nuñez (1429) CABA, BUENOS AIRES, ARGENTINA<br>

        </p>
      </div>
    </div>
  </div>
    
<div class="col-md-12 col-sm-12 col-lg-12 text-center">
    <br>
    <br>
    <p><small>DATOS INSTITUCIONALES La FUNDACIÓN ESPIGA es una Entidad de Bien Público sin fines de lucro, con Personería Jurídica obtenida por Resolución de la Inspección General de Justicia Nº 000986, con fecha 22 de Septiembre de 1998. Inscripta en la Administración Federal de Ingresos Públicos (AFIP); en el REPOC, Registro de Organizaciones Comunitarias de la Provincia de Buenos Aires; en el R.U.O., Registro Único de Organizaciones de la Sociedad Civil de la Provincia de Buenos Aires y en el Registro de Entidades de Bien Público de la Municipalidad de Vicente López. Nuestra Fundación es Miembro de la RELAF, Red Latinoamericana de Acogimiento Familiar.</small></p>
    
    <br>
    <br>
</div>	
    
</div>  
<!--FIN DE MODAL PARA COMPARTIR LINK-->
@endif
<div id="fb-root"></div>
@endsection


