@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->
@if(Session::has('successMail'))
<br><br>
<div class="input-field text-center">
    <h2><strong>GRACIAS POR ENVIAR TU MENSAJE!</strong> </h2>
    <h3><br> NOS COMUNICAREMOS CONTIGO <br> A LA BREVEDAD</h3>

</div>
@else
<div class="row">
		
    <br>		

    <div class="col-md-2 col-sm-2">
    
    
    </div>
    
    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
            
            
        <br>
        <h3><b>ENVIAR PROYECTO PROPIO</b></h3>						
                
            
        <form action="{{route('enviar.proyecto.send.email')}}"  method="post" class="m-t-15" enctype="multipart/form-data">
            @csrf
            <br>
            <div class="input-field">
                <input type="text" name="nombre" class="form-control" placeholder="Nombre y Apellido..." required>
            </div>
            <br>
            <div class="input-field">
                <input type="text" name="mail" class="form-control" placeholder="Email..." required>
            </div>		
            <br>
            <div class="input-field">
                <input type="text" name="organizacion" class="form-control" placeholder="Nombre de la organización / CUIT..." required>
            </div>
            <br>	
            <div class="input-field">
                <input type="text" name="proyecto" class="form-control" placeholder="Nombre del proyecto..." required>
            </div>		
            <br>
            <div class="input-field">
                <input type="text" name="telefono" class="form-control" placeholder="Teléfono celular..." required>
            </div>	
            <br>
            <div class="input-field">
                <input type="text" name="ciudad" class="form-control" placeholder="Ciudad y provincia...">
            </div>										
            <br>
            <div class="input-field">
                <textarea class="form-control" name="texarea" id="comments" rows="6" placeholder="Detalle de tu caso (máximo 300 palabras)..."></textarea>
            </div>
            <br>
            
                    <div class="input-field">
                        <label for="">Enviar fotos: <span></span></label>
                      <input type="file" name="photos[]" id="photo" class="form-control" multiple>
                    </div>
                <br>
                <p><small>Enviando éste formulario autorizo a Fundación Espiga a utilizar la información que por este medio proveo para publicar en sus redes sociales y gestionar.</small></p>
                <br>
                
                <button type="submit" id="submit" class="btn btn-lg btn-success">Enviar</button>
            <br>
            <br>
            </form>
        <br>
        <br>
        <p><small>Art 236. Falsificación material en documento público, por funcionario público. El funcionario público que ejerciendo un acto de su función, hiciere un documento falso o alterare un documento verdadero, será castigado con tres diez años de penitenciaría. Quedan asimilados a los documentos, las copias de los documentos inexistente y las copias infieles de documentos existentes. Art 237. Falsificación o alteración de un documento público, por un particular o por un funcionario, fuera del ejercito de sus funciones, hiciere un documento público flaso o alterare un documento público verdadero, será castigado con dos a seis años de penitenciaría.</small></p>
        
    </div>
    
    <div class="col-md-2 col-sm-2">
    
    
    </div>

    <br>               
    <br>	
</div>	

<div class="row">
    
    <hr>
    <div class="col-md-6 col-sm-6 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
        
        <br>
                <a href="https://wa.me/5491121646003" target="_blanck"> <button type="button" class="btn btn-success"> <i class="fa fa-whatsapp"></i> WHATSAPP 11 2164 6003  </button></a>
                <br>
    
    
    
    </div>
    
    <div class="col-md-6 col-sm-6 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
        <br>
        <a href="mailto:info@puentesocial.org" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-envelope-o"></i> info@puentesocial.org  </button></a>
            <br>
        
    </div>
    
    <div class="col-md-12 col-sm-12 text-center">
        <br>
    
    </div>

</div>
    <div class="col-md-12 col-sm-12 col-xs-12 text-center">	
            
    <div class="row">
        <br>
        <br>
        <div class="col-md-1 col-sm-1 col-xs-1">
            
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><a href="tel:541147861798 "><i class="fa fa-phone fa-2x" aria-hidden="true"></i></a></p>							 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p><a href="mailto: caso@puentesocial.org?subject=Deseo%20Recibir%20más%20información%20#OP2003&body=Quiero%20saber%20mas%20sobre%20la%20donación%20de%20calzado%" target="_blank"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></p>						 	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
             <a onclick="sharing_link()"><i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></a>		
                                             
        </div>

        <div class="col-md-2 col-sm-2 col-xs-2">
             <a onclick="load_fb();"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>	
        </div>
        
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a onclick="sharing_button()" id="btn_twitter"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>							 	
        </div>
        
            <div class="col-md-1 col-sm-1 col-xs-1">
            
        </div>
    
    </div>

</div>
@endif

<!--FIN DE MODAL PARA COMPARTIR LINK-->

<div id="fb-root"></div>
@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
    $("#link_mp").val(sessionStorage.getItem('link_mp'));
    $("#link_paypal").val(sessionStorage.getItem('link_paypal'));

</script>
@endsection
