@extends('layouts.admin')

@section('content')
<!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="row">

                    <div class="col-lg-3 col-md-3 text-left">

                        <a href="javascript:window.history.back();;" class="btn-table"><i
                                class="fa fa-arrow-left"></i>
                        </a>
                        <b>Donantes</b>

                    </div>
                    <div class="col-md-6">

                    </div>

                    <div class="col-lg-3 col-md-3 align-content-rigth">
                        <a href="javascript:;" onclick="save_information_aditional();" id="btn_save"
                            class="btn-table "><i class="fa fa-floppy-o fa-1x" aria-hidden="true"></i> Guardar
                            cambios</a>

                    </div>
                </div>
            </div>
            <div class="panel-body">
              @foreach ($publications as $publication)
                <form id="form_information_aditional">
                  @csrf
                <input type="hidden" name="id" value="{{$publications_id}}">
                    <section id="section_case">
                        <div class="col-lg-6 col-md-6">
                            <div class="input-field">
                                <label for="">Link de pago Paypal</label>
                                <textarea class="form-control" name="link_paypal" id="link_paypal" rows="2"
                                    placeholder="Adjuntar link...">@isset($publication->link_paypal){{$publication->link_paypal}} @endisset</textarea>
                            </div>
                            <br>
                            <div class="input-field">
                                <label for="">Link de pago Mercado Pago</label>
                                <textarea class="form-control" name="link_mercado_pago" id="link_mercado_pago" rows="2"
                                    placeholder="Adjuntar link...">@isset($publication->link_mercado_pago){{$publication->link_mercado_pago}} @endisset</textarea>
                            </div>
                            <br>
                            <div class="input-field">
                                <label for="">Link Donar online</label>
                                <textarea class="form-control" name="link_donar_online" id="link_donar_online" rows="2"
                                    placeholder="Adjuntar link...">@isset($publication->link_donar_online){{$publication->link_donar_online}} @endisset</textarea>
                            </div>
                            <br>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="input-field">
                                <label>Progreso actual</label>
                            </div>
                            <div class="input-field">
                                <input type="number" class="form-control" placeholder="Progreso actual..."
                                    name="progress" @isset($publication->progress)
                                value="{{$publication->progress}}"
                                @endisset
                                >
                            </div>

                        </div>
                    </section>
            </div>
            </form>
            @endforeach

        </div>
        <div class="panel panel-default">
            <div class="panel-header">
                <h4 class="text-center">Listado de donantes</h4>
            </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class=" ml-4">
                        <tr style="border: 1px solid transparent">
                            <td>
                            </td>
                            <td>
                                <form id="form_2" action="{{route("admin.search.donor.index",$publications_id)}}"
                                    method="POST">
                                    @csrf
                                    <input type="hidden" id="filter" name="filter" value="users.name">
                                    <div class="input-group col-lg-9 col-md-8">
                                        <a href="javascript:;" onclick="document.getElementById('form_2').submit();"
                                            class="input-group-addon"><i class="fa fa-search"></i></a>
                                        <input type="text" placeholder="Buscar por nombre.." class="form-control"
                                            id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                                    </div>
                                </form>

                            </td>
                            <td>

                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th class="text-left">Foto</th>
                            <th class="text-left">Nombre</th>
                            <th class="text-left">Corazones</th>
                            <th class="text-left">Fecha de creación</th>
                            <th class="text-left">Donación</th>

                            <th class="text-center" style="width: 25%"><a
                                    href="{{route('admin.edit.donor',$publications_id)}}" class="btn-admin">+ Nuevo
                                    Donante</a>
                            </th>

                        </tr>
                    </thead>
                    <div class="panel-body">

                        <tbody>
                            @foreach($donors as $donor)
                            <tr>
                                <td class="text-left"><img loading="lazy" id="photo"
                                    src="{{asset('storage/users/'.$donor->users->id."/".$donor->users->photo )}}"  style="max-width: 50px; min-width:50px; max-height: 50px; " alt="Sin foto">
                                </td>
                                <td class="text-left">{{$donor->users->name}}</td>
                                <td class="text-left">{{$donor->hearts}}</td>
                                <td class="text-left">{{$donor->created_at}}</td>
                                <td class="text-left">{{$donor->observations}}</td>

                                <td>
                                    <a href="{{route('admin.edit.donor',['publications_id'=>$publications_id ,'users_id'=>$donor->id])}}"
                                        class="btn-table"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="javascript:;" data-id_donor={{$donor->id}} onclick="eliminar(this);"
                                        class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </div>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
 

    function eliminar(a) {
        swal({
                text: 'Seguro que deseas eliminar este donante,esto no se  puede revertir!".',
                buttons: {
                    cancel: "No,cancelar!",
                    catch: {
                        text: "Sí,eliminar",
                        value: "catch",
                    },
                },
            })
            .then((value) => {
                switch (value) {
                    case "catch":
                        let id_donor = $(a).data("id_donor");
                        let url = "{{route('admin.delete.donor','id')}}"
                        url = url.replace("id", id_donor);
                        $.ajax({
                            url: url,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'get',
                            dataType: "JSON",
                            success: function (data, textStatus, jqXHR) {
                                if (data.result != -1) {
                                    swal({
                                        tittle: "Excelente!",
                                        text: "Donante eliminado con éxito",
                                        icon: "success",
                                    }).then((value) => {
                                        location.href =
                                            '{{route("donors.index",$publications_id)}}';


                                    });

                                } else {

                                }

                            }
                        });
                        break;
                    default:

                }
            });
    }

    function save_information_aditional() {

        var formdata = new FormData($("#form_information_aditional")[0]);
        $.ajax({
            url: "{{route('admin.update.publication')}}",
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,

                });
            },
            success: function (data, textStatus, jqXHR) {
                if (data.result != null) {
                    swal({
                        tittle: "Excelente!",
                        text: "Actualización exitosa",
                        icon: "success",
                    }).then((value) => {
                        location.reload();
                    });
                }
            },
            error: function (data, message, res) {
                swal("ups!", "Hubo un error al procesar tu petición,vuelve a intentar por favor", "error");
            },
            statusCode: {
                422: function (data) {
                    swal("Ups!", "El número de caso ingresado ya existe,por favor ingrese uno nuevo",
                        "error");
                }
            }
        });
        // do other things for a valid form


    }

</script>
@endsection
