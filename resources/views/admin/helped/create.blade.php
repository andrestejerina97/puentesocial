@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->

     @include('sweet::alert')
<!-- Page content -->

<div class="main">
  <div class="panel panel-default">
    <div class="panel-header">
      <div class="row">

        <div class="col-lg-3 col-md-3">
          <a href="{{route('benef.index',$publications_id)}}" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
          <b >Nuevo Donante</b> 

          </div>
        <div class="col-md-6"></div>
    
        <div class="col-lg-3 col-md-3 align-content-rigth">
         <a href="javascript:;"  onclick="save_benef();" id="btn_save" class="btn-table " ><i class="fa fa-floppy-o fa-1x" aria-hidden="true"></i> Guardar beneficiario</a>
          
      </div>        
        </div>        
    </div>
    <div class="panel-body">
        <div class="input-field">
            <label >A continuación debe buscar y seleccionar el beneficiario que desea agregar:</label>
        </div>
    <div class="row">

        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
            <form id="form_3" action="{{route("admin.search.benef",$publications_id)}}" method="POST">
                @csrf
            <input type="hidden" value="{{$publications_id}}">
                <input type="hidden" id="filter" name="filter" value="email">
                <div class="input-group col-lg-9 col-md-8">
                  <a href="javascript:{}" onclick="document.getElementById('form_3').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <input placeholder="Correo electrónico..." type="text" class="form-control" name="search" id="search" aria-describedby="inputGroupSuccess1Status">
                </div>
              </form>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
            <form id="form_4" action="{{route("admin.search.benef",$publications_id)}}" method="POST">
                @csrf
                <input type="hidden" value="{{$publications_id}}">
                <input type="hidden" id="filter" name="filter" value="name">
                <div class="input-group col-lg-9 col-md-8">
                  <a href="javascript:{}" onclick="document.getElementById('form_4').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <input placeholder="Nombre.." type="text" class="form-control" name="search" id="search" aria-describedby="inputGroupSuccess1Status">
                </div>
              </form>
        </div>
        <br> <br>
        <section id="section_table">
            @isset($users)
            <table class="table table-hove">
                <tr>
                    <td>Nombre</td>
                    <td>Email</td>
                    <td>Foto</td>
                    <td></td>
                </tr>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        <div >
                            <img loading="lazy" src="{{asset('storage/users/'.$user->id."/".$user->photo)}}"  class="img-thumbnail" style="max-width: 50px; min-width:50px; max-height: 50px; " alt=""  >
        
                        </div>
                    </td>
                    <td>
                        <a href="javascript:;" data-users_id='{{$user->id}}'   data-name='{{$user->name}}' data-photo='{{$user->photo}}' onclick="change_section(this)" class="btn-admin">Seleccionar</a>
                    </td>
        
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>
            @endisset
           </section>

           <section id="section_form" style="display: none">
            <form   id="form_benef" method="POST"  enctype="multipart/form-data" action="{{route('admin.store.case')}}">
              @csrf
              <div class="col-lg-12 col-xs-12">
              <input type="hidden" name="publications_id" value="{{$publications_id}}">
              <input type="hidden" id="users_id" name="users_id" >

                @include('admin.partials.form-benef')
              </div>

            </form>
    </section>
    </div>
    <br>
   
    <br>

  </div>
  </div>
</div> 

@endsection
@section('scripts')
  <script>
      function change_section(a) {
          $("#section_table").css('display','none');
          $("#section_form").css('display','block');
          let name=$(a).data("name");
          let users_id=$(a).data("users_id");
          let src="{{asset('storage/users/0')}}";
          src=src.replace('0',$(a).data("photo"));
          $("#users_id").val(users_id);
          $("#name").val(name);
          $("#photo").attr("src",src);


      }
      function save_benef(params) {
       
            var formdata = new FormData($("#form_benef")[0]);
            $.ajax({
                url         : "{{route('admin.store.benef')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Beneficiario agregado con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        let url="{{route('admin.edit.benef',['publications_id'=>$publications_id,'users_id'=>0])}}"
                        url=url.replace('0',data.result);
                        location.href=url;

                        
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {
                        swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                    }
                  }
            });
      }
      
      </script>

@endsection