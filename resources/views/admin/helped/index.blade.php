@extends('layouts.admin')

@section('content')
<!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="row">

                    <div class="col-lg-3 col-md-3 text-left">
                        <a href="{{route('admin.edit.oportunity',$publications_id)}}" class="btn-table"><i
                                class="fa fa-arrow-left"></i>
                        </a>
                        <b>Volver a oportunidad</b>

                    </div>
                    <div class="col-md-6">

                    </div>

                    <div class="col-lg-3 col-md-3 align-content-rigth">
                        <!--<a href="javascript:;" onclick="save_information_aditional();" id="btn_save"
                            class="btn-table "><i class="fa fa-floppy-o fa-1x" aria-hidden="true"></i> Guardar
                            cambios</a> -->

                    </div>
                </div>
            </div>
           
            <div class="panel-body">
                <h4 class="text-center">Listado de beneficiarios</h4>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class=" ml-4">
                        <tr style="border: 1px solid transparent">
                            <td>
                            </td>
                            <td>
                                <form id="form_2" action="{{route("admin.search.benef.index",$publications_id)}}"
                                    method="POST">
                                    @csrf
                                    <input type="hidden" id="filter" name="filter" value="users.name">
                                    <div class="input-group col-lg-9 col-md-8">
                                        <a href="javascript:{}" onclick="document.getElementById('form_2').submit();"
                                            class="input-group-addon"><i class="fa fa-search"></i></a>
                                        <input type="text" placeholder="Buscar por nombre.." class="form-control"
                                            id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                                    </div>
                                </form>

                            </td>
                            <td>

                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th class="text-left">Foto</th>
                            <th class="text-left">Nombre</th>
                            <th class="text-left">Fecha de creación</th>
                            <th class="text-left">Detalle</th>

                            <th class="text-center" style="width: 25%"><a
                                    href="{{route('admin.edit.benef',$publications_id)}}" class="btn-admin">+ Nuevo
                                    Beneficiario</a>
                            </th>

                        </tr>
                    </thead>
                    <div class="panel-body">

                        <tbody>
                            @foreach($donors as $donor)
                            <tr>
                                <td class="text-left"><img loading="lazy" id="photo"
                                    src="{{asset('storage/users/'.$donor->users->id."/".$donor->users->photo )}}"  style="max-width: 50px; min-width:50px; max-height: 50px; " alt="Sin foto">
                                </td>
                                <td class="text-left">{{$donor->users->name}}</td>
                                <td class="text-left">{{$donor->created_at}}</td>
                                <td class="text-left">{{$donor->observations}}</td>

                                <td>
                                    <a href="{{route('admin.edit.donor',['publications_id'=>$publications_id ,'users_id'=>$donor->id])}}"
                                        class="btn-table"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="javascript:;" data-id_donor={{$donor->id}} onclick="eliminar(this);"
                                        class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </div>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
 

    function eliminar(a) {
        swal({
                text: 'Seguro que deseas eliminar este beneficiario,esto no se  puede revertir!".',
                buttons: {
                    cancel: "No,cancelar!",
                    catch: {
                        text: "Sí,eliminar",
                        value: "catch",
                    },
                },
            })
            .then((value) => {
                switch (value) {
                    case "catch":
                        let id_donor = $(a).data("id_donor");
                        let url = "{{route('admin.delete.donor','id')}}"
                        url = url.replace("id", id_donor);
                        $.ajax({
                            url: url,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'get',
                            dataType: "JSON",
                            success: function (data, textStatus, jqXHR) {
                                if (data.result != -1) {
                                    swal({
                                        tittle: "Excelente!",
                                        text: "Beneficiario eliminado con éxito",
                                        icon: "success",
                                    }).then((value) => {
                                        location.href =
                                            '{{route("donors.index",$publications_id)}}';


                                    });

                                } else {

                                }

                            }
                        });
                        break;
                    default:

                }
            });
    }

    function save_information_aditional() {

        var formdata = new FormData($("#form_information_aditional")[0]);
        $.ajax({
            url: "{{route('admin.update.publication')}}",
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,

                });
            },
            success: function (data, textStatus, jqXHR) {
                if (data.result != null) {
                    swal({
                        tittle: "Excelente!",
                        text: "Actualización exitosa",
                        icon: "success",
                    }).then((value) => {
                        location.reload();
                    });
                }
            },
            error: function (data, message, res) {
                swal("ups!", "Hubo un error al procesar tu petición,vuelve a intentar por favor", "error");
            },
            statusCode: {
                422: function (data) {
                    swal("Ups!", "El número de caso ingresado ya existe,por favor ingrese uno nuevo",
                        "error");
                }
            }
        });
        // do other things for a valid form


    }

</script>
@endsection
