@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->

<!-- Page content -->

<div class="main">
  <div class="panel panel-default">
    <div class="panel-header">
      <div class="row">

        <div class="col-lg-3 col-md-3">
          <a href="{{route('users.index')}}" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
          <b >Volver a usuarios</b> 

          </div>
        <div class="col-md-6"></div>
    
        <div class="col-lg-3 col-md-3 align-content-rigth">
         <a href="javascript:;"  onclick="save_user();" id="btn_save" class="btn-table " ><i class="fa fa-floppy-o fa-1x" aria-hidden="true"></i> Guardar usuario</a>
          
      </div>        
        </div>        
    </div>
    <div class="panel-body">
 
    <div class="row">
           <section id="section_form" >
            <form method="POST" id="form_user" action="{{ route('register') }}" >
              @csrf
              <div class="col-lg-12 col-xs-12">

                @include('admin.partials.form-user')
              </div>

            </form>
    </section>
    </div>
    <br>
   
    <br>

  </div>
  </div>
</div> 

@endsection
@section('scripts')
  <script>
      function change_section(a) {
          $("#section_table").css('display','none');
          $("#section_form").css('display','block');
          let name=$(a).data("name");
          let users_id=$(a).data("users_id");
          let src="{{asset('storage/users/0')}}";
          src=src.replace('0',$(a).data("photo"));
          $("#users_id").val(users_id);
          $("#name").val(name);
          $("#photo").attr("src",src);


      }
      function save() {
        $("#form_user")[0].submit();
      }
      function save_user(params) {
       
            var formdata = new FormData($("#form_user")[0]);
            $.ajax({
                url         :  "{{route('admin.store.user')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"usuario agregado con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        let url="{{route('admin.edit.user',['users_id'=>0])}}"
                        url=url.replace('0',data.result);
                        location.href=url;

                        
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {

                      console.log(data.responseJSON.errors);
                      let lista= "";
                for(var k in data.responseJSON.errors) {
                lista += ""+ data.responseJSON.errors[k][0] +"";              
                }
                
              lista+="";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error: '+lista,
                      icon: 'error',
                    });                    }
                  }
            });
    // do other things for a valid form

      
      }
      </script>

@endsection