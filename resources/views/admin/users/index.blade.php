@extends('layouts.admin')

@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
          <div class="panel-header">
            <div class="row">
      
              <div class="col-lg-3 col-md-3 text-left">
              <a href="{{route('admin.edit.case')}}"  class="btn-table"><i class="fa fa-arrow-left"></i> 
                </a>
                <b>Usuarios</b> 
      
                </div>
              <div class="col-md-6"></div>
          
              <div class="col-lg-3 col-md-3 align-content-rigth">
                
            </div>        
              </div>       
          </div>
        <div class="table-responsive">
    <table class="table table-hover">
        
        <thead class=" ml-4">
          
          <tr style="border: 1px solid transparent">
            <td>
            </td>
            <td>
              <form id="form_2"  action="{{route("admin.search.user")}}" method="POST">
                @csrf
                <input type="hidden" id="filter" name="filter" value="users.name">
                <div class="input-group col-lg-9 col-md-8">
                  <a href="javascript:{}" onclick="document.getElementById('form_2').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <input type="text" placeholder="Buscar por nombre.." class="form-control" id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                </div>
              </form>

            </td>
            <td>
              <form id="form_3"  action="{{route("admin.search.user")}}" method="POST">
                @csrf
                <input type="hidden" id="filter" name="filter" value="role">
                <div class="input-group col-lg-9 col-md-8">
                  <a href="javascript:{}" onclick="document.getElementById('form_3').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <select name="search" id="search" class="form-control">
                    <option value="nada" selected>Roles</option>
                    <option value="business">Empresas</option>
                    <option value="user">Usuarios</option>
                    <option value="edit">Editores</option>
                    <option value="admin">Administradores</option>

                  </select>
                </div>
              </form>
            </td> 
               <td> </td>
               <td> 
                <a class="btn-table" target="_blank" href="{{route('reports.export-excel')}}"> <i class="fa fa-file-excel-o"></i></a>
                <a class="btn-table" target="_blank" href="{{route('reports.export-pdf')}}"> <i class="fa fa-file-pdf-o"></i></a>
               </td>
          </tr>
        <tr>
        <th class="text-left">Foto</th>
        <th class="text-left">Nombre</th>
        <th class="text-left" >Rol</th>
        <th class="text-left" >email</th>
        <th class="text-left" >Fecha de alta</th>
        <th class="text-left" >Proveedor</th>
        <th class="text-left" >Último acceso</th>

        <th class="text-center" style="width: 28%"><a href="{{route('admin.edit.user')}}" class="btn-admin" >+ Nuevo Usuario</a>
        </th>

    </tr>
        </thead>
    <div class="panel-body">

    <tbody>
        @foreach($users as $user)
        <tr>
        <td class="text-left"><img loading="lazy" src="{{asset('storage/users/'.$user->id."/".$user->photo)}}"  style="max-width: 50px; min-width:50px; max-height: 50px;    border-radius: 50%;
          "  id="photo" style=""  >
        </td>
        <td class="text-left">{{$user->name}}</td>
        <td class="text-left">
          <ul>
            @foreach ($user->roles as $role)
            @switch($role->id)
                @case(1)
                    <li>Editor</li>
                    @break
                @case(2)
                <li>Empresas</li>
                    @break
                @case(3)
                <li>Administrador</li>
                    @break
                @case(4 )
                <li>Usuario</li>
                    @break
                @default
                    
            @endswitch
            @endforeach
          </ul>
        </td>
        <td class="text-left">{{$user->email}}</td>
        <td class="text-left">{{$user->created_at}}</td>
        <td class="text-left">{{$user->provider}}</td>
        <td class="text-left">{{$user->last_login}}</td>


        <td>
            <a href="{{route('admin.edit.user',['users_id'=>$user->id])}}" class="btn-table"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a href="javascript:;" data-id_user={{$user->id}} onclick="eliminar(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>

        </td>
        </tr>
        @endforeach

    </tbody>
</div>
    </table>
    @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator )
    {{$users->links()}}
    @endif

      </div>
</div>
</div>    
</div> 

@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(function(){
        $('.nav-text').each(function () {
            if ($(this).text()=="Usuarios") {
                $(this).parent().addClass("active");
            }
     });
});
    function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este usuario?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_user=$(a).data("id_user");
               let url="{{route('admin.delete.user','id')}}"
               url=url.replace("id",id_user);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Usuario eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("users.index")}}';


                      });

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

    </script>    
@endsection