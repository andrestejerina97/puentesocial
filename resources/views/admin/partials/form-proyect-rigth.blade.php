<br>
@isset($created_users)
<div class="input-field">
    <label for="">Creado por:</label>
</div>
<div class="input-field">
    <div class="col-lg-6">
        <input type="text" name="name" class="form-control" placeholder="Nombre y Apellido..." 
        @isset($created_users->name)
        value="{{$created_users->name}}"
        @endisset
        
        disabled
        >
    </div>
    <div class="col-lg-6">
        <input type="text" name="created_at" class="form-control" placeholder="Fecha y hora" 
        @isset($publication->created_at)
        value="{{$publication->created_at}}"
        @endisset
        disabled
        >
    </div>
</div>
 <br>
@endisset

 <br>
 @isset($modified_users)

<div class="input-field">
    <label for="">Ultima actualización:</label>
</div>
<div class="input-field">
    <div class="col-lg-6">
        <input type="text" name="name" class="form-control" placeholder="Nombre y Apellido..." 
        @isset($modified_users->name)
        value="{{$modified_users->name}}"
        @endisset
        disabled
        >
    </div>
    <div class="col-lg-6">
        <input type="text" name="created_at" class="form-control" placeholder="Fecha y hora" 
        @isset($publication->updated_at)
        value="{{$publication->updated_at}}"
        @endisset
        disabled
        >
    </div>
</div>
@endisset
<hr>
 <br>
<br>
<div class="input-field">
    <label for="" class="label-checkbox">Este proyecto será visible para:</label>

</div>
<div class="input-field">
    <div class="col-lg-6">
        <div class="checkbox">
            <label><input value="1" type="checkbox" name="visibled_for_individual"  
                @isset($publication->visibled_for_individual)
                @if ($publication->visibled_for_individual==1)
                checked
 
                @endif
                @endisset >Individuo</label>
          </div>        
       
    </div>
    <div class="col-lg-6">
        <div class="checkbox">
            <label><input value="1" type="checkbox" name="visibled_for_bussiness"  
                @isset($publication->visibled_for_bussiness)
                @if ($publication->visibled_for_bussiness==1)
                checked
                @endif
                @endisset>Empresas</label>
          </div>      
    </div>
</div>
<br> <br>
<div class="col-lg-12 col-md-12 col-12">
    <div class="carousel-publications"  data-slick='{"slidesToShow": 1, "slidesToScroll": 2}'>
        @isset($photos)
        @foreach ($photos as $photo)
        <a onclick="click_photo(this)" href="javascript:;" style="color: grey" data-id={{$photo->id}} data-photo="{{$photo->photo}}" data-publications_id="{{$photo->publications_id}}" data-toggle="modal" ><img loading="lazy"   data-src="{{asset('storage/publicaciones/'.$photo->publications_id.'/'.$photo->photo)}}" data-lazy="{{asset('storage/publicaciones/'.$photo->publications_id.'/'.$photo->photo)}}">
        @if($publication->avatar==$photo->photo)
        FOTO DE PERFIL
        @endif
    </a>
            @endforeach
        @endisset    
   
      </div>
    
  </div>
<br>
<br>
<div class="input-field">
    <button type="button" data-target="#modal_files" data-toggle="modal" id="submit" class="btn btn-edit-publication "><i class="fa fa-image"></i> Agregar Foto/Video</button>

</div>
<br>
<div class="input-field">
    <div class="col-lg-6">
        <button type="submit" data-target="#modal_reports" data-toggle="modal" class="btn btn-edit-publication ">Informes</button>
    </div>
    <div class="col-lg-6">
    <button type="button" @isset($publication->id) onclick="location.href='{{route('donors.index',$publication->id)}}';"@endisset id="submit"  class=" btn btn-edit-publication ">Donantes</button>
    </div>

</div>
<br><br>

<br>
<div class="input-field">
    <label for="" class="label-checkbox">Frase inicial:</label>
</div>
<br>
<div class="input-field">
    <textarea class="form-control" name="opening_sentence" 
    id="opening_sentence" 
     placeholder="Frase destinada al inicio...">@isset($publication->opening_sentence){{$publication->opening_sentence}} @endisset</textarea>
</div>
<br>
<div class="input-field">
    <label for="" class="label-checkbox">Frase Faceebook:</label>
</div>
<br>
<div class="input-field">
    <textarea class="form-control" name="text_facebook" 
    id="text_facebook" 
     placeholder="Frase destinada en la publicación por facebook...">@isset($publication->text_facebook){{$publication->text_facebook}} @endisset</textarea>
</div>
    <br>
    <br>
<div class="input-field">
    <label for="" class="label-checkbox">Frase Twitter:</label>
</div>
<br>
<div class="input-field">
    <textarea class="form-control" name="text_twitter" 
    id="text_facebook" 
     placeholder="Frase destinada en la publicación para twitter...">@isset($publication->text_twitter){{$publication->text_twitter}} @endisset</textarea>
</div>
    <br>
    <div class="text-center">
        <div id="qrcode">
        </div>
        <br>
    </div>
  @isset($publication)
  <div class="input-field">
    <a id="download" download="{{$publication->name_case}}-qr.jpg" href="javascript:;" class="btn btn-edit-publication "  onclick="download_img(this);"><i class="fa fa-qrcode"></i> Descargar QR</a>
</div>
  @endisset

    @include('admin.partials.modal-publicaciones')
    
    @include('admin.partials.modal-informes')

<br>
<br>