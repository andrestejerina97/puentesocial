
    <div class="modal" tabindex="-1" id="modal_oportunityPhotos" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="input-field">
                    <label for="">Agregar Imágenes: <span>(Para agregar nuevos haga click en "Examinar")</span></label>
                  <input type="file" name="oportunityPhotos[]" id="oportunityPhotos" class="form-control" multiple>
                </div>
                <hr>
                <div class="input-field">
                  <label for=""> Mis Imágenes actuales: </label>
                </div>
                @isset($oportunityPhotos)
                @foreach ($oportunityPhotos as $oportunityPhoto)

                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <td></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <a href="{{asset('storage/oportunities/'.$oportunityPhoto->publications_id."/".$oportunityPhoto->photo)}}" target="_blanck" class="btn-table">{{$oportunityPhoto->photo}}</a>
                          </td>                      
                          <td>
                          <a href="javascript:;" data-id='{{$oportunityPhoto->id}}'  data-publications_id={{$oportunityPhoto->publications_id}} onclick="eliminar_oportunityPhoto(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
              
                      </td>
                      </tr>
                    </tbody>

                  </table>
                </div>
                                    
                @endforeach
                @endisset



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-success" 
              data-dismiss="modal"
              @isset($publications)
              onclick="actualizar();"
              >Guardar</button>
              @else
              data-dismiss="modal"
              >Aceptar</button>
              @endisset
              
              
            </div>
          </div>
        </div>
      </div>

    