<br>
<div class="input-field">
    <input type="text" id="code" 
    data-msg="Por favor debes completar este campo antes de guardar"
    name="code" class="form-control"  placeholder="Código de oportunidad Ej. PY202" 
    @isset($publication->code)
    value="{{$publication->code}}"
    @else
    value="#OP"
    @endisset
    required
    >

</div>
<br>
<div class="input-field">
    <input type="text"
    data-msg="Por favor debes completar este campo antes de guardar"

    name="name_case" class="form-control"
     placeholder="Nombre de la oportunidad"
     @isset($publication->name_case)
    value="{{$publication->name_case}}"
     @endisset
     required
     >
</div>
<br>
<div class="input-field">
    <label for="">Categorías (para seleccionar varias mantenga presionado la tecla "Ctrl"):</label>

    <select name="oportunity_category[]" multiple class="form-control" id="oportunity_category">
        @isset($oportunity_categories)
        @foreach ($oportunity_categories as $oportunity_category)
        <option value="{{$oportunity_category->id}}"
        @isset($selected_categories)
        @foreach ($selected_categories as $selected_category)
        @if ($selected_category->oportunity_categories_id==$oportunity_category->id)
            selected    
        @endif
        @endforeach
        @endisset
        >{{$oportunity_category->name}}</option>
        @endforeach
        @endisset
    </select>
</div>	
<br>
<div class="input-field">
    <label>Ubicación</label> 

    <input type="text" name="city_province" class="form-control" placeholder="Ciudad y provincia..."
    @isset($publication->city_province)
    value="{{$publication->city_province}}"
    @endisset
    >
</div>	
<br>
@isset($publication)
<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-6">
        <label>Progreso de la oportunidad</label> 
        <input type="number" class="form-control" placeholder="Progreso de la oportunidad..."  name="progress"
        @isset($publication->progress)
        value="{{$publication->progress}}"
        @endisset
        disabled
        >
    </div>
    <br>
    <div class="col-lg-6 col-md-6 col-xs-6">
        <div class="input-field">
            <a href="javascript:;" data-id_oportunity="{{$publication->id}}"  data-active="1" id="btn_finalize2" onclick="finalize(this);" class="btn-table">
                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i> Objetivo cumplido</a>
            </div>

    </div>
</div>
@else

    
@endisset

<br>
<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-6">
        <label for="">Me gusta:</label>
        <input type="number" name="likes" class="form-control" placeholder="Me gusta"
        @isset($publication->likes)
        value="{{$publication->likes}}"
        @endisset
        >
    </div>
    <div class="col-lg-6 col-md-6 col-xs-6">
            <label for="">Compartidos:</label>
            <input type="number" name="shared" class="form-control" placeholder="Compartidos"
            @isset($publication->shared)
            value="{{$publication->shared}}"
            @endisset
            >
    </div>
</div>
<br>

<br>
<div class="input-field">
<label for="">Oportunidad</label>
</div>
<div class="input-field">
    <textarea class="form-control" name="details" id="details" rows="3" placeholder="Detalle aquí la oportunidad..">@isset($publication->details){{$publication->details}}@endisset</textarea>
</div>

<br>
<div class="input-field">
    <label for="">A quines va dirigido:</label>
    </div>
<div class="input-field">
    <textarea class="form-control" name="objetive" 
    id="objetive" rows="3"
     placeholder="">@isset($publication->objetive){{$publication->objetive}} @endisset</textarea>
</div>
<br>
<div class="input-field">
    <label for="">Duración o stock:</label>
    </div>
<div class="input-field">
  
    <input class="form-control" name="duration" 
    id="duration" 
     placeholder="" @isset($publication->duration) value="{{$publication->duration}}" @endisset >
</div>
<br>


<br>
<div class="input-field">
    <label for="">Contacto:</label>
    </div>
    <div class="input-field">
  
        <input class="form-control" name="web" 
        id="web" 
         placeholder="" @isset($publication->web) value="{{$publication->web}}" @endisset >
    </div>
<br>
<br>
<div class="input-field">
    <label for="">Email:</label>
    </div>
    <div class="input-field">
  
        <input class="form-control" name="email" 
        id="email" 
         placeholder="" @isset($publication->email) value="{{$publication->email}}" @endisset >
    </div>
<br>
  <br>
  <div class="input-field">
    <label for="">Whatsapp:</label>
    </div>
    <div class="input-field">
  
        <input class="form-control" name="cellphone" 
        id="cellphone" 
         placeholder="" @isset($publication->cellphone) value="{{$publication->cellphone}}" @endisset >
    </div>
<br>
    <br>
    
<div class="panel">
    <div class="panel-body personal-data">
        <div class="input-field">
            <label for="">Datos de la persona ayudada / responsable de familia /responsable de grupo u organización u institución</label>
        </div>
        <div class="form-group">
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="input-field">
                    <label for="">Nombre completo:</label>
                    </div>
                <div class="input-field">
                    <input type="text" name="name" class="form-control" placeholder=""  
                @isset($publication->name)
                value="{{$publication->name}}"
                @endisset
                >
                </div>
                <br>
                <div class="input-field">
                    <label for="">Dirección:</label>
                    </div>
                <div class="input-field">
                    <input type="text" name="home" class="form-control" placeholder="..."  
                @isset($publication->home)
                value="{{$publication->home}}"
                @endisset
                >
                </div>
                <br>
                <div class="input-field">
                    <label for="">Telefono de contacto:</label>
                    </div>
                    <div class="input-field">
                <input type="text" name="contact_phone" id="contact_phone" class="form-control" placeholder=""  
                @isset($publication->contact_phone)
                value="{{$publication->contact_phone}}"
                @endisset
                >
              </div>
                <br>
                <div class="input-field">
                    <label for="">Email: </label>
                    </div>
                <div class="input-field">
                    <input type="text" name="email_contact" class="form-control" placeholder=""  
                    @isset($publication->email_contact)
                    value="{{$publication->email_contact}}"
                    @endisset
                    >
                </div>
            </div>
           
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="input-field">
                    <div class="input-field">
                        <label for="">Dni:</label>
                        </div>
                    <input type="text" name="dni" class="form-control" placeholder=""  
                    @isset($publication->dni)
                    value="{{$publication->dni}}"
                    @endisset
                    >
                  </div>
                  <br>
        
              <div class="input-field">
                <label for="">Ciudad/Provincia: </label>
                </div>
            <div class="input-field">
                <input type="text" name="city_province_contact" class="form-control" placeholder=""  
            @isset($publication->city_province_contact)
            value="{{$publication->city_province_contact}}"
            @endisset
            >
            </div>
              <br>
              <div class="input-field">
                <div class="input-field">
                    <label for="">Celular:</label>
                    </div>
                <input type="text" name="cellphone_contact" id="cellphone_contact" class="form-control" placeholder=""  
                @isset($publication->cellphone_contact)
                value="{{$publication->cellphone_contact}}"
                @endisset>
              </div>
              <br>
              <div class="input-field">
                <div class="input-field">
                    <label for="">Web:</label>
                    </div>
                <input type="text" name="web_contact" id="web_contact" class="form-control" placeholder=""  
                @isset($publication->web_contact)
                value="{{$publication->web_contact}}"
                @endisset
                >
              </div>    
            </div>
           
        </div><br>
        <div class="col-lg-12 col-xs-12 col-md-12">
            <br> 
            <div class="input-field">
                <label for="">Otros:</label>
                </div>
            <div class="input-field">
            <textarea name="others" placeholder="" id="others" class="form-control" rows="3">@isset($publication->others){{$publication->others}}@endisset</textarea>
            
          </div>
        </div>
        <br>
        <div class="col-lg-12 col-xs-12 col-md-12">
            <br> 
      
            <div class="input-field">

    <button type="button" data-target="#modal_oportunityPhotos" data-toggle="modal" id="submit" class="btn btn-success "><i class="fa fa-image"></i> Subir imágenes</button>

          </div>
        </div>  
        
    </div>
        
</div>

@include('admin.partials.modal-oportunity-photos')
