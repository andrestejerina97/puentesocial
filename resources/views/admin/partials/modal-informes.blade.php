
    <div class="modal" tabindex="-1" id="modal_reports" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="input-field">
                    <label for="">Agregar Informes: <span>(Para agregar nuevos haga click en "Elegir archivos")</span></label>
                  <input type="file" name="reports[]" id="photo" class="form-control" multiple>
                </div>
                <hr>
                <div class="input-field">
                  <label for=""> Mis Informes actuales: </label>
                </div>
                @isset($reports)
                @foreach ($reports as $report)

                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <td></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <a href="{{asset('storage/informes/'.$report->publications_id."/".$report->report)}}" target="_blanck" class="btn-table">{{$report->report}}</a>
                          </td>                      
                          <td>
                          <a href="javascript:;" data-report_id={{$report->id}}  data-publications_id={{$report->publications_id}} onclick="eliminar_reporte(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
              
                      </td>
                      </tr>
                    </tbody>

                  </table>
                </div>
                                    
                @endforeach
                @endisset



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-success" 
              data-dismiss="modal"
              @isset($publications)
              onclick="actualizar();"
              >Guardar</button>
              @else
              data-dismiss="modal"
              >Aceptar</button>
              @endisset
              
              
            </div>
          </div>
        </div>
      </div>

    