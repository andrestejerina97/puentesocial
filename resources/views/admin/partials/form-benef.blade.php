
 <div class="col-lg-8 col-xs-8">
    <div class="input-field ">
        <label for=""></i>Nombre del beneficiario: </label>
        <input type="text" id="name" name="name" class="form-control" placeholder="" 
        @isset($donor->users->name)
        value="{{$donor->users->name}}"
        @endisset
        required
        disabled
        >
    </div>

    <br>
    <div class="input-field ">
        <label for=""><i class="fa fa-edit"></i>Detalles: </label>
        <textarea name="observations" class="form-control" id="observations" rows="4">@isset($donor->hearts){{$donor->observations}}@endisset</textarea>
    </div>
  </div>
  <div class="col-lg-4 col-md-4  col-xs-4 text-center">
    @isset($donor)
    <a onclick="#" href="javascript:;" style="color: grey" data-id={{$donor->users->id}} data-photo="{{$donor->users->photo}}" data-publications_id="{{$donor->users->publications_id}}" ><img loading="lazy"  id="photo"  src="{{asset('storage/users/'.$donor->users->id."/".$donor->users->photo)}}" class="img-thumbnail" style="max-width: 50px; min-width:50px; max-height: 50px; " alt="" >
    </a>
<div class="input-field">
    <b>Foto del usuario</b>

</div>
@endisset    

  </div>
<br>
