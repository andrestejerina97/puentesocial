
    <div class="modal" tabindex="-1" id="modal_files" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="input-field">
                    <label for="">Agregar Fotografías: <span></span></label>
                  <input type="file" name="photos[]" id="photo" class="form-control" multiple>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-success" 
              @isset($publication)
              onclick="actualizar();"
              >Guardar</button>
              @else
              data-dismiss="modal"
              >Aceptar</button>
              @endisset
            </div>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" id="modal_photo" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="panel">
                   <div>
                       <h4>¿Qué desea hacer?</h4>
                   </div>
                   <div class="input-group">
                    <button type="button" id="btn_modal_delete" onclick="delete_photo(this);"  id="submit" class="btn btn-danger  "><i class="fa fa-image"></i> Eliminar foto</button>
                    <button type="button" id="btn_modal_update" onclick="selected_photo_for_profile(this);" id="submit" class="btn btn-success  "><i class="fa fa-check-circle"></i> Eligir como foto de perfil</button>
                </div>            
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>