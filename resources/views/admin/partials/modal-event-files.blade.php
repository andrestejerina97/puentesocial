
    <div class="modal" tabindex="-1" id="modal_file" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="input-field">
                    <label for="">Agregar Adjuntos: <span>(Para agregar nuevos haga click en "Elegir archivos")</span></label>
                  <input type="file" name="files[]" id="file" class="form-control" multiple>
                </div>
                <hr>
                <div class="input-field">
                  <label for=""> Mis Adjuntos actuales: </label>
                </div>
                @isset($files)
                @foreach ($files as $file)

                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <td></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <a href="{{asset('storage/events/'.$file->events_id."/".$file->file)}}" target="_blanck" class="btn-table">{{$file->file}}</a>
                          </td>                      
                          <td>
                          <a href="javascript:;" data-file_id={{$file->id}}  data-events_id={{$file->events_id}} onclick="eliminar_file(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
              
                      </td>
                      </tr>
                    </tbody>

                  </table>
                </div>
                                    
                @endforeach
                @endisset



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-success" 
              data-dismiss="modal"
              @isset($events)
              onclick="save_event();"
              >Guardar</button>
              @else
              data-dismiss="modal"
              >Aceptar</button>
              @endisset
              
              
            </div>
          </div>
        </div>
      </div>

    