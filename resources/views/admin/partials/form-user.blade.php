
   <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="input-field">
            <label for="name" class="">{{ __('Name') }}</label>
    
            <div class="">
            <input id="name"  type="text" @isset($user->name) value="{{$user->name}}" @endisset class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
    
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    
        <div class="input-field">
            <label for="email" class="">{{ __('E-Mail Address') }}</label>
    
            <div class="">
                <input id="email" type="email" @isset($user->email) value="{{$user->email}}" @endisset class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
    
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        @isset($user->password)
        <br>
        <div class="input-field">
            <label for="">Si desea cambiar la contreseña complete los campos, de lo contrario mantengalos en blanco.</label>
        </div>
        @endisset
    
        <div class="input-field">
            <label for="password" class="">{{ __('Password') }}</label>
    
            <div class="">
                <input id="password"  class="form-control" type="password" name="password" required autocomplete="new-password">
    
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>        
        <div class="input-field">
            <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
    
            <div class="">
                <input id="password-confirm" type="password"  class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>
        @isset($user->photo)
        <div class="input-field">
            <label for="password-confirm" class="">Foto actual:</label>

            <div class="">

           <img loading="lazy" src="{{asset('storage/users/'.$user->id."/".$user->photo)}}"  style="max-width: 50px; min-width:50px; max-height: 50px;    border-radius: 50%;
           "  id="photo" style=""  >  
                </div>
            </div>
        @endisset 
        
        <div class="input-field">
            <label for="password" class="">Foto de perfil</label>
    
            <div class="">
                <input id="photo" type="file" class="form-control @error('photo') is-invalid @enderror" name="photo" >
    
                @error('photo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="input-field">
            <label for="names" class="">Edad</label>
    
            <div class="">
                <input id="age" type="text" @isset($user->age) value="{{$user->age}}" @endisset  class="form-control @error('age') is-invalid @enderror" name="age" value="{{ old('name') }}" required autocomplete="age" >
    
                @error('age')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="input-field">
            <label for="name" class="">Dirección</label>
    
            <div class="">
                <input id="home" type="text" @isset($user->home) value="{{$user->home}}" @endisset  class="form-control @error('home') is-invalid @enderror" name="home" value="{{ old('home') }}" required autocomplete="home" >
    
                @error('home')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="input-field">
            <label for="name" class="">Género</label>
    
            <div class="">
                <select class="form-control" name="gender" id="gender">
                    @isset($user->gender)
                    <option value="ninguno"
                    @if ($user->gender==null) selected @endif>Seleccionar</option>
                    <option value="Masculino" @if ($user->gender=='Masculino') selected @endif >Masculino</option>
                    <option value="Femenino" @if ($user->gender=='Femenino') selected @endif>Femenino</option>
                    <option value="Otro" @if ($user->gender=='Otro') selected @endif>Otro</option>
                    @else
                    <option value="ninguno">Seleccionar</option>
                    <option value="Masculino" >Masculino</option>
                    <option value="Femenino">Femenino</option>
                    <option value="Otro">Otro</option>
                    @endisset

                </select>
                @error('gender')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <br>
        <div class="input-field">
            <label for="name" class="">Rol</label>
    
            <div class="">
                <select class="form-control" name="role" id="role" required>
                    @isset($user->roles)
                    <option value="" >Seleccionar</option>
                    <option value="admin" @if ($user->hasRole('admin')== true) selected @endif>Administrador</option>
                    <option value="edit" @if ($user->hasRole('edit')== true) selected @endif>Editor</option>
                    <option value="user" @if ($user->hasRole('user')== true) selected @endif>Usuario</option>
                    <option value="business" @if ($user->hasRole('business')== true)selected @endif>Empresa</option>
                    @else
                    <option value="" selected>Seleccionar</option>
                    <option value="admin" >Administrador</option>
                    <option value="edit" >Editor</option>
                    <option value="user" >Usuario</option>
                    <option value="business">Empresa</option>
                    @endisset
 
                </select>
              
            </div>
        </div>
        <br>
        <div class="input-field">
            <label for="name" class="">Permisos</label>
    
            <div class="">
                <select class="  js-states form-control" multiple="multiple" disabled aria-readonly="false" id="permission" >
                    @isset($user->permissions)
                    @foreach ($user->permissions as $permission)
                    <option value="" >{{$permission->name}}</option>
                    @endforeach

                    @else
                    <option value="" >Sin permisos</option>

                    @endisset
                </select>
              
            </div>
        </div>
       </div>
   </div>