<br>
<div class="input-field">
    <input type="text" id="code" 
    data-msg="Por favor debes completar este campo antes de guardar"
    name="code" class="form-control"  placeholder="Código del proyecto Ej. PY202" 
    @isset($publication->code)
    value="{{$publication->code}}"
    @else
    value="#PY"
    @endisset
    required
    >

</div>
<br>
<div class="input-field">
    <input type="text"
    data-msg="Por favor debes completar este campo antes de guardar"

    name="name_case" class="form-control"
     placeholder="Nombre del proyecto"
     @isset($publication->name_case)
    value="{{$publication->name_case}}"
     @endisset
     required
     >
</div>
<br>
<div class="input-field">
    <label for="">Categorías (para seleccionar varias mantenga presionado la tecla "Ctrl"):</label>

    <select name="proyect_category[]" multiple class="form-control" id="proyect_category">
        @isset($proyect_categories)
        @foreach ($proyect_categories as $proyect_category)
        <option value="{{$proyect_category->id}}"
        @isset($selected_categories)
        @foreach ($selected_categories as $selected_category)
        @if ($selected_category->proyect_categories_id==$proyect_category->id)
            selected    
        @endif
        @endforeach
        @endisset
        >{{$proyect_category->name}}</option>
        @endforeach
        @endisset
    </select>
</div>	
<br>
<div class="input-field">
    <label>Ubicación</label> 

    <input type="text" name="city_province" class="form-control" placeholder="Ciudad y provincia..."
    @isset($publication->city_province)
    value="{{$publication->city_province}}"
    @endisset
    >
</div>	
<br>
@isset($publication)
<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-6">
        <label>Progreso del proyecto</label> 
        <input type="number" class="form-control" placeholder="Progreso del proyecto..."  name="progress"
        @isset($publication->progress)
        value="{{$publication->progress}}"
        @endisset
        disabled
        >
    </div>
    <br>
    <div class="col-lg-6 col-md-6 col-xs-6">
        <div class="input-field">
            <a href="javascript:;"  data-id_proyect="{{$publication->id}}" data-active="1" id="btn_finalize2" onclick="finalize(this);" class="btn-table">
                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i> Objetivo cumplido</a>
            </div>

    </div>
</div>
@else

    
@endisset

<br>
<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-6">
        <label for="">Me gusta:</label>
        <input type="number" name="likes" class="form-control" placeholder="Me gusta"
        @isset($publication->likes)
        value="{{$publication->likes}}"
        @endisset
        >
    </div>
    <div class="col-lg-6 col-md-6 col-xs-6">
            <label for="">Compartidos:</label>
            <input type="number" name="shared" class="form-control" placeholder="Compartidos"
            @isset($publication->shared)
            value="{{$publication->shared}}"
            @endisset
            >
    </div>
</div>
<br>
<div class="input-field">
    <label>Link podcast</label> 

    <input type="text" name="link_podcast" class="form-control" placeholder="Link podcast..."
    @isset($publication->link_podcast)
    value="{{$publication->link_podcast}}"
    @endisset
    >
</div>
<br>
<div class="input-field">
<label for="">Detalles del proyecto</label>
</div>
<div class="input-field">
    <textarea class="form-control" name="details" id="details" rows="3" placeholder="">@isset($publication->details){{$publication->details}}@endisset</textarea>
</div>
<br>
<div class="input-field">
    <label for="">Necesidades:</label>
    </div>
<div class="input-field">
    <textarea class="form-control" name="report" 
    id="report" rows="3"
     placeholder="">@isset($publication->report){{$publication->report}} @endisset</textarea>
</div>
<br>
<div class="input-field">
    <label for="">Objetivo:</label>
    </div>
<div class="input-field">
    <textarea class="form-control" name="objetive" 
    id="objetive" rows="3"
     placeholder="">@isset($publication->objetive){{$publication->objetive}} @endisset</textarea>
</div>
<br>
<div class="input-field">
    <label for="">Duración:</label>
    </div>
<div class="input-field">
  
    <input class="form-control" name="duration" 
    id="duration" 
     placeholder="" @isset($publication->duration) value="{{$publication->duration}}" @endisset >
</div>
<br>
<div class="input-field">
    <label for="">Plan de ayuda:</label>
    </div>
<div class="input-field">

    <textarea class="form-control" name="help_plan" 
    id="help_plan" rows="3"
     placeholder="Plan de ayuda...">@isset($publication->help_plan){{$publication->help_plan}} @endisset</textarea>
</div>
<br>
<div class="input-field">
    <label for="">Acuerdo:</label>
    </div>
<div class="input-field">
    <textarea class="form-control" name="agreement" 
    id="agreement" rows="3"
     placeholder="">@isset($publication->agreement){{$publication->agreement}} @endisset</textarea>
</div>
<br>
<br>
  <br>
    <br>
    
<div class="panel">
    <div class="panel-body personal-data">
        <div class="input-field">
            <label for="">Datos de la persona ayudada / responsable de familia /responsable de grupo u organización u institución</label>
        </div>
        <div class="form-group">
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="input-field">
                    <label for="">Nombre completo:</label>
                    </div>
                <div class="input-field">
                    <input type="text" name="name" class="form-control" placeholder=""  
                @isset($publication->name)
                value="{{$publication->name}}"
                @endisset
                >
                </div>
                <br>
                <div class="input-field">
                    <label for="">Dirección:</label>
                    </div>
                <div class="input-field">
                    <input type="text" name="home" class="form-control" placeholder="..."  
                @isset($publication->home)
                value="{{$publication->home}}"
                @endisset
                >
                </div>
                <br>
                <div class="input-field">
                    <label for="">¿Quién lo ingresó?</label>
                    </div>
                <div class="input-field">
                    <input type="text" name="login_name" class="form-control" placeholder=""  
                @isset($publication->login_name)
                value="{{$publication->login_name}}"
                @endisset
                >
                </div>
                <br>
                <div class="input-field">
                    <label for="">Antecedentes/Historia clínica:</label>
                    </div>
                <div class="input-field">
                    <input type="text" name="clinical_history" class="form-control" placeholder=""  
                    @isset($publication->clinical_history)
                    value="{{$publication->clinical_history}}"
                    @endisset
                    >
                </div>
            </div>
           
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="input-field">
                    <div class="input-field">
                        <label for="">Dni:</label>
                        </div>
                    <input type="text" name="dni" class="form-control" placeholder=""  
                    @isset($publication->dni)
                    value="{{$publication->dni}}"
                    @endisset
                    >
                  </div>
                  <br>
              <div class="input-field">
                <div class="input-field">
                    <label for="">Telefono de contacto:</label>
                    </div>
                <input type="text" name="contact_phone" id="contact_phone" class="form-control" placeholder=""  
                @isset($publication->contact_phone)
                value="{{$publication->contact_phone}}"
                @endisset
                >
              </div>
              <br>
              <div class="input-field">
                <div class="input-field">
                    <label for="">Contacto familia:</label>
                    </div>
                <input type="text" name="family_contact" id="family_contact" class="form-control" placeholder=""  
                @isset($publication->family_contact)
                value="{{$publication->family_contact}}"
                @endisset>
              </div>
              <br>
              <div class="input-field">
                <div class="input-field">
                    <label for="">Médico de contacto:</label>
                    </div>
                <input type="text" name="medical_contact" id="medical_contact" class="form-control" placeholder=""  
                @isset($publication->medical_contact)
                value="{{$publication->medical_contact}}"
                @endisset
                >
              </div>    
            </div>
           
        </div><br>
        <div class="col-lg-12 col-xs-12 col-md-12">
            <br> 
            <div class="input-field">
                <label for="">Otros:</label>
                </div>
            <div class="input-field">
            <textarea name="others" placeholder="" id="others" class="form-control" rows="3">@isset($publication->others){{$publication->others}}@endisset</textarea>
            
          </div>
        </div>
        <br>
       
        
    </div>
        
</div>