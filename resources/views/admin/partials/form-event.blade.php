
 <div class="col-lg-7 col-xs-7">
    <div class="input-field ">
        <label for=""></i>Fecha del evento *: </label>
        <input type="date" id="date" name="date" class="form-control" placeholder="" 
        @isset($event->date)
        value="{{$event->date}}"
        @endisset
        required
        
        >
    </div>
    <br>

    <div class="input-field ">
        <label for=""><i class="fa fa-edit"></i>Detalles/Descripción: </label>
        <textarea name="description" placeholder="Escriba aquí el detalle..." class="form-control" id="observations" rows="4">@isset($event->description){{$event->description}}@endisset</textarea>
    </div>
  </div>

  
<br>
<div class="col-lg-5 col-xs-5">
<div class="input-field">
    @isset($event)
    <div class="input-field">
        <label for="">Creado por:</label>
    </div>
    <div class="input-field">
        <div class="col-lg-6">
            <input type="text" name="name" class="form-control" placeholder="Nombre y Apellido..." 
            @isset($event->users->name)
            value="{{$event->users->name}}"
            @endisset
            disabled
            >
           
        </div>
        
        <div class="col-lg-6">
            <input type="text" name="created_at" class="form-control" placeholder="Fecha y hora" 
            @isset($event->created_at)
            value="{{$event->created_at}}"
            @endisset
            disabled
            >
        </div>
    </div>
     <br>
    @endisset
    
     <br>
     @isset($event->modifiedUsers)
    
    <div class="input-field">
        <label for="">Ultima actualización:</label>
    </div>
    <div class="input-field">
        <div class="col-lg-6">
            <input type="text" name="name" class="form-control" placeholder="Nombre y Apellido..." 
            value="{{$event->modifiedUsers->name}}"
            disabled
            >
        </div>
        <div class="col-lg-6">
            <input type="text" name="created_at" class="form-control" placeholder="Fecha y hora" 
            @isset($event->updated_at)
            value="{{$event->updated_at}}"
            @endisset
            disabled
            >
        </div>
    </div>
    @endisset
    <hr>
<br>
    <div class="col-lg-12 text-center">
        <button type="button" data-target="#modal_file" data-toggle="modal"  id="submit" class="btn btn-edit-publication ">Adjuntos</button>
    </div>
</div>
</div>
@include('admin.partials.modal-event-files')
