@extends('layouts.admin')

@section('content')
     <!-- Side navigation -->
<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
            <div class="panel-header">
                {{$publication->name_case}}

            </div>
        <div class="table-responsive">
    <table class="table ">

        <thead class=" ml-4">

        
        <tr>
        <th class="text-left">Usuario</th>
        <th class="text-center" >Mensaje</th>

        <th class="text-center" style="width: 20%">
        </th>

    </tr>
        </thead>
    <div class="panel-body">

    <tbody>
        @foreach($publication->comments as $comment_id => $comments)

        @if($comments->child_id =='')
        <tr style=" background-color: #d6d8d7" >
            <td style="width: 30%"> <img  loading="lazy" 
                src="{{asset('storage/users/'.$comments->commenter->id."/".$comments->commenter->photo)}}"  style="max-width: 30px; min-width:30px; max-height: 30px; border-radius: 20px;" alt=""><b>{{$comments->commenter->name}}</b>
            </td>
                <td id="comment-{{$comments->id}}"><p> {{$comments->comment}}</p>
                    <p><span style="color: #575454;"><small ><i class="fa fa-thumbs-up" aria-hidden="true"></i> me gusta</small> <small >@isset($comments->likeCounter['count']){{$comments->likeCounter['count']}}@endisset</small><small> -  Mensaje {{\Carbon\Carbon::parse($comments->created_at)->diffForHumans() }}</small></span></p>	<hr> 
                </td>
                <td>
                    <a href="javascript:;" onclick="eliminar(this);" data-id_case="{{$comments->id}}" class="btn-table"><i class="fa fa-remove" aria-hidden="true"></i></a>
                </td>							 
            </tr>
        
        @endif 
       @if (count($comments->children) >0)
  
       @foreach ($comments->children as $children)
     
       <tr class="ml-1">
           <td colspan="3"> 
             <table>
                 <tbody >
                 <tr>
                    <td style="width: 30%"></td>
                     <td >
                   
                        <img  loading="lazy" 
                        src="{{asset('storage/users/'.$children->commenter->id."/".$children->commenter->photo)}}"  style="max-width: 30px; min-width:30px; max-height: 30px; border-radius: 20px;" alt="">
                        <b>{{$children->commenter->name}}</b>  
                        <p class="text-center " style="margin-top: 10px;">{{$children->comment}}</p>
                         <p><span style="color: #696767;"><small ><i class="fa fa-thumbs-up" aria-hidden="true"></i> me gusta</small> <small >@isset($children->likeCounter['count']){{$children->likeCounter['count']}}@endisset</small> - Mensaje {{\Carbon\Carbon::parse($children->created_at)->diffForHumans() }}</small></span></p>	<hr>
                     </td>
                     <td style="width: 40%">
                        <a href="javascript:;" onclick="eliminar(this);" data-id_case="{{$children->id}}" class="btn-table"><i class="fa fa-remove" aria-hidden="true"></i></a>
                    </td>								 
                       </tr>
                       </tbody>
             </table>
           </td>

       </tr>
          
     @endforeach
       @endif
     
      
     @endforeach
       
  
     
    </tbody>
</div>
    </table>
    </div>
</div>
</div>    
</div> 

@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(function(){
        $('.nav-text').each(function () {
            if ($(this).text()=="Comentarios") {
                $(this).parent().addClass("active");
            }
     });
});
function eliminar(a) {
          swal({
              text: 'Seguro que deseas eliminar el message, esto no se puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
                let id_case=$(a).data("id_case");
               let url="{{route('admin.delete.comment','id')}}"
               url=url.replace("id",id_case);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Mensaje eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();


                      });
                         

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
         
      }


   
    </script>    
@endsection