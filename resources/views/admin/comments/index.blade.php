@extends('layouts.admin')

@section('content')
     <!-- Side navigation -->
<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
         
        <div class="table-responsive">
    <table class="table table-hover">

        <thead class=" ml-4">
            <tr style="border: 1px solid transparent">
                <td>
                <form id="form_1"  action="{{route("admin.search.comment")}}" method="POST">
                  @csrf
                  <input type="hidden" id="filter" name="filter" value="code">
    
                    <div class="input-group col-lg-9 col-md-8 ">
                      <a href="javascript:{}" onclick="document.getElementById('form_1').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                      <input placeholder="Buscar por N°" type="text" class="form-control" id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                    </div>
                  </form>
                </td>
                <td>
                  <form id="form_2"  action="{{route("admin.search.comment")}}" method="POST">
                    @csrf
                    <input type="hidden" id="filter" name="filter" value="name_case">
                    <div class="input-group col-lg-9 col-md-8">
                      <a href="javascript:{}" onclick="document.getElementById('form_2').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                      <input type="text" placeholder="Buscar por nombre.." class="form-control" id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                    </div>
                  </form>
    
                </td>
                <td>
                  <form id="form_3" action="{{route("admin.search.comment")}}" method="POST">
                    @csrf
                    <input type="hidden" id="filter" name="filter" value="city_province">
    
                    <div class="input-group col-lg-9 col-md-8">
                      <a href="javascript:{}" onclick="document.getElementById('form_3').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                      <input placeholder="Buscar por ubicación" type="text" class="form-control" name="search" id="search" aria-describedby="inputGroupSuccess1Status">
                    </div>
                  </form>
                </td> 
                   <td></td>
                   <td></td>
              </tr>
        
        <tr>
        <th class="text-left">Nro</th>
        <th class="text-left">Nombre</th>
        <th class="text-left" >Ubicación</th>

        <th class="text-center" style="width: 20%">
        </th>

    </tr>
        </thead>
    <div class="panel-body">

    <tbody>
        @if ($publications->count()>0)
        @foreach($publications as $publication)
        @isset($publication->commentable)

        <tr>
        <td class="text-left">{{$publication->commentable->code}}</td>
        <td class="text-left">{{$publication->commentable->name_case}}</td>
        <td class="text-left">{{$publication->commentable->city_province}}</td>

        <td>
            @isset($publication->unread)
            <i class="fa fa-comments active"></i> {{$publication->unread}}
            @else
            <i class="fa fa-comments "></i> {{$publication->readed}}

            @endisset

            <a href="{{route('admin.view.comment',['id'=>$publication->commentable->id])}}" class="btn-table"><i class="fa fa-plus" aria-hidden="true"></i></a>

        </td>
      
        </tr>
        @endisset

        @endforeach
        @else
       <tr><h3>No hay resultados disponibles (0)</h3></tr> 
        @endif

    </tbody>

</div>
    </table>
    @if($publications instanceof \Illuminate\Pagination\LengthAwarePaginator )
    {{$publications->links()}}
    @endif
    </div>
</div>
</div>    
</div> 

@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(function(){
        $('.nav-text').each(function () {
            if ($(this).text()=="Comentarios") {
                $(this).parent().addClass("active");
            }
     });
});

   
    </script>    
@endsection