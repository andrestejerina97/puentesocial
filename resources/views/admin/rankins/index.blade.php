@extends('layouts.admin')
@section('css')

<style>
.avatar {
  display: block;

  vertical-align: middle;
  border-radius: 50%;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
}

.ranking{
  position: relative;
  left: 40%;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: rgb(247, 235, 235);
  padding: 9px;
  font-size: 140%;
  font-weight: 700;
}
.panel {
  border-width: 0;
  outline: none;
  border-radius: 2px;
  box-shadow: 0 1px 4px rgba(0, 0, 0, .6);

  
  transition: background-color .3s;
}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->
<!-- Page content -->

<div class="main">
    @php
     $ranking=1;   
    @endphp
    
    <div class="row ">
      <div class="col-lg-1 col-md-1 col-sm-1">
        
      </div>
      <div class="col-lg-10 col-md-10 col-sm-12">

     @foreach ($donors as $donor)

     <div class="col-lg-4 col-md-4 col-sm-6">

      <div class="panel panel-default" style="min-height: 250px !important;max-height: 250px !important;">
           
            <div class="panel-body">
            <div style="margin-top: 5px;">                    
            <img class="avatar center" src="{{asset('storage/users/'.$donor->users->id."/".$donor->users->photo )}}"  style="max-width: 50px; min-width:50px; max-height: 50px; " alt="Sin foto">
            <h5 class="text-center"><div class="ranking">{{$ranking}}</div></h5>

          </div>

            <h5 class="text-center">{{$donor->users->name}}</h5>
            <hr>
            <a href="{{route('admin.filter.rankin',$donor->users->id)}}" style="">
            <div>
            <h5 class="text-center" style="color: #03910e;">{{$donor->hearts}} Corazones <i class="fa fa-heart " ></i></h5>
            <div class="text-center"><small class="text-center" style="color: #a1ada2;">(Click aquí para ver)</small>
            </div>
           </div>
            </a>

          </div>
          </div>

     </div>
     @php
     $ranking=$ranking +1;   
     @endphp
         @endforeach
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
</div>

    
 
 
</div> 

@endsection
@section('scripts')
  <script>
     
      </script>

@endsection