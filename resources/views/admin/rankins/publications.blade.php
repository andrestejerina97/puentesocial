@extends('layouts.admin')

@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                      <a href="{{route('rankins.index')}}" class="btn-table"><i class="fa fa-arrow-left"></i> 
                    </a>
                    Volver a rankin  

                      </div>
                    <div class="col-lg-9 col-md-9 col-12 col-sm-12">

                    </div>
                </div>
            </div>
         
        <div class="table-responsive">
    <table class="table table-hover">

        <thead class=" ml-4">

       
        <tr>
        <th class="text-left">Código</th>
        <th class="text-left">Nombre</th>
        <th class="text-left">Progreso</th>
        <th class="text-left" style="width: 15%" >Detalles</th>
        <th class="text-left" style="width: 11%"></th>

        </th>

    </tr>
        </thead>
    <div class="panel-body">

    <tbody>
        @foreach($publications as $donor)
        <tr>
        <td class="text-left">{{$donor->publications->code}}</td>
        <td class="text-left">{{$donor->publications->name_case}}</td>
        <td class="text-left">{{$donor->publications->progress}}</td>

        <td class="text-left">{{$donor->observations}}</td>
        <td>
            @switch($donor->publications->categories_id)
                @case(1)
                <a href="{{route('admin.edit.case',['id'=>$donor->publications->id])}}" class="btn-admin">Ir</a>
                </a> 
                    @break
                @case(2)
                <a href="{{route('admin.edit.proyect',['id'=>$donor->publications->id])}}" class="btn-admin">Ir</a>
                </a> 
                    @break
                @case(3)
                <a href="{{route('admin.edit.oportunity',['id'=>$donor->publications->id])}}" class="btn-admin">Ir</a>
                </a> 
                    @break
                @default
            @endswitch
        </td>
        </tr>
        @endforeach

    </tbody>
</div>
    </table>
    </div>
</div>
</div>    
</div> 

@endsection