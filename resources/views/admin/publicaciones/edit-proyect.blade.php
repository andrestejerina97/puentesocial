@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->

<!-- Page content -->

<div class="main">
  <div class="panel panel-default">
    <div class="panel-header">
      <div class="row">
        @isset($publications)        
        @foreach ($publications as $publication)
        <div class="col-lg-3 col-md-3">
          <a href="{{route('proyects.index')}}" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
        <b> {{$publication->code}}</b>        
          </div>
        <div class="col-md-6"></div>
    
        <div class="col-lg-3 col-md-3 align-content-rigth">
          <a href="{{route('publication.view',['id'=>$publication->id])}}" target="_blanck" class="btn-table"><i class="fa fa-globe"></i></a>
        <a href="{{route('events.index',['publication_id'=>$publication->id])}}" class="btn-table"><i class="fa fa-history"></i></a>
        <a href="javascript:;" id="btn_delete" data-id_proyect="{{$publication->id}}" data-active="1" onclick="eliminar(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>
         <a href="javascript:;"  onclick="actualizar();" id="btn_save" class="btn-table " ><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
        <a href="javascript:;" data-id_proyect="{{$publication->id}}" data-active="1" id="btn_active" onclick="activar(this);" 
          
          @if ($publication->active==1)
           class="btn-table active"   
           @else
           class="btn-table"
           @endif
           ><i class="fa fa-eye" aria-hidden="true"></i></a>
           <a href="javascript:;"  data-id_proyect="{{$publication->id}}" data-active="1" id="btn_finalize" onclick="finalize(this);" class="btn-table"
           @if ($publication->finalize==true)
           class="btn-table active"   
           @else
           class="btn-table"
           @endif
           ><i class="fa fa-check-circle" aria-hidden="true"></i></a>

      </div>        

        @endforeach
        @else
        <div class="col-lg-3 col-md-3">
          <a href="javascript:history.back()" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
            <b >Nuevo proyecto</b>        
          </div>
        <div class="col-md-6"></div>
        <div class="col-lg-3 col-md-3 align-content-rigth">
          
          <a href="javascript:;" class="btn-table"><i class="fa fa-history"></i></a>
          <a href="javascript:;" id="btn_delete" data-id_proyect="-1" data-active="-1" onclick="eliminar(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>
         <a href="javascript:;"  onclick="guardar();" id="btn_save" class="btn-table " ><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
        <a href="javascript:;" data-id_proyect="-1" data-active="-1" id="btn_active" onclick="activar(this);" class="btn-table"><i class="fa fa-eye" aria-hidden="true"></i></a>
        <!--<a href="javascript:;" data-id_proyect="-1" data-active="-1" id="btn_active" onclick="finalize(this);" class="btn-table"><i class="fa fa-check-circle" aria-hidden="true"></i></a> -->
      </div>        

        @endisset
        </div>        
    </div>
    <div class="panel-body">

  <div class="row">
    @isset($publications)        
    @foreach ($publications as $publication)
    <form id="form_new" method="POST"  enctype="multipart/form-data" action="{{route('admin.store.proyect')}}">
      @csrf
      <input type="hidden" name="id" value="{{$publication->id}}">
      <div class="col-lg-7 col-xs-7">
        @include('admin.partials.form-proyect-left')

      </div>
      <div class="col-lg-5 col-xs-5">
        @include('admin.partials.form-proyect-rigth')
      </div>
    </form>

      @endforeach
      @else
      <form id="form_new" method="POST"  enctype="multipart/form-data" action="{{route('admin.store.proyect')}}">
        @csrf
      <div class="col-lg-7 col-xs-7">
        @include('admin.partials.form-proyect-left')

      </div>
      <div class="col-lg-5 col-xs-5">
        @include('admin.partials.form-proyect-rigth')
      </div>
    </form>
      @endisset
    </div>
  </div>
  </div>
</div> 

@endsection
@section('scripts')
           
    <!-- carousel logos empresas -->   	
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery.qrcode.min.js')}}"></script>

    <!-- end - carousel logos empresas -->  
      <script>
         /***QR**/
         @isset($publication)
         let text='{{route('publication.view',['id'=>$publication->id])}}';
        $('#qrcode').qrcode(text);
        @if(!$publication->qr)
          $('#qrcode').css('display','none');
        @endif
        function download_img(el) {
      $.ajax({
        type: "POST",
        url: "{{route('admin.qr.case')}}",
        data: {
          'publication':'{{$publication->id}}',
          "_token": "{{ csrf_token() }}",
        },
        success: function( data ) {
        $('#qrcode').css('display','block');

      },
      });
      let canvas = $("#qrcode").find('canvas')[0];
      let ctx = canvas.getContext("2d");
      var image = canvas.toDataURL("image/jpg");
      el.href = image;
    };
         @endisset
  

        /**END QR**/
   var saveActive=1;
   $(function(){
    $("#photo").val('');

        $('.nav-text').each(function () {
            if ($(this).text()=="Proyectos") {
                $(this).parent().addClass("active");
            }
     });
});
$('.carousel-publications').slick({
  centerMode: true,
  lazyLoad: 'ondemand',
  centerPadding: '0px',
  slidesToShow: 1,
  nav:true,
  fade: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        nav:true,

      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        nav:true,

      }
    },
    {
      breakpoint: 365,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        nav:true,

      }
    }
  ]
});

      function guardar(){
        jQuery.validator.setDefaults({
          debug: true,
          success: "Valido"
        });
        $("#form_new").validate({
          rules: {
            field: {
              required: true
            }
          },
          submitHandler: function(form) {
            var formdata = new FormData($(form)[0]);
            $.ajax({
                url         : '{{route('admin.store.proyect')}}',
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.id_proyecto != null) {
                      swal("Excelente!","Proyecto guardado con éxito","success");
                      url="{{route('admin.edit.proyect','id')}}";
                      url=url.replace('id',data.id_proyecto);
                      location.href=url;
                      
                    }
                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {
                swal("Ups!","El número de proyecto ingresado ya existe,por favor ingrese uno nuevo","error");
                    }
                  }
            });
    // do other things for a valid form
          }
        });

        $("#form_new").submit();
      }
      
      function activar(a) {
        if ($(a).data("active")==1) {
          let id_proyect=$(a).data("id_proyect");
          let url="{{route('admin.active.proyect','id')}}"
          url=url.replace("id",id_proyect);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal("Excelente!","Proyecto "+data.message +" con éxito","success");
                      if (data.result ==1) {
                        $("#btn_active").addClass("active");
                      }else{
                        $("#btn_active").removeClass("active");

                      }
                    }
                  },
                  statusCode:{
                    403:function(data) {
                  swal("Ups!","No tienes permiso para realizar esta operación","error");
                    }
                  }
            });
        }else{
          swal("Ups!","No puedes publicar un proyecto que aún no está creado","warning")
        }

      }

      function eliminar(a) {
        if ($(a).data("active")==1) {
          swal({
              text: 'Seguro que deseas eliminar el proyecto".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
                  let id_proyect=$(a).data("id_proyect");
               let url="{{route('admin.delete.proyect','id')}}"
               url=url.replace("id",id_proyect);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Proyecto eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("proyects.index")}}';


                      });
                          $("#btn_delete").data("active",-1);
                      $("#btn_delete").data("id_proyect",-1);

                    }else{

                    }
               
                  },
                  statusCode:{
                    403:function(data) {
                  swal("Ups!","No tienes permiso para realizar esta operación","error");
                    }
                  }
            });
                    break;
                  default:
                  
                }
              });
         
        }else{
          swal("Ups!","No puedes eliminar un proyecto que aún no está creado","warning")
        }

      }


      function actualizar() {
        jQuery.validator.setDefaults({
          debug: true,
          success: "Valido"
        });
        $( "#form_new" ).validate({
          rules: {
            field: {
              required: true
            }
          },
          submitHandler: function(form) {
            var formdata = new FormData($(form)[0]);
            $.ajax({
                url         : '{{route('admin.update.proyect')}}',
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.id_proyecto != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Actualización exitosa",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                    }
                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {
                swal("Ups!","El número de proyecto ingresado ya existe,por favor ingrese uno nuevo","error");
                    }
                  }
            });
    // do other things for a valid form
          }
        });

        $("#form_new").submit();
      }

      function click_photo(a) {
        let photo=$(a).data("photo");
        let publications_id=$(a).data("publications_id");
        let id=$(a).data("id");

        $("#btn_modal_update").data("photo",photo);
        $("#btn_modal_update").data("publications_id",publications_id);
        $("#btn_modal_delete").data("photo",photo);
        $("#btn_modal_delete").data("id",id);
        $("#btn_modal_delete").data("publications_id",publications_id);
        $("#modal_photo").modal("show");
      }
      function selected_photo_for_profile(a) {
        let photo=$(a).data("photo");
        let publications_id=$(a).data("publications_id");
         
          let url="{{route('admin.active.photo','id')}}"
          url=url.replace("id",publications_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"photo="+photo,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Foto de perfil cargada con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }
                  }
            });
      
      }
      function eliminar_reporte(a) {
        let report_id=$(a).data("report_id");
        let publications_id=$(a).data("publications_id");

         $(a).attr("disable",true);
        let url="{{route('admin.delete.report','id')}}"
          url=url.replace("id",publications_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"id_report="+report_id,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success :function(data, textStatus, jqXHR){
                    if (data.result==1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Informe eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }else{
                      swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }
                    $(a).attr("disable",false);

                  }
            });
      }
      function delete_photo(a) {
        let photo=$(a).data("photo");
        let publications_id=$(a).data("publications_id");
        let id=$(a).data("id");

         $(a).attr("disable",true);
        let url="{{route('admin.delete.photo','id')}}"
          url=url.replace("id",publications_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"photo="+photo+"&id_photo="+id,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result==1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Foto eliminada con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }else{
                      swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }
                    $(a).attr("disable",false);

                  }
            });
      }

      function finalize(a) {
        if ($(a).data("active")==1) {
          let id_proyect=$(a).data("id_proyect");
          let url="{{route('admin.finalize.proyect','id')}}"
          url=url.replace("id",id_proyect);

          swal({
              text: 'Seguro que deseas finalizar el proyecto".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,finalizar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case 'catch':
                  $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Se ha finalizado con éxito, será redirigido nuevamente a la seccion anterior",
                        icon :"success",
                      }).then((value) => {
                        location.href="{{route("proyects.index")}}";
                      });
                      
                    }
                  },
                  statusCode:{
                    403:function(data) {
                  swal("Ups!","No tienes permiso para realizar esta operación","error");
                    }
                  }
            });
                    break;
                
                  default:
                    break;
                }
              
              });
        
        }else{
          swal("Ups!","No puedes finalizar un proyecto no creado","warning")
        }

      }

      </script>

@endsection