@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->

<!-- Page content -->

<div class="main">
  <div class="panel panel-default">
    <div class="panel-header">
      <div class="row">
        <div class="col-lg-3 col-md-3">
          <a href="javascript:history.back()" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
        <b> {{$publication->code}}</b>        
          </div>
        <div class="col-md-6"></div>
    
        <div class="col-lg-3 col-md-3 align-content-rigth">
          
      

      </div>        

        <div class="col-lg-3 col-md-3">
               
          </div>
        <div class="col-md-6"></div>
        <div class="col-lg-3 col-md-3 align-content-rigth">
        <!--<a href="javascript:;" data-id_case="-1" data-active="-1" id="btn_active" onclick="finalize(this);" class="btn-table"><i class="fa fa-check-circle" aria-hidden="true"></i></a> -->
      </div>        

        </div>        
    </div>
    <div class="panel-body">

  <div class="row">
      <form id="form_new" aria-disabled="true"  method="POST"  enctype="multipart/form-data" >
        @csrf
        @switch($publication->last_category) 
        @case(1)
        <div class="col-lg-7 col-xs-7">
            @include('admin.partials.form-casos-left')
    
          </div>
          <div class="col-lg-5 col-xs-5">
            @include('admin.partials.form-casos-rigth')
          </div>
            @break
        @case(2)
        <div class="col-lg-7 col-xs-7">
            @include('admin.partials.form-proyect-left')
    
          </div>
          <div class="col-lg-5 col-xs-5">
            @include('admin.partials.form-proyect-rigth')
          </div>
            @break
        @case(3)
        <div class="col-lg-7 col-xs-7">
            @include('admin.partials.form-oportunity-left')
    
          </div>
          <div class="col-lg-5 col-xs-5">
            @include('admin.partials.form-oportunity-rigth')
          </div>
        @break
        @default
            
    @endswitch
    
    </form>
    </div>
  </div>
  </div>
</div> 

@endsection
@section('scripts')
           
    <!-- carousel logos empresas -->   	
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <!-- end - carousel logos empresas -->  
      <script>
   var saveActive=1;
   $("#form_new").find('input').attr('disabled',true);
   $("#form_new").find('textarea').attr('disabled',true);
   $("#form_new").find('button').attr('disabled',true);

   $(function() {
       $("#form_new > *").attr('disabled',true);
   });
$('.carousel-publications').slick({
  centerMode: true,
  lazyLoad: 'ondemand',
  centerPadding: '0px',
  slidesToShow: 1,
  nav:true,
  fade: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        nav:true,

      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        nav:true,

      }
    },
    {
      breakpoint: 365,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        nav:true,

      }
    }
  ]
});


      </script>

@endsection