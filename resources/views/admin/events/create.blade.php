@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->

     @include('sweet::alert')
<!-- Page content -->

<div class="main">
  <div class="panel panel-default">
    <div class="panel-header">
      <div class="row">

        <div class="col-lg-3 col-md-3">
          <a href="{{route('events.index',$publications_id)}}" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
          <b >Nuevo Evento</b> 

          </div>
        <div class="col-md-6"></div>
    
        <div class="col-lg-3 col-md-3 align-content-rigth">
         <a href="javascript:;"  onclick="save_event();" id="btn_save" class="btn-table " ><i class="fa fa-floppy-o fa-1x" aria-hidden="true"></i> Guardar Evento</a>
          
      </div>        
        </div>        
    </div>
    <div class="panel-body">
       

           <section id="section_form" >
            <form   id="form_event" method="POST"  enctype="multipart/form-data" action="{{route('admin.store.case')}}">
              @csrf
              <div class="col-lg-12 col-xs-12">
              <input type="hidden" name="publications_id" value="{{$publications_id}}">
              <input type="hidden" id="users_id" name="users_id" >

                @include('admin.partials.form-event')
              </div>

            </form>
    </section>
    </div>
    <br>
   
    <br>

  </div>
  </div>
</div> 

@endsection
@section('scripts')
  <script>

      function change_section(a) {
          $("#section_table").css('display','none');
          $("#section_form").css('display','block');
          let name=$(a).data("name");
          let users_id=$(a).data("users_id");
          let src="{{asset('storage/users/0')}}";
          src=src.replace('0',$(a).data("photo"));
          $("#users_id").val(users_id);
          $("#name").val(name);
          $("#photo").attr("src",src);


      }
      function save_event(params) {
       
            var formdata = new FormData($("#form_event")[0]);
            $.ajax({
                url         : "{{route('admin.store.event')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Evento agregado con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        let url="{{route('admin.edit.event',['publications_id'=>$publications_id,'events_id'=>0])}}"
                        url=url.replace('0',data.result);
                        location.href=url;

                        
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {
                swal("Ups!","El número de caso ingresado ya existe,por favor ingrese uno nuevo","error");
                    }
                  }
            });
      }
      
      </script>

@endsection