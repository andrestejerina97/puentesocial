@extends('layouts.admin')
@section('css')
<style>
.timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;
}

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 10%;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li > .timeline-panel {
            width: 76%;
            float: left;
            left: 20%;
            border: 1px solid #d4d4d4;
            border-radius: 2px;
            padding: 20px;
            position: relative;
            -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        }

            .timeline > li > .timeline-panel:before {
                position: absolute;
                top: 26px;
                right: -15px;
                display: inline-block;
                border-top: 15px solid transparent;
                border-left: 15px solid #ccc;
                border-right: 0 solid #ccc;
                border-bottom: 15px solid transparent;
                content: " ";
            }

            .timeline > li > .timeline-panel:after {
                position: absolute;
                top: 27px;
                right: -14px;
                display: inline-block;
                border-top: 14px solid transparent;
                border-left: 14px solid #fff;
                border-right: 0 solid #fff;
                border-bottom: 14px solid transparent;
                content: " ";
            }

        .timeline > li > .timeline-badge {
            color: #fff;
            width: 40px;
            height: 40px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: 16px;
            left: 10%;
            margin-left: -25px;
            background-color: #ececec;
            z-index: 100;
            border-top-right-radius: 50%;
            border-top-left-radius: 50%;
            border-bottom-right-radius: 50%;
            border-bottom-left-radius: 50%;
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
        }

            .timeline > li.timeline-inverted > .timeline-panel:before {
                border-left-width: 0;
                border-right-width: 15px;
                left: -15px;
                right: auto;
            }

            .timeline > li.timeline-inverted > .timeline-panel:after {
                border-left-width: 0;
                border-right-width: 14px;
                left: -14px;
                right: auto;
            }

.timeline-badge.primary {
    background-color: #2e6da4 !important;
}

.timeline-badge.success {
    background-color: #3f903f !important;
}

.timeline-badge.warning {
    background-color: #f0ad4e !important;
}

.timeline-badge.danger {
    background-color: #d9534f !important;
}

.timeline-badge.info {
    background-color: #5bc0de !important;
}

.timeline-title {
    margin-top: 0;
    color: inherit;
}

.timeline-body > p,
.timeline-body > ul {
    margin-bottom: 0;
}

    .timeline-body > p + p {
        margin-top: 5px;
    }

@media (max-width: 767px) {
    ul.timeline:before {
        left: 40px;
    }

    ul.timeline > li > .timeline-panel {
        width: calc(100% - 90px);
        width: -moz-calc(100% - 90px);
        width: -webkit-calc(100% - 90px);
    }

    ul.timeline > li > .timeline-badge {
        left: 15px;
        margin-left: 0;
        top: 16px;
    }

    ul.timeline > li > .timeline-panel {
        float: right;
    }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
}
</style>    
@endsection
@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
          <div class="panel-header">
            <div class="row">

                <div class="col-lg-3 col-md-3 text-left">
                    @isset($publication)
                    @switch($publication->categories_id)
                        @case(1)
                        <a href="{{route('admin.edit.case',['id'=>$publication->id])}}" class="btn-table"><i
                            class="fa fa-arrow-left"></i>
                        </a> 
                            @break
                        @case(2)
                        <a href="{{route('admin.edit.proyect',['id'=>$publication->id])}}" class="btn-table"><i
                            class="fa fa-arrow-left"></i>
                        </a> 
                            @break
                        @case(3)
                        <a href="{{route('admin.edit.oportunity',['id'=>$publication->id])}}" class="btn-table"><i
                            class="fa fa-arrow-left"></i>
                        </a> 
                            @break
                        @default
                            
                    @endswitch
                    <b>Volver a  {{$publication->code}} </b>

                    @endisset
                </div>
                <div class="col-md-6">

                </div>

                <div class="col-lg-3 col-md-3 align-content-rigth">
                <a href="{{route('admin.edit.event',$publications_id)}}" id="btn_save"
                        class="btn-table " style="font-weight: 700; color:#3f903f">+ AGREGAR EVENTO</a>

                </div>
            </div>
          </div>
    <div class="panel-body">
        <div class="page-header">
            <h1 id="timeline">Historial de eventos</h1>
            <small>Haga click en el evento para editar</small>
        </div>
          @foreach ($dates as $date)
          <div class="text-left ml-3">
          <button class="btn-admin" style="width: 12%">{{$date->date}}</button>
          </div>
          <ul class="timeline">
         @foreach ($events as $event)
          @if ($date->date == $event->date)
          <li>
            <div  class="timeline-badge"><i class="glyphicon glyphicon-pushpin"></i></div>

            <div class="timeline-panel">
                <div class="text-right"><a href="javascript:;" onclick="eliminar(this);" data-events_id="{{$event->id}}"  class=" timeline-title text-right"> <i class="glyphicon glyphicon-trash"></i></a></div>

                <a href="{{route('admin.edit.event',['publications_id'=> $publications_id,'events_id'=>$event->id])}}"  class="timeline-title">

              <div class="timeline-heading">
              <h4 class="timeline-title">{{$event->description}}</h4>
              <p><small class="text-muted"><i class="glyphicon glyphicon-user"></i>Creado por : {{$event->users->name}}  <i class="glyphicon glyphicon-time"></i> {{$event->created_at}}</small></p>
              </div>

              <div class="timeline-body">
                <p><small class="text-muted"><i class="glyphicon glyphicon-download-alt"></i>2 adjuntos</small></p>
              </div>
            </a>

            </div>

          </li>

          @endif
         @endforeach
        </ul> 

          @endforeach
        @foreach($events as $event)
        
        @endforeach

    </div>
</div>
</div>    
</div> 

@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(function(){
     
});
    function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este evento?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_event=$(a).data("events_id");
               let url="{{route('admin.delete.event','id')}}"
               url=url.replace("id",id_event);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Evento eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();


                      });

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

    </script>    
@endsection