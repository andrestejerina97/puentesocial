@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
     <!-- Side navigation -->

     @include('sweet::alert')
<!-- Page content -->

<div class="main">
  <div class="panel panel-default">
    <div class="panel-header">
      <div class="row">

        <div class="col-lg-3 col-md-3">
          <a href="{{route('events.index',$publications_id)}}" class="btn-table"><i class="fa fa-arrow-left"></i> 
          </a>
          <b >Volver a Historial</b> 

          </div>
        <div class="col-md-6"></div>
    
        <div class="col-lg-3 col-md-3 align-content-rigth">
         <a href="javascript:;"  onclick="save_event();" id="btn_save" class="btn-table " ><i class="fa fa-floppy-o fa-1x" aria-hidden="true"></i> Guardar cambios</a>
          
      </div>        
        </div>        
    </div>
    <div class="panel-body">
       
    <div class="row">

           <section id="section_form">
            <form   id="form_event" method="POST"  enctype="multipart/form-data">
              @csrf
              @foreach ($events as $event)
            <input type="hidden" name="id" value="{{$event->id}}">
              <div class="col-lg-12 col-xs-12">
                @include('admin.partials.form-event')
              </div>
              @endforeach

            </form>
    </section>
    </div>
    <br>
   
    <br>

  </div>
  </div>
</div> 

@endsection
@section('scripts')
  <script>

     
      function save_event(params) {
       
            var formdata = new FormData($("#form_event")[0]);
            $.ajax({
                url         : "{{route('admin.update.event')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Datos actualizados con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        location.reload()
                      });
                    }
                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {
                swal("Ups!","El usuario ingresado ya está registrado como donante,por favor ingrese uno nuevo","error");
                    }
                  }
            });
    // do other things for a valid form
      }

      function eliminar_file(a) {
        let event_id=$(a).data("file_id");
        let publications_id=$(a).data("events_id");
         $(a).attr("disable",true);
        let url="{{route('admin.delete.file','id')}}"
          url=url.replace("id",publications_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"id_event="+event_id,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success :function(data, textStatus, jqXHR){
                    if (data.result==1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Adjunto eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }else{
                      swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }
                    $(a).attr("disable",false);

                  }
            });
      }
      </script>

@endsection