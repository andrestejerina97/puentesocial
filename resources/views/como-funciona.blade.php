
@extends('layouts.public-2')

@section('content')
<!-- Start Service area -->
<div class="col-md-1 col-sm-1">
			
</div>

<div class="col-md-10 col-sm-10 col-xs-12 text-center">
    

    <br>
    <br>
    <br>
    <h2><b><span style="color: #fff">¿QUE ES PUENTE SOCIAL?</span></b></h2>
    <br>
    <p><b><span style="color: #fff"><p>Es una RED SOCIAL que facilita la interacción para que las personas se ayuden entre sí. También es una plataforma para que las empresas se involucren activamente en proyectos que ayuden a la comunidad, promuevan la sustentabilidad y colaboren con el medio ambiente.</p></span></b></p>
    
    <br>
    <video src="https://puentesocial.org/video/puentesocial.mp4" controls style="max-width:100%; height:auto"></video>
    <br>
    <br>
    

    <h4><b>¿POR QUE CREAMOS PUENTE SOCIAL?</b></h4>
    <p><b>A/</b> Nuestra Fundación, desde hace 5 años, trabaja activamente en la promoción de las personas, ayudando a aquellas que por diversas situaciones no lograron desarrollar su potencial. En más de 500 casos, hemos experimentado que las personas a las que ayudamos, en un altísimo porcentaje se convierten en multiplicadores de acciones solidarias con intención de devolver la ayuda.</p>
    <br>
    <p><b>B/</b> Por otro lado, está comprobado mediante innumerables estudios y ratificado por la OMS (Organización Mundial de la Salud) que quienes ayudan a otros desinteresadamente tienen una mayor actividad en dos centros de recompensa del cerebro, provocándoles bienestar a nivel mental, lo que se traduce en felicidad y alegría. Reduciendo el estrés, la ansiedad y la depresión.</p>
    <br>
    <br>
</div>

  <div class="col-md-1 col-sm-1">

</div>
 
<!--FIN DE MODAL PARA COMPARTIR LINK-->

<div id="fb-root"></div>
@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>
<script>
    $("#link_mp").val(sessionStorage.getItem('link_mp'));
    $("#link_paypal").val(sessionStorage.getItem('link_paypal'));

</script>
@endsection

