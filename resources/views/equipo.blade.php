@extends('layouts.public-2')
@section('css')
<style>
.modal-body{
    color: #333 !important; 
}
.modal-header{
    color: #333 !important; 

}
</style>   
@endsection
@section('content')
<div class="row text-center">
    <br>
    <br>
    <br>
    <h4><b>NUESTRO EQUIPO</b></h4>

    <br>


 </div> 			 

<div class="row row-0-gutter wow animated fadeIn text-center " data-wow-delay="0.6s">	


 <!-- start portfolio item -->
<div class="col-md-2 col-xs-4 col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/marcelo-quirici.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Marcelo Quirici</h2>
                <p>Presidente fundación Espiga</p>
                <a href="#" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->
                <!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter oportunidad">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/vero.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Veronica Pages</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>						
            </figcaption>
        </figure>
    </div>
</div>                
<!-- end portfolio item -->	
<!-- start portfolio item -->
<div class="col-md-2 col-xs-4 col-0-gutter proyecto">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/lucas.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Lucas Araya</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->
<!-- start portfolio item -->
<div class="col-md-2 col-xs-4 col-0-gutter caso ">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/sabrina.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Sabrina Bielat</h2>
                <p>Legales</p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
 
<!-- end portfolio item -->                
     

<!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/laura.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Laura Spinelli</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->

 <!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso ">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/cande.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Candelaria Vegth</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->         
</div>


<div class="row row-0-gutter wow animated fadeIn text-center " data-wow-delay="1.4s">	

  <!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/esteban.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Esteban Echeverria</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->
 
 
 <!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter oportunidad">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/tobias.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Tobias Quirici</h2>
                <p></p>
            <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->
<!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/simon.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Simon Quirici</h2>
                <p></p>
            <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->

<!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
                <img src="images/equipo/isolda.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Isolda Serafino</h2>
                <p>psicologa</p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>	
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->
    
<!-- start portfolio item -->
<div class="col-md-2 col-xs-4 col-0-gutter proyecto">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/andres.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Andres Tejerina</h2>
                <p>Programador</p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>
            </figcaption>
        </figure>
    </div>
</div>
<!-- end portfolio item -->

 <!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/andra-silva.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Andrea Silva</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>								
            </figcaption>
        </figure>
    </div>
</div>

<!-- end portfolio item -->	
 <!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/luciana-yara.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Luciana Yara</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>								
            </figcaption>
        </figure>
    </div>
</div>                
<!-- end portfolio item -->	
 
<!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/mariano-oberti.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Mariano Oberti</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>								
            </figcaption>
        </figure>
    </div>
</div>                
<!-- end portfolio item -->	
 
<!-- start portfolio item -->
<div class="col-md-2 col-xs-4  col-0-gutter caso">
    <div class="ot-portfolio-item">
        <figure class="effect-bubba">
            <img src="images/equipo/marida-dona.jpg" alt="img02" class="img-responsive" />
            <figcaption>
                <h2>Marida Dona</h2>
                <p></p>
                <a href="" data-toggle="modal" data-target="#">VER MAS</a>								
            </figcaption>
        </figure>
    </div>
</div>                
<!-- end portfolio item -->	
 
</div>			
<div id="fb-root"></div>

	<!-- Modal for portfolio item 1 -->
	<div class="modal fade" id="marceloquirici" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>MARCELO QUIRICI</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/marcelo-quirici.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/5491166701732" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto: marcelo@puentesocial.org" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>					
                    <br>
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 2 -->
<div class="modal fade" id="veronicapages" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>VERONICA PAGES</b></h4>
                </div>
                <div class="modal-body text-center">
                    
                    <img src="images/equipo/vero.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto: veronica@puentesocial.org" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>	
                    <br>
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 3 -->
<div class="modal fade" id="lucasaraya" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>LUCAS ARAYA</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/lucas.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/5491155765143" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto: marcelo@puentesocial.org" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>	
                    <br>
                </div>
                
    </div>
        </div>			
</div>


<!-- Modal for portfolio item 4 -->
<div class="modal fade" id="sabrinabielat" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>SABRINA BIELAT</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/sabrina.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>	
                    <br>
                </div>
                
            </div>
        </div>			
</div>


    <!-- Modal for portfolio item 5 -->
<div class="modal fade" id=laura tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>LAURA SPINELLI</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/laura.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                </div>
            </div>
        </div>			
</div>

        <!-- Modal for portfolio item 6 -->
<div class="modal fade" id="cande" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>CANDELARIA VEGH</b></h4>
                </div>
                <div class="modal-body text-center">
                    
                    <img src="images/equipo/cande.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                </div>
                
            </div>
        </div>			
</div>


<!-- Modal for portfolio item 8-->
<div class="modal fade" id="esteban" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>ESTEBAN ECHEVERRIA</b></h4>
                </div>
                <div class="modal-body text-center">
                    
                    <img src="images/equipo/esteban.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                </div>
                
            </div>
        </div>			
</div>


<!-- Modal for portfolio item  9-->
<div class="modal fade" id="tobias" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="Modal-label-1"><b>TOBIAS QUIRICI</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/tobias.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                </div>
                
    </div>
        </div>			
</div>

    <!-- Modal for portfolio item 10-->
<div class="modal fade" id="simon" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>SIMON QUIRICI</b></h4>
                </div>
                <div class="modal-body text-center">
                    
                    <img src="images/equipo/simon.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 11-->
<div class="modal fade" id="isolda" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>ISOLDA</b></h4>
                </div>
                <div class="modal-body text-center">						
                    
                    <img src="images/equipo/isolda.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 12-->
<div class="modal fade" id="andres" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>ANDRES TEJERINA</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/andres.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 7 -->
<div class="modal fade" id="andrea" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>ANDREA SILVA</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/andra-silva.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 13 -->
<div class="modal fade" id="luciana" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>LUCIANA YARA</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/luciana-yara.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 14 -->
<div class="modal fade" id="mariano" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>MARIANO OBERTI</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/mariano-oberti.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>

<!-- Modal for portfolio item 15 -->
<div class="modal fade" id="marida" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">						
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="Modal-label-1"><b>MARIDA DONA</b></h4>
                </div>
                <div class="modal-body text-center">
                    <img src="images/equipo/marida-dona.jpg" controls style="width:100%; height:auto">
                    <br>
                    <h3>Título</h3>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                    <h3><a href="https://wa.me/" target="_blanck"><span style="color: #4CAF50;"><i class="fa fa-whatsapp fa-2x"> </i></span></a>
                    <a href="mailto:" target="_blank"><span style="color: #4CAF50;"> <i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span></a></h3>
                    <br>
                    
                    
                </div>
                
            </div>
        </div>			
</div>  

@endsection
@section('scripts')

<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=2564677387125919&autoLogAppEvents=1"
    nonce="wsqECk3M"></script>

@endsection
