@extends('layouts.public-2')
@section('css')
<style>
{#aqui va código css#}
</style>   
@endsection
@section('content')
<div class="row text-center">
    <br>
    <br>
    <div class="section-title text-center">
							
							
							<h1><b>¡GRACIAS POR SUMARTE A PUENTE SOCIAL!</b></h1>                         
							
							<p>Por último te pedimos por favor que nos envies el comprobante o captura pantalla de la transacción, o simplemente completa la informacion ya sea por mail o whatsApp; indicandonos el número o nombre del caso / proyecto con el que estas colaborando, también necesitaremos tu nombre y DNI cuil o cuit según corresponda para poder enviarles el comprobante correspondiente.</p>
							
							<br>					
							
							
							<a href="https://wa.me/5491121646003&text=Hola%20Quisiera%20informar%20mi%20aporte%20para%20el%20Caso/proyecto%20Nº:...%20Mi%20Nombre:...%20DNI/cuit/cuil:..." target="_blanck"> <button type="button" class="btn btn-success"><b><i class="fa fa-whatsapp"></i> 11 2164 6003</b> </button></a>
							<br>							
							<br>
							<a href="mailto:info@puentesocial.org?subject=%20Quiero%20Informar%20mi%20aporte&body=Para%20el%20caso/proyecto%20Nº:...%20Mi%20Nombre:...%20DNI/cuit/cuil:..." target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-envelope-o"></i> info@puentesocial.org</button></a>
							<br>
							<br>
							<br>
							<p>Por cualquier duda ponete en contacto con nosotros, estamos a disposición.</p>
							<h2><b>"SOMOS LO QUE HACEMOS"</b></h2>
							<br>
							  <p><small>(*) Nuestra Fundación posee el Certificado de Exención vigente, en el marco de lo dispuesto en la Resolución General Nº2681 (AFIP), lo que determina la procedencia de la exención aludida - en los casos en que se autorice la deducción de donaciones (Art. 81 c) - Validará para el donante la deducción en el impuesto a las ganancias.</small></p>
						
						
	</div>


 </div> 			 

<div class="row row-0-gutter wow animated fadeIn text-center " data-wow-delay="0.6s">	


 
</div>			


@endsection
@section('scripts')
<script>
   //aqui va código js
</script>
@endsection
