@extends('layouts.login')

@section('content')
    

<div class="row">
		
    <br>		
    
        <div class="col-md-4 col-sm-4 col-lg-4">
    
        </div>
    
        <div class="col-md-4 col-sm-4 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">
                
            
            
                    <img src="images/iconos/logo-puente.svg"> 
                    <br>
                    <br>
                    <h4><b>SE PARTE DEL CAMBIO SOCIAL</b></h4>
                    <form method="POST" action="{{ route('login') }}">
                @csrf
                    <br>
            <p>¿Aún no registrarte tu empresa? <a href="{{url('/registro-empresas')}}"><span  style="color: #8ae58a;"> Registrala desde aquí</span></a></p>
            
                        <div class="input-field">
                            <input id="email" type="email" placeholder="*Email....." class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror                                        </div>
                                <br>
                                <input class="form-check-input" hidden type="checkbox" name="remember" id="remember" checked>

                        <div class="input-field">
                            <input id="password" placeholder="*Clave.." type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror             
                        </div>
                    <br>
                    <button type="submit" id="submit" class="btn btn-info">EMPRESA O ONG</button>
                    <br>
                </form>                                           

                @if (Route::has('password.request'))
                <br><p><a href="{{ route('password.request') }}">¿OLVIDASTE LA CLAVE?</a></p>
        
        @endif
                        <br>
                <br>
                <br>
                      <p><small>Al registrarte aceptas los <br><a href="{{url('terminos-y-politicas')}}"><span style="font-size: 9px"><u>terminos de uso y la politica de privacidad</u></span></a></small></p>
                  
                        <br>
                <br>
                <br>
                <br>
            
    
    </div>
    
    <div class="col-md-4 col-sm-4 col-lg-4">
    
    </div>
    <br>
             
            <br>	
   </div>	
    
    
@endsection
