@extends('layouts.login')

@section('content')
    
<div class="row">
		
	<br>		
	
		<div class="col-md-4 col-sm-4 col-lg-4">
	
		</div>
	
		<div class="col-md-4 col-sm-4 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">
				
			
			
					<img src="images/iconos/logo-puente.png"> 
					<br>
					<br>
					<h4><b>SE PARTE DEL CAMBIO SOCIAL</b></h4>
					   
					<br>
					<p>¿No tenes cuenta aún? <a href="#">Créala desde aquí</a></p>
			
						<div class="input-field">
							<input type="text" name="mail" class="form-control" placeholder="* Email..." required>
								</div>
								<br>
						<div class="input-field">
							<input type="mail" name="clave" class="form-control" placeholder="* Clave..." required>
						</div>
					<br>
					<button type="submit" id="submit" class="btn btn-success">INGRESAR COMO USUARIO</button>
					<br>
					
					
					<p><a href="#">¿OLVIDASTE LA CLAVE?</a></p>
					
					<p>Podes ingresar con</p>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
						
						
						<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 text-right">
						<button type="button" class="btn btn-secondary"><b> <i class="fa fa-facebook-official" aria-hidden="true"></i> facebook</b></button>
			
					
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 text-left">
						<button type="button" class="btn btn-secondary"><b> <i class="fa fa-google" aria-hidden="true"></i> gmail</b></button>
			
					
						</div>
						<br>
					<br>
					<br>
					<br>	
					
					</div>
					<br>
					<br>
					<p>Si sos una EMPRESA, FUNDACIÓN u ONG ingresa aquí</p>
					<a href="login-empresas.html"><button type="submit" id="submit" class="btn btn-info">EMPRESA, FUNDACIÓN O ONG</button></a>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
					  <p><small>Al registrarte aceptas los TERMINOS DE USO Y LA POLITICA DE PRIVACIDAD de PUENTE SOCIAL</small></p>
					<p><small><a href="#">VER LEGALES</a></small></p>
						<br>
				<br>
				<br>
				<br>
					
	
	</div>
	
	<div class="col-md-4 col-sm-4 col-lg-4">
	
	</div>
	<br>
			 
			<br>	
   </div>	

@endsection
