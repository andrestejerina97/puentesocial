@extends('layouts.login')

@section('content')
    
    <div class="row">
		
		<br>		
		
			<div class="col-md-4 col-sm-4 col-lg-4">
		
			</div>
		
			<div class="col-md-4 col-sm-4 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">
					
            <img src="{{asset('images/iconos/logo-puente-grande.png')}}"> 

				<br><br>
						<br>
						<br>
				<h4><b>INGRESA Y SE PROTAGONISTA <br>DEL CAMBIO SOCIAL</b></h4>
						<br>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
						<div class="input-field">
                                <input id="email" type="email" placeholder="*Email....." class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
									</div>
									<br>
						  <div class="input-field">
                                <input id="password" placeholder="*Password.." type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                      
                        <br>
                        <div class="input-field">
                            <button type="submit" id="submit" class="btn btn-success">INGRESAR</button>

                            @if (Route::has('password.request'))
                                <a class="" href="{{ route('password.request') }}">
                                    <h5>OLVIDASTE LA CLAVE?</h5>
                                </a>
                            @endif
                        </div>
						<br>
						</form>

						<br>
						<br>
				<br>
						

						
				
				
		
		</div>
		
		<div class="col-md-4 col-sm-4 col-lg-4">
		
		</div>
		<br>
                 
				<br>	
       </div>	

@endsection
