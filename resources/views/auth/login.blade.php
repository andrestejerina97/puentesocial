@extends('layouts.login')

@section('content')
    
<div class="row">
		
    <br>		
    
        <div class="col-md-4 col-sm-4 col-lg-4">
    
        </div>
    
        <div class="col-md-4 col-sm-4 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">
                
            
            
                    <img src="images/iconos/logo-puente.svg"> 
                    <br>
                    <br>
                    
            <h4><b>SE PARTE DEL CAMBIO SOCIAL</b></h4>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                    <br>
                    <p>¿No tenes cuenta? <a href="{{route('register')}}">Créala aquí</a></p>
            
                        <div class="input-field">
                            <input id="email" type="email" placeholder="*Email....." class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror                                
                        </div>
                        <input class="form-check-input" hidden type="checkbox" name="remember" id="remember" checked>

                                <br>
                        <div class="input-field">
                            <input id="password" placeholder="*Clave.." type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror             
                                   </div>
                    <br>
                    <div class="input-field">
                        <button type="submit" id="submit" class="btn btn-success">ENTRAR COMO USUARIO</button>

                    </div>
                    <br>
                </form>
                    
                @if (Route::has('password.request'))
                                    <p><a href="{{ route('password.request') }}">¿OLVIDASTE LA CLAVE?</a></p>
                            
                            @endif
                    
                    <p>Entrar con</p>
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                        
                        
                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 text-right">
                        <a href="{{route('social.auth','facebook')}}" class="btn btn-secondary"><b> <i class="fa fa-facebook-official" aria-hidden="true"></i> facebook</b></a>
            
                    
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 text-left">
                        <a href="{{route('social.auth','google')}}" class="btn btn-secondary"><b> <i class="fa fa-google" aria-hidden="true"></i> gmail</b></a>
            
                    
                        </div>
                        <br>
                    <br>
                    <br>
                    <br>	
                    
                    </div>
                    <br>
                    <br>
                    <p>Si sos una EMPRESA o ONG ingresa aquí</p>
                <a href="{{url('/login-empresas')}}"><button type="submit" id="submit" class="btn btn-info">EMPRESAS O ONG</button></a>
                <br>
                <br>
                      <p><small>Al registrarte aceptas los <br><a href="{{url('terminos-y-politicas')}}"><span style="font-size: 9px"><u>terminos de uso y la politica de privacidad</u></span></a></small></p>
                        <br>
                <br>
                <br>
                <br>
                    
    
    </div>
    
    <div class="col-md-4 col-sm-4 col-lg-4">
    
    </div>
    <br>
             
            <br>	
   </div>	
    
@endsection
