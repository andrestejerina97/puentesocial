@extends('layouts.public-2')

@section('content')

<div class="row">
		
    <br>		

    <div class="col-md-2 col-sm-2">
    
    
    </div>
    
    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 wow animated bounceInLeft box2 text-center" data-wow-delay="0.6s">			
            
            <br>
        <br>
                <h3><b>Formulario de registro de Empresas, Fundaciones o ONG</b></h3>
                <p>Todos podemos ser parte porque todos tenemos algo para compartir. Si sentís nuestros mismos anhelos de cambiar la realidad de muchas personas y querés brindar tu aporte para construir una sociedad más justa, te invitamos a unirte a Puente Social.</p>
                <br>
        
                            <p><small>(*) Campos requeridos</small></p>
                      <form name="form_register"  action="{{route('register')}}" method="post">
                                @csrf
                                <br>
                                <p><label name="sel1">Seleccione tipo de entidad a registrar:</label>
                                    
                                  <select type="text" class="form-control" name="role" required>
                                    <option >Seleccione una opción...</option>
                                    <option value="business">EMPRESA</option>
                                    <option value="business">FUNDACIÓN SIN FINES DE LUCRO</option>										
                                    <option value="business">ONG</option>
                                    <option value="business">OTRA</option>									
                                    
                                  </select>
                                    <br>
                                <div class="row">
                                <div class="input-field col-lg-6 col-md-6">
                                    <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('email') is-invalid @enderror" placeholder="*Nombre de la entidad..." required autocomplete="name">
                                </div>
                                <div class="input-field col-lg-6 col-md-6">
                                    <input id="email"  placeholder="* Email corporativo..."  type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                              </div>										
                              <input type="hidden" name="role" value="business">

                                <div class="row text-center">
                                <br>
                                
                                    
                                    <div class="input-field col-lg-6 col-md-6">
                                    <input type="tel" name="cel" class="form-control @error('cellphone') is-invalid @enderror" value="{{ old('cellphone') }}" placeholder="Teléfono">
                                    @error('cel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>	
                                    
                                    <div class="input-field col-lg-6 col-md-6">
                                    <input type="email" name="email_alternative" class="form-control @error('email_alternative') is-invalid @enderror" value="{{ old('email_alternative') }}" placeholder="Email alternativo">
                                    @error('email_alternative')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>	
                                </div>
                                <br>
                                <div class="row text-center">
                                    <div class="input-field col-lg-6 col-md-6">
                                        <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                    <div class="input-field col-lg-6 col-md-6">
                                        <input id="password-confirm" placeholder="Confirmación de password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">            
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                 
                                </div>
                               
                                                                                            
                                <br>
                                <button type="submit"  id="submit" class="btn btn-lg btn-success">REGISTRARSE</button>
                                <br>
                                <br>
                            </form>
                
                
                
        
    </div>
    
    <div class="col-md-2 col-sm-2">
    
    
    </div>
    <br>               
    <br>	
</div>	

<div class="row">
    <hr>
    <div class="col-md-2 col-sm-2">
    
    </div>
    
    <div class="col-md-4 col-sm-4 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
        
        <br>
                <a href="https://wa.me/541130718986" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-whatsapp"></i> WHATSAPP 11 6670 1732   </button></a>
                <br>
    
    
    
    </div>
    
    <div class="col-md-4 col-sm-4 text-center wow animated bounceInLeft box2" data-wow-delay="0.9s">
        <br>
        <a href="mailto:info@puentesocial.org" target="_blanck"> <button type="button" class="btn btn-success"><i class="fa fa-envelope-o"></i> info@puentesocial.org  </button></a>
            <br>
        
    </div>
    
    <div class="col-md-2 col-sm-2">
    
    </div>
    
    <div class="col-md-12 col-sm-12 text-center">
        <br>
        <p><small>FUNDACIÓN ESPIGA es una Entidad de Bien Público sin fines de lucro, con Personería Jurídica obtenida por Resolución de la Inspección General de Justicia Nº 000986, con fecha 22 de Septiembre de 1998. Inscripta en la Administración Federal de Ingresos Públicos (AFIP); en el REPOC, Registro de Organizaciones Comunitarias de la Provincia de Buenos Aires; en el R.U.O., Registro Único de Organizaciones de la Sociedad Civil de la Provincia de Buenos Aires y en el Registro de Entidades de Bien Público de la Municipalidad de Vicente López.</small></p>
        <br>
            <p><small><a href="legales.html">VER LEGALES</a></small></p>
    </div>

</div>

@endsection
