<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicationReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publication_reports', function (Blueprint $table) {
            $table->id();
            $table->string('class')->nullable();
            $table->string('icon')->nullable();
            $table->unsignedBigInteger('publications_id')->nullable();
            $table->foreign('publications_id')->references('id')->on('publications')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publication_reports');
    }
}
