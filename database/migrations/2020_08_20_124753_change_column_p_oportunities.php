<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnPOportunities extends Migration
{

    //cambio de nombre de la tabla opotunities_images
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oportunity_images', function (Blueprint $table) {
           
            $table->unsignedBigInteger('publications_id')->nullable();
            $table->foreign('publications_id')->references('id')->on('publications')->onDelete('cascade')->onUpdate('cascade');


          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
