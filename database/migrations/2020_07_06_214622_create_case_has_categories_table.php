<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseHasCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_has_categories', function (Blueprint $table) {
        $table->unsignedBigInteger('publications_id')->nullable();
        $table->unsignedBigInteger('case_categories_id')->nullable();
        $table->foreign('publications_id')->references('id')->on('publications')->onDelete('cascade')->onUpdate('cascade'); 
        $table->foreign('case_categories_id')->references('id')->on('case_categories')->onDelete('cascade')->onUpdate('cascade'); 
  
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_has_categories');
    }
}
