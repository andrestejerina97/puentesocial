<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOportunityHasCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oportunity_has_categories', function (Blueprint $table) {
            $table->id();
 

            $table->unsignedBigInteger('publications_id')->nullable();
            $table->unsignedBigInteger('oportunity_categories_id')->nullable();
            $table->foreign('publications_id')->references('id')->on('publications')->onDelete('cascade')->onUpdate('cascade'); 
            $table->foreign('oportunity_categories_id')->references('id')->on('oportunity_categories')->onDelete('cascade')->onUpdate('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oportunity_has_categories');
    }
}
