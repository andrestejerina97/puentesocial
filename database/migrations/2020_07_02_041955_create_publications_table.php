<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
                $table->id();
                $table->string('name')->nullable();
                $table->bigInteger('likes')->nullable();
                $table->bigInteger('shared')->nullable();
                $table->text('details')->nullable();
                $table->text('report')->nullable();
                $table->text('objetive')->nullable();
                $table->string('progress')->nullable();
                $table->string('duration')->nullable();
                $table->string('email')->nullable();
                $table->string('cellphone')->nullable();
                $table->string('code')->nullable();
                $table->text('opening_sentence')->nullable();
                $table->string('avatar')->nullable();
                $table->boolean('finalized')->nullable();
                $table->text('text_facebook')->nullable();
                $table->text('text_twitter')->nullable();
                $table->string('diagnosis')->nullable();
                $table->string('login_name')->nullable();
                $table->string('dni')->nullable();
                $table->string('home')->nullable();
                $table->string('clinical_history')->nullable();
                $table->string('contact_phone')->nullable();
                $table->string('medical_contact')->nullable();
                $table->string('family_contact')->nullable();
                $table->text('others')->nullable();
                $table->integer('visibled_for_individual')->nullable()->default(0);
                $table->integer('visibled_for_bussiness')->nullable()->default(0);

                $table->integer('active')->nullable();
                $table->string('city_province')->nullable();
                $table->string('initial_details')->nullable();
                $table->text('help_plan')->nullable();
                $table->text('agreement')->nullable();
                $table->text('name_case')->nullable();
                $table->text('link_paypal')->nullable();
                $table->text('link_mercado_pago')->nullable();
                $table->unsignedBigInteger('users_id')->nullable();
                $table->unsignedBigInteger('modified_users_id')->nullable();
                $table->unsignedBigInteger('categories_id')->nullable();
                $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('categories_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('modified_users_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
