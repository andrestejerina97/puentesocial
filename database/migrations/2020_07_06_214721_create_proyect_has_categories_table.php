<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectHasCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyect_has_categories', function (Blueprint $table) {
            $table->unsignedBigInteger('publications_id')->nullable();
            $table->unsignedBigInteger('proyect_categories_id')->nullable();
        $table->foreign('publications_id')->references('id')->on('publications')->onDelete('cascade')->onUpdate('cascade'); 
        $table->foreign('proyect_categories_id')->references('id')->on('proyect_categories')->onDelete('cascade')->onUpdate('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyect_has_categories');
    }
}
