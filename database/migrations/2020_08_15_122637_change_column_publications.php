<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnPublications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publications', function (Blueprint $table) {
            $table->renameColumn('diagnosis', 'web');
            $table->string('web_contact')->nullable();
            $table->string('city_province_contact')->nullable();           
            $table->string('email_contact')->nullable();          
            $table->string('cellphone_contact')->nullable();          
            $table->text('link_podcast')->nullable();
            $table->text('link_donar_online')->nullable();

                  
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publications', function (Blueprint $table) {
            $table->dropColumn('web_contact');
            $table->dropColumn('city_province_contact');
            $table->dropColumn('email_contact');
            $table->dropColumn('cellphone_contact');
            $table->dropColumn('link_podcast');
            $table->dropColumn('link_donar_online');

          });
    }
}
