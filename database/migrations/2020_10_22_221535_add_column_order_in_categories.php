<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOrderInCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            Schema::table('case_categories', function (Blueprint $table) {
                $table->integer('order')->nullable();
                $table->softDeletes();
            });
            Schema::table('proyect_categories', function (Blueprint $table) {

                $table->integer('order')->nullable();
                $table->softDeletes();

            });
            Schema::table('oportunity_categories', function (Blueprint $table) {

                $table->integer('order')->nullable();
                $table->softDeletes();

            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_categories', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('proyect_categories', function (Blueprint $table) {

            $table->dropColumn('order');
        });
        Schema::table('oportunity_categories', function (Blueprint $table) {

            $table->dropColumn('order');
        });
    }
}
