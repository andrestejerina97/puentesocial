<?php

use App\oportunity_categories;
use App\proyect_categories;
use Illuminate\Database\Seeder;

class CategoriesOportunitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         
        oportunity_categories::insert([
            'name'=>"Donación",
            'icon'=>"fa fa-heart",

        ]);
        oportunity_categories::insert([
            'name'=>"Empleo",
            'icon'=>"fa fa-hand-paper-o",

        ]);
        oportunity_categories::insert([
            'name'=>"Trabajo temporal",
            'icon'=>"fa fa-thumb-tack",

        ]);
        oportunity_categories::insert([
            'name'=>"Ropa /Calzado",
            'icon'=>"fa fa-umbrella",

        ]);
        oportunity_categories::insert([
            'name'=>"Alimentos",
            'icon'=>"fa fa-shopping-bascket",

        ]);
        oportunity_categories::insert([
            'name'=>"Habitad",
            'icon'=>"fa fa-bed",

        ]);
        oportunity_categories::insert([
            'name'=>"Materiales Construcción",
            'icon'=>"fa fa-building",

        ]);
        oportunity_categories::insert([
            'name'=>"Bicicleta",
            'icon'=>"fa fa-bicycle",

        ]);
       
    }
}
