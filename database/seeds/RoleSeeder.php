<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            Role::create(['name' => 'edit']);
            Role::create(['name' => 'business']);
            Role::create(['name' => 'admin']);
            Role::create(['name' => 'user']);
        

            $user=User::create([
                'name' => "Marcelo",
                'email' => "admin@puentesocial.org",
                'password' => Hash::make("PasajeU1944"),
            ]);
            $user->assignRole("admin");
        $user=User::create([
            'name' => "administrador",
            'email' => "administrador@gmail.com",
            'password' => Hash::make("administrador1234"),
        ]);
        $user->assignRole("admin");
        $user=User::create([
            'name' => "user",
            'email' => "user@gmail.com",
            'password' => Hash::make("user1234"),
        ]);
        $user->assignRole("user");
        $user=User::create([
            'name' => "editor",
            'email' => "editor@gmail.com",
            'password' => Hash::make("editor1234"),
        ]);
        $user->assignRole("edit");

    }
}
