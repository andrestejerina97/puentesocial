<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OportunitiesSeeder::class);
         $this->call(CategoriesSeeder::class);

         $this->call(CategoriesCasesSeeder::class);
        $this->call(CategoriesProyectSeeder::class);
         $this->call(CategoriesOportunitySeeder::class);
         $this->call(RoleSeeder::class);

    }
}
