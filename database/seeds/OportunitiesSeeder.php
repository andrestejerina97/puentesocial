<?php

use App\oportunity_types;
use Illuminate\Database\Seeder;

class OportunitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oportunity= new oportunity_types();
        $oportunity->name="Empleo...";
        $oportunity->save();
        $oportunity= new oportunity_types();
        $oportunity->name="Materiales...";
        $oportunity->save();
        $oportunity= new oportunity_types();
        $oportunity->name="Donaciones...";
        $oportunity->save();
        $oportunity= new oportunity_types();
        $oportunity->name="Otros...";
        $oportunity->save();



    }
}
