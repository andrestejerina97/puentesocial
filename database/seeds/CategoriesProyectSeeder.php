<?php

use App\oportunity_categories;
use App\proyect_categories;
use Illuminate\Database\Seeder;

class CategoriesProyectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        proyect_categories::insert([
            'name'=>"Solidario",
            'icon'=>"fa fa-user",

        ]);
        proyect_categories::insert([
            'name'=>"Comunitario",
            'icon'=>"fa fa-cutlery",

        ]);
        proyect_categories::insert([
            'name'=>"Comedores | Merenderos",
            'icon'=>"fa fa-recycle",

        ]);
        proyect_categories::insert([
            'name'=>"Medio Ambiente",
            'icon'=>"fa fa-lightbulb-o",

        ]);
        proyect_categories::insert([
            'name'=>"Innovación",
            'icon'=>"fa fa-trunk",

        ]);
        proyect_categories::insert([
            'name'=>"Voluntariado",
            'icon'=>"fa fa-wrench",

        ]);
        proyect_categories::insert([
            'name'=>"Capacitación",
            'icon'=>"fa fa-briefcase",

        ]);
        proyect_categories::insert([
            'name'=>"Cultura",
            'icon'=>"fa fa-university",

        ]);
        proyect_categories::insert([
            'name'=>"Investigación",
            'icon'=>"fa fa-flask",

        ]);
    }
}
