<?php

use App\case_categories;
use Illuminate\Database\Seeder;

class CategoriesCasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        case_categories::insert([
            'name'=>"Individuo",
            'icon'=>"fa fa-chil",

        ]);
        case_categories::insert([
            'name'=>"Familia",
            'icon'=>"fa fa-home",

        ]);
        case_categories::insert([
            'name'=>"Comunidad",
            'icon'=>"fa fa-users",

        ]);
        case_categories::insert([
            'name'=>"Animales",
            'icon'=>"fa fa-paw",

        ]);
        case_categories::insert([
            'name'=>"Salud",
            'icon'=>"fa fa-user-md",

        ]);
        case_categories::insert([
            'name'=>"Trabajo",
            'icon'=>"fa fa-wrench",

        ]);
        case_categories::insert([
            'name'=>"Cultura",
            'icon'=>"fa fa-paint-brush",

        ]);
        case_categories::insert([
            'name'=>"Eduación",
            'icon'=>"fa fa-book",

        ]);
        case_categories::insert([
            'name'=>"Deportes",
            'icon'=>"fa fa-futbol-o",

        ]);
        case_categories::insert([
            'name'=>"Legal",
            'icon'=>"fa fa-gavel",

        ]);
        case_categories::insert([
            'name'=>"Discapacidad",
            'icon'=>"fa fa-weelchair",

        ]);
    }
}
