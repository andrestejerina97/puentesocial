<?php

use App\categories;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        categories::insert(['name'=>"CASOS PARA AYUDAR",
        "icon"=>"fa fa-user-circle fa-2x",
        "class"=>"casos"]);
        categories::insert(['name'=>"PROYECTOS COMUNITARIOS",
        "icon"=>"fa fa-globe fa-2x",
        "class"=>"proyectos"]);
        categories::insert(['name'=>"DONACIONES Y OPORTUNIDAD",
        "icon"=>"fa fa-bullhorn fa-2x",
        "class"=>"oportunidades"]);
        categories::insert(['name'=>"PUENTES REALIZADOS",
        "icon"=>"fa fa-heart-o fa-2x",
        "class"=>"puentes"]);
 
    }
}
