<?php

function isAdmin() {
	if (\Auth::user()->hasRole('admin')) {
		return true;
	} else {
		return false;
	}


}
function isEdit() {
	if (\Auth::user()->hasRole('edit')) {
		return true;
	} else {
		return false;
	}

}
function isBusiness() {
	if (\Auth::user()->hasRole('business')) {
		return true;
	} else {
		return false;
	}

}
if (! function_exists('current_user')) {
    function current_user()
    {
        return auth()->user();
    }
}
?>