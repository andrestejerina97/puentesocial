<?php

namespace App\Http\Controllers;

use App\proyect_images;
use App\proyects;
use Illuminate\Http\Request;

class ProyectsController extends Controller
{
   
    public function save(Request $request)
    {
        
        $proyects= new proyects();
   
       // $currentUser = Auth::user();
        $proyects->name= $request->input('name');
        $proyects->email=$request->input('email');
        $proyects->cellphone=$request->input('cellphone');
        $proyects->city_province= $request->input('city_province');
        $proyects->initial_details=$request->input('initial_details');
        $proyects->details=$request->input('initital_details');
        $proyects->name_proyect=$request->input('name_proyect');
        $proyects->name_organization=$request->input('name_organization');

        $proyects->save();
        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            $allowedfileExtension=['jpg','png','jpeg'];
            $contador=1;
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if($check){
                    $report_file= new proyect_images();
                    $report_file->proyects_id=$proyects->id;
                    $report_file->photo="foto"."-".$contador.'.'.$extension;
                    $path = $file->storeAs('proyects/'.$proyects->id,$report_file->photo);
                    $report_file->save();
                    $contador++;
                }
            }
              
            }
                  

          //   Storage::disk('public')->put($proyects->avatar,  \File::get($file));

       //indicamos que queremos guardar un nuevo archivo en el disco local
       //Storage::disk('local')->put($nombre,  \File::get($file));
       
        return redirect('/ingreso-exitoso')
        ->with('categoria_publicacion','PROYECTO');
    }
}
