<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\oportunities;
use App\oportunity_images;
use Illuminate\Http\Request;

class OportunitiesController extends Controller
{
    public function index()
    {
        
        $oportunities= oportunities::all();
      return view('admin.publicaciones.oportunity',compact('oportunities'));
    }
    public function edit($id)
    {
      $oportunities= oportunities::where('id','=',$id)->get();
      $photos= oportunity_images::where('oportunities_id','=',$id)->get();

      return view('admin.publicaciones.edit-oportunity',compact('oportunities','photos'));   
     }
    public function update()
    {
      # code...
    }
    public function save(Request $request)
    {
        
        $oportunities= new oportunities();
       // $currentUser = Auth::user();
        $oportunities->name= $request->input('name');
        $oportunities->email=$request->input('email');
        $oportunities->cellphone=$request->input('cellphone');
        $oportunities->city_province= $request->input('city_province');
        $oportunities->initial_details=$request->input('initial_details');
        $oportunities->details=$request->input('initital_details');
        $oportunities->oportunity_types_id=$request->input('oportunity_types_id');

        $oportunities->save();
        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            $allowedfileExtension=['jpg','png','jpeg'];
            $contador=1;
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if($check){
                    $report_file= new oportunity_images();
                    $report_file->oportunities_id=$oportunities->id;
                    $report_file->photo="foto"."-".$contador.'.'.$extension;
                    $path = $file->storeAs('oportunities/'.$oportunities->id,$report_file->photo);
                    $report_file->save();
                    $contador++;
                }
            }
              
            }
                  

          //   Storage::disk('public')->put($oportunities->avatar,  \File::get($file));

       //indicamos que queremos guardar un nuevo archivo en el disco local
       //Storage::disk('local')->put($nombre,  \File::get($file));
       
        return redirect('/ingreso-exitoso')
        ->with('categoria_publicacion','OPORTUNIDAD');
    }
}
