<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravelista\Comments\Comment;

class CommentsController extends Controller
{
    public function addLike(Request $request)
         {
             $id=$request->comment;
             $comment=Comment::find($id);
             if($comment->liked()){
                $comment->unlike();
             }else{
                $comment->like();
             }
        
             return response()->json(['result'=> $comment->likeCount]);
         }
}
