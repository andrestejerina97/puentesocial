<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'photo' => 'file|max:3200',
          ],
          ['name.required' => 'El nombre no puede ser nulo',
          'photo.max' => 'Archivo demasiado grande! 3mb(max)',
          ]
  
        );
      $user=User::where('id',Auth::user()->id)->firstOrFail();
    
      $user->name=$request->name;
      $user->home=$request->home;
      $user->birthdate=$request->birthdate;
      $user->age=$request->age;
      $user->save();
      if ($request->hasFile('photo')) {

        $file = $request->file('photo');
        $allowedfileExtension=['jpg','png','jpeg','PNG'];
         $extension = $file->getClientOriginalExtension();
         $check=in_array($extension,$allowedfileExtension);
          if($check){
              $user->photo="foto"."-".$user->id.'.'.$extension;
              $user->save();
              $path = $file->storeAs('users/'.$user->id,$user->photo);
            }else{
                session()->flash('errorProfile', "Formato incorrecto de foto!");
            }
          }
      session()->flash('successProfile', "Actualización exitosa!");

      return redirect('mi-perfil');

    }
}
