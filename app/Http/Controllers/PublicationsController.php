<?php

namespace App\Http\Controllers;

use App\publication_images;
use App\publications;
use Illuminate\Http\Request;
use Alert;
use App\case_categories;
use App\case_has_categories;
use App\donors;
use App\oportunity_categories;
use App\oportunity_has_categories;
use App\oportunity_types;
use App\proyect_categories;
use App\proyect_has_categories;
use Laravel\Ui\Presets\React;

class PublicationsController extends Controller
{
    public function index()
    {
        $publications= publications::all();
        $case_categories=case_categories::orderBy('order','asc')->get();
        $proyect_categories=proyect_categories::orderBy('order','asc')->get();
        $oportunity_categories=oportunity_categories::orderBy('order','asc')->get();
        $case_has_categories=case_has_categories::where('publications_id',$id)->select('case_categories_id')->get();
        $proyect_has_categories=proyect_has_categories::where('publications_id',$id)->select('proyect_categories_id')->get();
        $oportunity_has_categories=oportunity_has_categories::where('publications_id',$id)->select('oportunity_categories_id')->get();
        
        
      return view('publications.index',compact('publications','donors',
      'photos',
      'case_categories',
      'proyect_categories',
      'oportunity_categories',
      'case_has_categories',
      'proyect_has_categories',
      'oportunity_has_categories'))
      ->with('totalDonors',$totalDonors);
       // return view('publicaciones.index',compact('publications'));  
    }
    public function case()
    {
        $categories= case_categories::orderBy('order','asc')->get();
        return view('publicaciones.create-case',compact('categories'));      
    }
    public function proyect()
    {
        $categories= case_categories::orderBy('order','asc')->get();
        return view('publicaciones.create-proyect',compact('categories'));      
    }
    public function oportunity()
    {
        $oportunity_types= oportunity_types::all();
        return view('publicaciones.create-oportunity',compact('oportunity_types'));      
    }
    public function update()
    {
        # code...
    }
    public function save(Request $request)
    {
        $publication= new publications();
   
       // $currentUser = Auth::user();
        $publication->name= $request->input('name');
        $publication->email=$request->input('email');
        $publication->cellphone=$request->input('cellphone');
        $publication->city_province= $request->input('city_province');
        $publication->initial_details=$request->input('initial_details');
        $publication->details=$request->input('initital_details');

        $publication->save();
        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            $allowedfileExtension=['jpg','png','jpeg'];
            $contador=1;
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if($check){
                    $report_file= new publication_images();
                    $report_file->publications_id=$publication->id;
                    $report_file->photo="foto"."-".$contador.'.'.$extension;
                    $path = $file->storeAs('publicacion/'.$publication->id,$report_file->photo);
                    $report_file->save();
                    $contador++;
                }
            }
              
            }
                  

          //   Storage::disk('public')->put($publication->avatar,  \File::get($file));

       //indicamos que queremos guardar un nuevo archivo en el disco local
       //Storage::disk('local')->put($nombre,  \File::get($file));
       
        return redirect('/ingreso-exitoso')
        ->with('categoria_publicacion','Caso');
    }
    public function delete(Type $var = null)
    {
        # code...
    }
    public function view($id=0)
    {
        if($id==0){
       
          //  $case_categories= case_categories::all();
           // return view('admin.publicaciones.edit-case',compact('case_categories'));   
          }else{
            $donors=donors::with('users')
            //join('users','users.id','donors.users_id')
            ->where('donors.publications_id','=',$id)
            //->select('users.id','users.photo')
            ->inRandomOrder()->get();
            $totalDonors=$donors->count();
            $donors=$donors->take(15);
            $case_categories=case_has_categories::join('case_categories','case_categories.id','case_has_categories.case_categories_id')
                ->where('publications_id','=',$id)->get();

            
            $photos= publication_images::where('publications_id','=',$id)->get();
            $publications= publications::with('likes')->with(['comments.children'=> function ($query) {
                $query->orderBy('comments.created_at','desc');
            }])->where('id','=',$id)->get();

           
          return view('publications.view',compact('publications','donors','photos','case_categories'))
          ->with('totalDonors',$totalDonors);   
          }   
         }
         public function addLike(Request $request)
         {
             $id=$request->publication;
             $publication=publications::find($id);
             if($publication->liked()){
                $publication->likes=$publication->likes-1;
                $publication->save();
                $publication->unlike();
             }else{
                $publication->likes=$publication->likes+1;
                $publication->save();
                $publication->like();
             }
        
             return response()->json(['result'=> $publication->likes]);
         }
         public function addSharing(Request $request)
         {
            $id=$request->publication;
            $publication=publications::find($id);
               $publication->shared=$publication->shared+1;
               $publication->save();
           
       
            return response()->json(['result'=> $publication->shared]);
         }

 
}
