<?php

namespace App\Http\Controllers;

use App\case_categories;
use App\case_has_categories;
use App\categories;
use App\Notifications\DonacionMaterialesNotifications;
use App\Notifications\FormRequestNotification;
use App\publications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\oportunity_categories;
use App\oportunity_has_categories;
use App\oportunity_types;
use App\proyect_categories;
use App\proyect_has_categories;
use App\User;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     //   $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       
        if(Auth::check()){
            if(isAdmin() && Auth::user()->id !=2){ // desarrollador
                return redirect()->route('admin');
            }
        }
    
        $categories= categories::all();
        $publications= publications::with(['subcases','subproyects','suboportunities'])->with('category')->where('active',1)->inRandomOrder()->get();
        $case_categories=case_categories::orderBy('order','asc')->get();
        $proyect_categories=proyect_categories::orderBy('order','asc')->get();
        $oportunity_categories=oportunity_categories::orderBy('order','asc')->get();
        //  $case_has_categories=case_has_categories::->select('case_categories_id')->get();
        // $proyect_has_categories=proyect_has_categories::where('publications_id',$id)->select('proyect_categories_id')->get();
       // $oportunity_has_categories=oportunity_has_categories::where('publications_id',$id)->select('oportunity_categories_id')->get();
       if($request->has('puente')){
        $puente= publications::where('code',trim($request->puente))->first();
        if($puente != null){
            $request->session()->flash('slugPuente', $puente->id);
            return redirect('/home');
        }
       }
       if(Cookie::get('firstLogin')==1){
        Session::put('firstLogin',0); //poner a 1 para ver el modal de bienvenida

        return response()
        ->view('welcome',compact('publications','categories',
        'case_categories',
        'proyect_categories',
        'oportunity_categories'));
        }else{
            Session::put('firstLogin',1);

        $minutes = 60 * 12 * 365 * 10;
        return response()
        ->view('welcome',compact('publications','categories',
        'case_categories',
        'proyect_categories',
        'oportunity_categories'))->cookie(
          'firstLogin', '1',$minutes);
         }
        

        

        //return view('welcome',compact('categories','publications'));
    }
    public function previewPublication($name,$id,$activate,Request $request)
    {
        if($request->has('puente')){
            $publication= publications::with('category')->where('id',$id)->where('name_case',$name)->first();
            if($publication != null){
                return view('publications.previewBridge',compact('publication'));
            }
        }
        $publication=publications::with('category')->where('id',$id)->where('name_case',$name)->where('active',1)->first();
        if (!empty($publication)>0) {
            return view('publications.preview',compact('publication'));
        }else{
            Session::flash('error','El link no está disponible actualmente,fuiste redirigido a la página principal');
            return redirect()->route('home');
        }

    }
  
    public function donarMateriales(Request $request)
    {
        $users=User::where('email','info@puentesocial.org')->get();
        $data=$request->except('_token','photos');
        $attach=array();
        $body['title']="Nuevo envío de Donaciones";
        $body['subject']="Formulario de donaciones";

        $body['data']['direccion']=$data['direccion'];
        $body['data']['mail']=$data['mail'];
        $body['data']['telefono']=$data['telefono'];
        $body['data']['provincia']=$data['provincia'];
        $body['data']['ciudad']=$data['ciudad'];
        $body['data']['mensaje']=$data['texarea'];

        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            //$allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
                $nameFile=date('Y-m-d-H-m-s').".".trim($file->getClientOriginalExtension());
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $path = $file->storeAs('formsRequest/donaciones',$nameFile);
                  $attach[]=env('APP_URL')."/storage"."/". $path;
                //}
            }
          }

         // dd($attach);
         // $attach=array();

        if(isset($users)){
            foreach ($users as $user ){
                $user->notify(new FormRequestNotification($body,$attach));
            }
        }
        session()->flash('successMail', "La solicitud fue enviada con éxito!");
        return view('donacion-materiales');
    }
    public function enviarCaso(Request $request)
    {
        $users=User::where('email','casos@puentesocial.org')->get();
        $data=$request->except('_token','photos');
        $attach=array();
        $body['title']="Nuevo envío de Puente para construir";
        $body['subject']="Puentes para construir";

        $body['data']['nombre']=$data['nombre'];
        $body['data']['mail']=$data['mail'];
        $body['data']['telefono']=$data['telefono'];
        $body['data']['ciudad']=$data['ciudad'];
        $body['data']['mensaje']=$data['texarea'];

        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            //$allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
                $nameFile=date('Y-m-d-H-m-s').".".trim($file->getClientOriginalExtension());
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $path = $file->storeAs('formsRequest/casos',$nameFile);
                  $attach[]=env('APP_URL')."/storage"."/". $path;
                //}
            }
          }

         // dd($attach);
         // $attach=array();

        if(isset($users)){
            foreach ($users as $user ){
                $user->notify(new FormRequestNotification($body,$attach,"lucas@puentesocial.org"));
            }
        }
        session()->flash('successMail', "La solicitud fue enviada con éxito!");
        return view('enviar-caso');
    }
    public function enviarOportunidad(Request $request)
    {
        $users=User::where('email','info@puentesocial.org')->get();
        $data=$request->except('_token','photos');
        $attach=array();
        $body['title']="Nuevo envío de Oportunidad/Donación";
        $body['subject']="Oportunidad / Donación";

        $body['data']['nombre']=$data['nombre'];
        $body['data']['mail']=$data['mail'];
        $body['data']['telefono']=$data['telefono'];
        $body['data']['ciudad']=$data['ciudad'];
        $body['data']['mensaje']=$data['texarea'];

        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            //$allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
                $nameFile=date('Y-m-d-H-m-s').".".trim($file->getClientOriginalExtension());
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $path = $file->storeAs('formsRequest/oportunidad',$nameFile);
                  $attach[]=env('APP_URL')."/storage"."/". $path;
                //}
            }
          }

         // dd($attach);
         // $attach=array();

        if(isset($users)){
            foreach ($users as $user ){
                $user->notify(new FormRequestNotification($body,$attach));
            }
        }
        session()->flash('successMail', "La solicitud fue enviada con éxito!");
        return view('enviar-oportunidad');
    }
    public function enviarOportunidadLaboral(Request $request)
    {
        $users=User::where('email','info@puentesocial.org')->get();
        $data=$request->except('_token','photos');
        $attach=array();
        $body['title']="Nuevo envío de Oportunidad Laboral";
        $body['subject']="Oportunidad Laboral";

        $body['data']['nombre']=$data['nombre'];
        $body['data']['mail']=$data['mail'];
        $body['data']['telefono']=$data['telefono'];
        $body['data']['ciudad']=$data['ciudad'];
        $body['data']['mensaje']=$data['texarea'];

        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            //$allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
                $nameFile=date('Y-m-d-H-m-s').".".trim($file->getClientOriginalExtension());
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $path = $file->storeAs('formsRequest/oportunidadLab',$nameFile);
                  $attach[]=env('APP_URL')."/storage"."/". $path;
                //}
            }
          }

         // dd($attach);
         // $attach=array();

        if(isset($users)){
            foreach ($users as $user ){
                $user->notify(new FormRequestNotification($body,$attach));
            }
        }
        session()->flash('successMail', "La solicitud fue enviada con éxito!");
        return view('enviar-oportunidad-laboral');
    }
    public function enviarProyecto(Request $request)
    {
        $users=User::where('email','info@puentesocial.org')->get();
        $data=$request->except('_token','photos');
        $attach=array();
        $body['title']="Nuevo envío de Proyecto social";
        $body['subject']="PROYECTO SOCIAL";

        $body['data']['nombre']=$data['nombre'];
        $body['data']['mail']=$data['mail'];
        $body['data']['telefono']=$data['telefono'];
        $body['data']['organizacion']=$data['organizacion'];
        $body['data']['proyecto']=$data['proyecto'];

        $body['data']['ciudad']=$data['ciudad'];
        $body['data']['mensaje']=$data['texarea'];

        if ($request->hasFile('photos')) {
            $files = $request->file('photos');
            //$allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
                $nameFile=date('Y-m-d-H-m-s').".".trim($file->getClientOriginalExtension());
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $path = $file->storeAs('formsRequest/proyectos',$nameFile);
                  $attach[]=env('APP_URL')."/storage"."/". $path;
                //}
            }
          }

         // dd($attach);
         // $attach=array();

        if(isset($users)){
            foreach ($users as $user ){
                $user->notify(new FormRequestNotification($body,$attach));
            }
        }
        session()->flash('successMail', "La solicitud fue enviada con éxito!");
        return view('enviar-proyecto');
    }

    public function enviarContacto(Request $request)
    {
        $users=User::where('email','info@puentesocial.org')->get();
        $data=$request->except('_token','photos');
        $attach=array();
        $body['title']="Nuevo mensaje de CONTACTO";
        $body['subject']="CONTACTO";

        $body['data']['nombre']=$data['nombre'];
        $body['data']['mail']=$data['mail'];
        $body['data']['telefono']=$data['telefono'];
        $body['data']['asunto']=$data['asunto'];
        $body['data']['mensaje']=$data['texarea'];

     

         // dd($attach);
         // $attach=array();

        if(isset($users)){
            foreach ($users as $user ){
                $user->notify(new FormRequestNotification($body,$attach));
            }
        }
        session()->flash('successMail', "La solicitud fue enviada con éxito!");
        return view('enviar-proyecto');
    }
}
