<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Session;

use Laravel\Socialite\Facades\Socialite ;
use Str;
class SocialLoginController extends Controller
{
    public function redirectToProvider($provider) {
        switch ($provider) {
            case 'facebook':
                $provider_is_valid=true;
                break;
            case 'google':
                $provider_is_valid=true;
                break;
            default:
                break;
        }

        if ($provider_is_valid) {
            return Socialite::driver($provider)->redirect();

        }else{
            dd("sitio inexistente");
        }
    }

    public function handleProviderCallback($provider, Request $request){
        switch ($provider) {
            case 'facebook':
                $provider_is_valid=true;
                break;
            case 'google':
                $provider_is_valid=true;
                break;
            default:
                break;
        }

        if ($provider_is_valid) {
            $social_user_data = Socialite::driver($provider)->stateless()->user();

            if ($social_user = User::where('email', $social_user_data->email)->first()) {
                return $this->authAndRedirect($social_user);
            } else {
                    $role = 'user';
                    $date = date("Y-m-d g:i:s");
                    $user = User::create([
                        'name' => $social_user_data->name,
                        'email' => $social_user_data->email,
                        'photo' => $social_user_data->avatar,
                        'password' => Hash::make($social_user_data->id),
                        'provider' => $provider,
                        'email_verified_at' => $date
                    ]);

                    $fileContents = file_get_contents( $social_user_data->avatar);
                    $photo = $user->id. "_avatar.jpg";
                 
                    $path = public_path() .'/storage/users/'.$user->id.'/';
                    if (!file_exists($path)) {
                        mkdir($path);
                    }
                    File::put($path.$photo, $fileContents);
                   // $path = $file->storeAs('users/'.$user->id,$photo);  
                    $user->photo=$photo;
                    $user->save();
                   // if($role !='admin') $user->assignRole($role);
                    $user->assignRole('user');

                    return $this->authAndRedirect($user);
      
            }
        }else{
            dd("Url no válida");
        }
    }


    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
            'role' => 'required|string',

        ],
        [
            'email.required' => 'El campo email es obligatorio',
            'password.required' => 'El password es obligatorio',
            'role' => 'Error interno,no tiene permisos',

        ]
    );
    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {

        if($request->role=='admin')
        // Authentication passed...
        return redirect()->intended('dashboard');
    }

    }
  // Login y redirección
    public function authAndRedirect($user){
        Auth::login($user);
        return redirect()->intended('home');

    }


    public function AddRole(Request $request){
        $has_role = $request->input('role');
        $name_role = $has_role;
        if( $has_role=! NULL){
            $date = date("Y-m-d g:i:s");
            $user = User::create([
                'name' => session('name'),
                'email' => session('email'),
                'avatar' => session('avatar'),
                'password' => Hash::make(session('id')),
                'provider' => session('provider'),
                'puntaje' => 10,
                'email_verified_at' => $date
            ]);

            $request->session()->forget('name');
            $request->session()->forget('email');
            $request->session()->forget('avatar');
            $request->session()->forget('provider');

            if($name_role !='admin'){
                $user->assignRole($name_role);

            return $this->authAndRedirect($user); // Login y redirección

            }
        }else{
            return redirect('/register')
            ->with('error','Hubo un error al registrarte por favor vuelve a intentar'); // Login y redirección

        }

    }

}
