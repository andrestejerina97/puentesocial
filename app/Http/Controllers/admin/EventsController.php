<?php

namespace App\Http\Controllers\admin;

use App\event_file;
use App\events;
use App\Http\Controllers\Controller;
use App\publications;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    public function index($publication_id)
    {
        $publication=publications::find($publication_id); 
        $events=events::with('users')->with('publications')
        ->where('publications_id','=',$publication_id)->simplePaginate(15);
        $dates=events::where('publications_id','=',$publication_id)->select('date')->groupBy('date')->orderBy('date','Desc')->simplePaginate(10);
        return view('admin.events.index')
        ->with('publication',$publication)
        ->with('dates',$dates)
        ->with('events',$events)
        ->with('publications_id',$publication_id);
    }
    public function edit($publication_id,$event_id=0)
    {
    
        if ($event_id==0) {
        return view('admin.events.create')
        ->with('publications_id',$publication_id);  
          
        }else{

        $events=events::with('modifiedUsers')->with('users')->with('publications')->where('events.id','=',$event_id)->get();
        $files= event_file::where('events_id','=',$event_id)->get();
    
      return view('admin.events.edit')
      ->with('events',$events)
      ->with('files',$files)
      ->with('publications_id',$publication_id);  
        }
      
     }
     public function search($id,Request $request)
     {
       $users= User::where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();
       return view('admin.events.create')
       ->with('publications_id',$id)
       ->with('users',$users);     
    }
    public function searchevent($id,Request $request)
    {
      $events=events::join('users','events.users_id','users.id')
      ->where('publications_id','=',$id)
      ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('events.id','DESC')
      ->with('users')->with('publications')
      ->get();
      return view('admin.events.index')
      ->with('events',$events)
      ->with('publications_id',$id);
    }
    public function update(Request $request)
    {
      $event=events::find($request->input('id'));
      $publication_id=$request->input('publications_id');

      $event->date=$request->input('date');
      $event->description=$request->input('description');
      $event->modified_users_id=Auth::user()->id;
      $event->save();

      if ($request->hasFile('files')) {
        $files = $request->file('files');
        //$allowedfileExtension=['jpg','png','jpeg'];
        foreach($files as $file){
         $extension = $file->getClientOriginalExtension();
         // $check=in_array($extension,$allowedfileExtension);
          //if($check){
              $report_file= new event_file();
              $report_file->events_id=$event->id;
              $report_file->file="adjunto";
              $report_file->save();
              $report_file->file="adjunto"."-".$report_file->id.'.'.$extension;
              $report_file->save();
              $path = $file->storeAs('events/'.$event->id,$report_file->file);
            //}
        }
       }
       return response()->json(['result'=>$event->id]);

    }
    public function store(Request $request)
    {
      $publication_id=$request->input('publications_id');
   
      $event=new events();
      $event->date=$request->input('date');
      $event->description=$request->input('description');
      $event->users_id=Auth::user()->id;
      $event->modified_users_id=Auth::user()->id;
      $event->publications_id=$publication_id;
      $event->save();

      if ($request->hasFile('files')) {
        $files = $request->file('files');
        //$allowedfileExtension=['jpg','png','jpeg'];
        foreach($files as $file){
         $extension = $file->getClientOriginalExtension();
         // $check=in_array($extension,$allowedfileExtension);
          //if($check){
              $report_file= new event_file();
              $report_file->events_id=$event->id;
              $report_file->file="adjunto";
              $report_file->save();
              $report_file->file="adjunto"."-".$report_file->id.'.'.$extension;
              $report_file->save();
              $path = $file->storeAs('events/'.$event->id,$report_file->file);
            //}
        }
       }
      return response()->json(['result'=>$event->id]);
    }
    public function delete($id){
      if ($id != 'id') {
        $event= events::find($id);
        $event->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }

    public function deleteFile($id,Request $request){
        if ($id!= 0) {
          //$publication_images=publication_images::find($id);
          $report=event_file::find($request->input("id_event"));
  
          if ($request->has("id_event")) {
            Storage::disk('local')->delete('events/'.$id."/".$report->file);
            event_file::where("id",$request->input("id_event"))->delete();
           // $path = Storage::delete();
          }
  
          return response()->json(['result'=>1]);
  
        }
      }
  
}
