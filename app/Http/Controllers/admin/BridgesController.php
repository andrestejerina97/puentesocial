<?php


namespace App\Http\Controllers\admin;

use App\case_categories;
use App\case_has_categories;
use App\Http\Controllers\Controller;
use App\oportunity_categories;
use App\oportunity_has_categories;
use App\oportunity_images;
use App\proyect_categories;
use App\proyect_has_categories;
use App\publication_images;
use App\publication_reports;
use App\publications;
use App\User;
use Illuminate\Http\Request;

class BridgesController extends Controller
{
    public function index()
    {
      $publications= publications::with('category')->where('categories_id','=',4)->orderBy('id','DESC')->get();
      return view('admin.publicaciones.bridge',compact('publications'));
    }
    public function edit($id=0)
    {
      if($id==0){
       
       dd("Url incorrecta");   
      }else{
        $publication_user=publications::find($id);
        switch ($publication_user->last_category)
         {
          case 1:
            $case_categories= case_categories::all();
            $selected_categories= case_has_categories::where('publications_id','=',$id)->get();
            $photos= publication_images::where('publications_id','=',$id)->get();
            $publication= publications::where('id','=',$id)->firstOrFail();
            $created_users=User::where('id','=',$publication_user->users_id)->first();
            $modified_users=User::where('id','=',$publication_user->modified_users_id)->first();
            $reports= publication_reports::where('publications_id','=',$id)->get();
            return view('admin.publicaciones.view-bridge',compact('reports','publication','photos','case_categories','selected_categories','created_users','modified_users'));   

            break;
          case 2:
            $proyect_categories= proyect_categories::all();
            $selected_categories= proyect_has_categories::where('publications_id','=',$id)->get();
            $photos= publication_images::where('publications_id','=',$id)->get();
            $publication= publications::where('id','=',$id)->firstOrFail();
            $publication_user=publications::find($id);
            $created_users=User::where('id','=',$publication_user->users_id)->first();
            $modified_users=User::where('id','=',$publication_user->modified_users_id)->first();
            $reports= publication_reports::where('publications_id','=',$id)->get();
            return view('admin.publicaciones.view-bridge',compact('reports','publication','photos','proyect_categories','selected_categories','created_users','modified_users'));   

            break;
          case 3:
            $oportunity_categories= oportunity_categories::all();
            $selected_categories= oportunity_has_categories::where('publications_id','=',$id)->get();
            $photos= publication_images::where('publications_id','=',$id)->get();
            $publication= publications::where('id','=',$id)->firstOrFail();
            $publication_user=publications::find($id);
            $created_users=User::where('id','=',$publication_user->users_id)->first();
            $modified_users=User::where('id','=',$publication_user->modified_users_id)->first();
              $oportunityPhotos=oportunity_images::where('publications_id',$id)->orderBy('id','desc')->get();
            $reports= publication_reports::where('publications_id','=',$id)->get();
          return view('admin.publicaciones.view-bridge',compact('oportunityPhotos','reports','publication','photos','oportunity_categories','selected_categories','created_users','modified_users'));   
            break;
          default:
          abort(404);       
             break;
        }
       
      }
     }
    public function finalize($id)
    {
      if ($id != 'id') {
        $case= publications::find($id);
        

        if ($case->categories_id==4 ) {
          $case->categories_id=$case->last_category;
          $case->finalized=0;
  
        }else{
          
          $case->last_category=$case->categories_id;
          $case->categories_id=4;
          $case->finalized=1;
        }
        $case->save();

        return response()->json(['result'=>1]);

        }else{
          return response()->json(['result'=>-1]);
  
        }
  
    }

    public function search(Request $request)
    {
      $publications= publications::where('categories_id','=',4)->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();
      return view('admin.publicaciones.bridge',compact('publications'));
    }
}
