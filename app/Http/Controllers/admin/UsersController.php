<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Laravelista\Comments\Comment;
use Spatie\Permission\Models\Permission;

class UsersController extends Controller
{
    public function index()
    {
        $users=User::with('roles')->paginate(10);
        return view('admin.users.index')
        ->with('users',$users);
        
    }
    public function edit($user_id=0)
    {
    
        if ($user_id==0) {
        return view('admin.users.create');  
          
        }else{

        $users=User::with(['roles','permissions'])->where('id','=',$user_id)->get();
      return view('admin.users.edit')
      ->with('users',$users);
        }
      
     }
     public function search(Request $request)
     {
       if ($request->input('filter')=="role") {
        $roles=Role::all()->pluck('name')->toArray();
        if (in_array($request->input('search'),$roles)) {
          $users = User::role($request->input('search'))->get();
        }else{
          $users=User::with('roles')->simplePaginate(10);
        }
       

       }else{
        $users= User::where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();

       }
       return view('admin.users.index')
       ->with('users',$users);     
    }
    public function searchuser($id,Request $request)
    {
      $users=User::join('users','users.users_id','users.id')
      ->where('publications_id','=',$id)
      ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('users.id','DESC')
      ->with('users')->with('publications')
      ->get();
      return view('admin.users.index')
      ->with('users',$users)
      ->with('publications_id',$id);
    }
    public function update(Request $request)
    {
      $data= $request->except('_token','photo','role','password_confirmation','password');

      if ($request->input('password')=="") {
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
          'role' => ['required','string'],
        ],
        ['role.required' => 'Rol no seleccionado!']
      );

      }else{
 
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'role' => ['required','string'],
        ],
        ['role.required' => 'Rol no seleccionado!']

      );
        $data['password']=Hash::make($request->input('password'));
      }



      $user=User::find($request->input('id'));
      
      if (in_array($request->input('role'),Role::all()->pluck('name')->toArray())) {
        $user->syncRoles($request->input('role'));
        $roles=Role::findByName($request->input('role'))->permissions;
        $user->syncPermissions($roles->pluck('name'));
      }
      
      if ($request->hasFile('photo')) {
        $file = $request->file('photo');
        $allowedfileExtension=['jpg','png','jpeg'];
         $extension = $file->getClientOriginalExtension();
         $check=in_array($extension,$allowedfileExtension);
          if($check){
              $user->photo="foto"."-".$user->id.'.'.$extension;
              $user->save();
              $path = $file->storeAs('users/'.$user->id,$user->photo);
            }
          }
        
          $user=User::where('id','=',$request->input('id'))->update($data);
          return response()->json(['result'=>$request->input('id')]);

    }
 
    public function store(Request $request)
    {
      
     
      $validatedData = $request->validate([
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
        'role' => ['required','string'],
      ],
      ['role.required' => 'Rol no seleccionado!']

    );
      $data= $request->except('_token','photo','role');
      $data['provider']='email';
      $data['password']=Hash::make($request->input('password'));
      $user=User::create($data);
      
      if (in_array($request->input('role'),Role::all()->pluck('name')->toArray())) {

        if($request->input('role')=='admin' && Auth::user()->hasRole('admin') ){
          $user->assignRole('admin');
      }else{
        $user->assignRole($request->input('role'));


      }
      $roles=Role::findByName($request->input('role'))->permissions;

      $user->syncPermissions($roles->pluck('name'));
      }
     
      if ($request->hasFile('photo')) {
        $file = $request->file('photo');
        $allowedfileExtension=['jpg','png','jpeg'];
         $extension = $file->getClientOriginalExtension();
         $check=in_array($extension,$allowedfileExtension);
          if($check){
              $user->photo="foto"."-".$user->id.'.'.$extension;
              $user->save();
              $path = $file->storeAs('users/'.$user->id,$user->photo);
            }
        
       }

      return response()->json(['result'=>$user->id]);

    }
    public function delete($id){
 
     try {
      if ($id != 'id') {
        $user= User::find($id);
        $comments= Comment::where('commenter_id',$id)->get()->pluck('id');

        foreach ($comments as $key => $value) {
          if($value != 0){
            $commentAux=Comment::findOrFail($value);
            $commentAux->likes()->delete();
            $commentAux->delete();
          }
        }
       
        
        $user->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
     } catch (\Throwable $th) {
      return response()->json(['result'=>-1]);
    }
    }
    public function exportPdf(){
      $users=User::with('roles')->get();
      return view('reports.usuarios-pdf')
      ->with('users',$users);
    }

    public function exportExcel(){
      $users=User::with('roles')->get();
        return view('reports.usuarios-excel')
        ->with('users',$users);
    }
    public function seeder(){
      \Artisan::call('cache:clear');
      $roles=Role::all();
      $permissions=Permission::all();
      foreach ($roles as $role) {
          $role->syncPermissions($permissions->pluck('name'));
        
      }
    }



}
