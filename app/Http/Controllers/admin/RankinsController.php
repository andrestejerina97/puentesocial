<?php
namespace App\Http\Controllers\admin;

use App\donors;
use App\Http\Controllers\Controller;
use App\publications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RankinsController extends Controller
{
    public function index()
    {
        $donors=donors::with('users')->join('publications','publications.id','donors.publications_id')
        ->select('donors.users_id',DB::raw('sum(hearts) as hearts'))
        ->where('publications.categories_id','<>',3)
        ->groupBy('users_id')
        ->orderBy('hearts','DESC')->get();
       return view('admin.rankins.index',compact('donors'));
    }
    public function filter($id=0)
    {
        $publications=donors::with('publications')->join('publications','publications.id','donors.publications_id')
        ->where('donors.users_id','=',$id)
        ->where('publications.categories_id','<>',3)
        ->orderBy('donors.created_at','DESC')->get();
       return view('admin.rankins.publications',compact('publications'));
    }
    public function filterHelped($id=0)
    {
        $publications=donors::with('publications')->join('publications','publications.id','donors.publications_id')
        ->where('donors.users_id','=',$id)
        ->where('publications.categories_id',3)        
        ->orderBy('donors.created_at','DESC')->get();
       return view('admin.rankins.publications',compact('publications'));
    }

    public function helped()
    {
        $donors=donors::with('users')->join('publications','publications.id','donors.publications_id')
        ->select('donors.users_id',DB::raw('sum(hearts) as hearts'))
        ->where('publications.categories_id',3)        
        ->groupBy('users_id')
        ->orderBy('hearts','DESC')->get();
       return view('admin.rankins.helped',compact('donors'));
    }
}
