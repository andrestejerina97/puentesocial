<?php

namespace App\Http\Controllers\admin;

use App\case_categories;
use App\case_has_categories;
use App\Http\Controllers\Controller;
use App\publication_images;
use App\publication_reports;
use App\publications;
use App\User ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class PublicationsController extends Controller
{
   public function __construct()
   {
   // $this->middleware('auth');
   }
  //Son todas las publicaciones correspondientes a la categoria: casos
    public function index()
    {
      $publications= publications::where('categories_id','=',1)->orderBy('id','DESC')->get();
      return view('admin.publicaciones.case',compact('publications'));
    }
    public function edit($id=0)
    {
      if($id==0){
       
        $case_categories= case_categories::orderBy('order','asc')->get();
        return view('admin.publicaciones.edit-case',compact('case_categories'));   
      }else{
        $case_categories= case_categories::orderBy('order','asc')->get();
        $selected_categories= case_has_categories::where('publications_id','=',$id)->get();
        $photos= publication_images::where('publications_id','=',$id)->get();
        $publications= publications::where('id','=',$id)->get();
        $publication_user=publications::find($id);
        $created_users=User::where('id','=',$publication_user->users_id)->first();
        $modified_users=User::where('id','=',$publication_user->modified_users_id)->first();
        $reports= publication_reports::where('publications_id','=',$id)->get();
      return view('admin.publicaciones.edit-case',compact('reports','publications','photos','case_categories','selected_categories','created_users','modified_users'));   
      }
     }

     public function store(Request $request)
     {
      $validatedData = $request->validate([
        'code' => 'required|unique:publications|',
    ]);
       $inserts=$request->except("_token",'case_category','photos','reports');
       $inserts['categories_id']=1;
       $inserts['users_id']= Auth::user()->id;

      $publication= publications::create($inserts);

       $categories=$request->input('case_category');

       if($request->has('case_category')){
         for ($i=0; $i <count($categories) ; $i++) { 
          case_has_categories::insert([
            "case_categories_id"=>$categories[$i],
            "publications_id"=>$publication->id,
          ]);
         }
      }

       if ($request->hasFile('photos')) {
           $files = $request->file('photos');
           //$allowedfileExtension=['jpg','png','jpeg'];
           foreach($files as $file){
            $extension = $file->getClientOriginalExtension();
            // $check=in_array($extension,$allowedfileExtension);
             //if($check){
                 $report_file= new publication_images();
                 $report_file->publications_id=$publication->id;
                 $report_file->photo="mi-foto".'.'.$extension;
                 $report_file->save();
                 $report_file->photo="foto"."-".$report_file->id.'.'.$extension;
                 $report_file->save();
                 $path = $file->storeAs('publicaciones/'.$publication->id,$report_file->photo);
               //}
           }
          }

          if ($request->hasFile('reports')) {
            $files = $request->file('reports');
            //$allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
             $extension = $file->getClientOriginalExtension();
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $report_file= new publication_reports();
                  $report_file->publications_id=$publication->id;
                  $report_file->report="mi-informe".'.'.$extension;
                  $report_file->save();
                  $report_file->report="informe"."-".$report_file->id.'.'.$extension;
                  $report_file->save();
                  $path = $file->storeAs('informes/'.$publication->id,$report_file->report);
                //}
            }
          }

     return response()->json(['id_caso'=> $publication->id]);
    }
    public function update(Request $request)
    {

      $validatedData = $request->validate([
        'code' => ['required',Rule::unique('publications')->ignore($request->input('id'))],
      ]);
      $data=$request->except("_token",'case_category','photos','reports');
      $data['modified_users_id']= Auth::user()->id;
      // $publication= publications::find($request->input("id"));
       $publication= publications::where('id',$request->input('id'))->update($data);
      $id=$request->input('id');
     
       $categories=$request->input('case_category');

       if($request->has('case_category')){
         case_has_categories::where("publications_id",'=',$id)->delete();
         for ($i=0; $i <count($categories) ; $i++) { 
          case_has_categories::insert([
            "case_categories_id"=>$categories[$i],
            "publications_id"=>$id,
          ]);
         }
      }

       if ($request->hasFile('photos')) {
           $files = $request->file('photos');
           //$allowedfileExtension=['jpg','png','jpeg'];
          
           foreach($files as $file){
               $extension = $file->getClientOriginalExtension();
              // $check=in_array($extension,$allowedfileExtension);
               //if($check){
                   $report_file= new publication_images();
                   $report_file->publications_id=$id;
                   $report_file->photo="mi-foto".'.'.$extension;
                   $report_file->save();
                   $report_file->photo="foto"."-".$report_file->id.'.'.$extension;
                   $report_file->save();
                   $path = $file->storeAs('publicaciones/'.$id,$report_file->photo);
               //}
              }
    }
    if ($request->hasFile('reports')) {
      $files = $request->file('reports');
      //$allowedfileExtension=['jpg','png','jpeg'];
      foreach($files as $file){
       $extension = $file->getClientOriginalExtension();
       // $check=in_array($extension,$allowedfileExtension);
        //if($check){
            $report_file= new publication_reports();
            $report_file->publications_id=$id;
            $report_file->report="mi-informe".'.'.$extension;
            $report_file->save();
            $report_file->report="informe"."-".$report_file->id.'.'.$extension;
            $report_file->save();
            $path = $file->storeAs('informes/'.$id,$report_file->report);
          //}
      }
    }

    return response()->json(['id_caso'=> $id]);

  }

    public function delete($id){
      if ($id != 'id') {
        $case= publications::find($id);
        $case->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }
    public function deleteReport($id,Request $request){
      if ($id!= 0) {
        //$publication_images=publication_images::find($id);
        $report=publication_reports::find($request->input("id_report"));

        if ($request->has("id_report")) {
          Storage::disk('local')->delete('informes/'.$id."/".$report->report);
          publication_reports::where("id",$request->input("id_report"))->delete();

         // $path = Storage::delete();
        }

        return response()->json(['result'=>1]);

      }
    }

    public function active($id)
    {
      if ($id != 'id') {
      $case= publications::find($id);
      if ($case->active==1) {
        $case->active=0;
        $case->update();
        return response()->json(['result'=>0,"message"=>"Desactivado"]);

      }else{
        $case->active=1;
        $case->update();
        return response()->json(['result'=>1,"message"=>"Publicado"]);

      }

      }else{
        return response()->json(['result'=>-1]);

      }

    }
    public function activePhoto($id=0,Request $request)
    {
        if ($id!= 0) {
          $publication=publications::find($id);
          $publication->avatar=$request->input("photo");
          $publication->save();
          return response()->json(['result'=>$publication->save()]);
  
        }
    }
    public function deletePhoto($id=0,Request $request)
    {
        if ($id!= 0) {
          $photo=$request->input("photo");
          //$publication_images=publication_images::find($id);
          $publication=publications::find($id);
          if ($publication->avatar==$photo) {
            $publication->avatar="";
            $publication->save();
          }
          if ($request->has("id_photo")) {
            publication_images::where("id",$request->input("id_photo"))->delete();

           // $path = Storage::delete();
            Storage::disk('local')->delete('publicaciones/'.$id."/".$photo);
          }

          return response()->json(['result'=>1]);
  
        }
    }

    public function finalize($id)
    {
      if ($id != 'id') {
        $case= publications::find($id);
        

        if ($case->categories_id==4 ) {
          $case->categories_id=$case->last_category;
          $case->finalized=0;
  
        }else{
          
          $case->last_category=$case->categories_id;
          $case->categories_id=4;
          $case->finalized=1;
        }
        $case->save();

        return response()->json(['result'=>1]);

        }else{
          return response()->json(['result'=>-1]);
  
        }
  
    }

    public function search(Request $request)
    {
      $publications= publications::where('categories_id','=',1)->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();
      return view('admin.publicaciones.case',compact('publications'));
    }
    public function qrDowloaded(Request $request)
    {
      $publication= publications::find($request->publication);
      $publication->qr=true;
      $publication->save();
      return true;
    }
}
