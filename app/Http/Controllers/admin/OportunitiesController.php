<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\oportunities;
use App\oportunity_categories;
use App\oportunity_has_categories;
use App\oportunity_images;
use App\publication_images;
use App\publication_reports;
use App\publications;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class OportunitiesController extends Controller
{
  public function __construct()
  {
  // $this->middleware('auth');
  }
 //Son todas las publicaciones correspondientes a la categoria: oportunitys
   public function index()
   {
     $publications= publications::where('categories_id','=',3)->orderBy('id','DESC')->get();
     return view('admin.publicaciones.oportunity',compact('publications'));
   }
   public function edit($id=0)
   {
     if($id==0){
      
       $oportunity_categories= oportunity_categories::orderBy('order','asc')->get();
       return view('admin.publicaciones.edit-oportunity',compact('oportunity_categories'));   
     }else{
       $oportunity_categories= oportunity_categories::orderBy('order','asc')->get();
       $selected_categories= oportunity_has_categories::where('publications_id','=',$id)->get();
       $photos= publication_images::where('publications_id','=',$id)->get();
       $publications= publications::where('id','=',$id)->get();
       $publication_user=publications::find($id);
       $created_users=User::where('id','=',$publication_user->users_id)->first();
       $modified_users=User::where('id','=',$publication_user->modified_users_id)->first();
        $oportunityPhotos=oportunity_images::where('publications_id',$id)->orderBy('id','desc')->get();
       $reports= publication_reports::where('publications_id','=',$id)->get();
     return view('admin.publicaciones.edit-oportunity',compact('oportunityPhotos','reports','publications','photos','oportunity_categories','selected_categories','created_users','modified_users'));   
     }
    }

    public function store(Request $request)
    {
     $validatedData = $request->validate([
       'code' => 'required|unique:publications|',
      ]);

      $inserts=$request->except("_token",'oportunity_category','photos','reports','oportunityPhotos');
      $inserts['categories_id']=3;
      $inserts['users_id']= Auth::user()->id;

     $publication= publications::create($inserts);

      $categories=$request->input('oportunity_category');

      if($request->has('oportunity_category')){
        for ($i=0; $i <count($categories) ; $i++) { 
         oportunity_has_categories::insert([
           "oportunity_categories_id"=>$categories[$i],
           "publications_id"=>$publication->id,
         ]);
        }
     }

      if ($request->hasFile('photos')) {
          $files = $request->file('photos');
          //$allowedfileExtension=['jpg','png','jpeg'];
          foreach($files as $file){
           $extension = $file->getClientOriginalExtension();
           // $check=in_array($extension,$allowedfileExtension);
            //if($check){
                $report_file= new publication_images();
                $report_file->publications_id=$publication->id;
                $report_file->photo="mi-foto".'.'.$extension;
                $report_file->save();
                $report_file->photo="foto"."-".$report_file->id.'.'.$extension;
                $report_file->save();
                $path = $file->storeAs('publicaciones/'.$publication->id,$report_file->photo);
              //}
          }
         }

         if ($request->hasFile('reports')) {
           $files = $request->file('reports');
           //$allowedfileExtension=['jpg','png','jpeg'];
           foreach($files as $file){
            $extension = $file->getClientOriginalExtension();
            // $check=in_array($extension,$allowedfileExtension);
             //if($check){
                 $report_file= new publication_reports();
                 $report_file->publications_id=$publication->id;
                 $report_file->report="mi-informe".'.'.$extension;
                 $report_file->save();
                 $report_file->report="informe"."-".$report_file->id.'.'.$extension;
                 $report_file->save();
                 $path = $file->storeAs('informes/'.$publication->id,$report_file->report);
               //}
           }
         }

         if ($request->hasFile('oportunityPhotos')) {
          $files = $request->file('oportunityPhotos');
          //$allowedfileExtension=['jpg','png','jpeg'];
          foreach($files as $file){
           $extension = $file->getClientOriginalExtension();
           // $check=in_array($extension,$allowedfileExtension);
            //if($check){
                $report_file= new oportunity_images();
                $report_file->publications_id=$publication->id;
                $report_file->photo="mi-foto".'.'.$extension;
                $report_file->save();
                $report_file->photo="foto"."-".$report_file->id.'.'.$extension;
                $report_file->save();
                $path = $file->storeAs('oportunities/'.$publication->id,$report_file->photo);
              //}
          }
        }

    return response()->json(['id_oportunity'=> $publication->id]);
   }
   public function update(Request $request)
   {

     $validatedData = $request->validate([
       'code' => ['required',Rule::unique('publications')->ignore($request->input('id'))],
     ]);
     $data=$request->except("_token",'oportunity_category','photos','reports','oportunityPhotos');
     $data['modified_users_id']= Auth::user()->id;
     // $publication= publications::find($request->input("id"));
      $publication= publications::where('id',$request->input('id'))->update($data);
     $id=$request->input('id');
    
      $categories=$request->input('oportunity_category');

      if($request->has('oportunity_category')){
        oportunity_has_categories::where("publications_id",'=',$id)->delete();
        for ($i=0; $i <count($categories) ; $i++) { 
         oportunity_has_categories::insert([
           "oportunity_categories_id"=>$categories[$i],
           "publications_id"=>$id,
         ]);
        }
     }

      if ($request->hasFile('photos')) {
          $files = $request->file('photos');
          //$allowedfileExtension=['jpg','png','jpeg'];
         
          foreach($files as $file){
              $extension = $file->getClientOriginalExtension();
             // $check=in_array($extension,$allowedfileExtension);
              //if($check){
                  $report_file= new publication_images();
                  $report_file->publications_id=$id;
                  $report_file->photo="mi-foto".'.'.$extension;
                  $report_file->save();
                  $report_file->photo="foto"."-".$report_file->id.'.'.$extension;
                  $report_file->save();
                  $path = $file->storeAs('publicaciones/'.$id,$report_file->photo);
              //}
             }
   }
   if ($request->hasFile('reports')) {
     $files = $request->file('reports');
     //$allowedfileExtension=['jpg','png','jpeg'];
     foreach($files as $file){
      $extension = $file->getClientOriginalExtension();
      // $check=in_array($extension,$allowedfileExtension);
       //if($check){
           $report_file= new publication_reports();
           $report_file->publications_id=$id;
           $report_file->report="mi-informe".'.'.$extension;
           $report_file->save();
           $report_file->report="informe"."-".$report_file->id.'.'.$extension;
           $report_file->save();
           $path = $file->storeAs('informes/'.$id,$report_file->report);
         //}
     }
   }
   if ($request->hasFile('oportunityPhotos')) {
    $files = $request->file('oportunityPhotos');
    //$allowedfileExtension=['jpg','png','jpeg'];
    foreach($files as $file){
     $extension = $file->getClientOriginalExtension();
     // $check=in_array($extension,$allowedfileExtension);
      //if($check){
          $report_file= new oportunity_images();
          $report_file->publications_id=$id;
          $report_file->photo="mi-foto".'.'.$extension;
          $report_file->save();
          $report_file->photo="foto"."-".$report_file->id.'.'.$extension;
          $report_file->save();
          $path = $file->storeAs('oportunities/'.$id,$report_file->photo);
        //}
    }
  }

   return response()->json(['id_oportunity'=> $id]);

 }

   public function delete($id){
     if ($id != 'id') {
       $oportunity= publications::find($id);
       $oportunity->delete();
       return response()->json(['result'=>1]);
     }else{
       return response()->json(['result'=>-1]);

     }
   }
   public function deleteReport($id,Request $request){
     if ($id!= 0) {
       //$publication_images=publication_images::find($id);
       $report=publication_reports::find($request->input("id_report"));

       if ($request->has("id_report")) {
         Storage::disk('local')->delete('informes/'.$id."/".$report->report);
         publication_reports::where("id",$request->input("id_report"))->delete();

        // $path = Storage::delete();
       }

       return response()->json(['result'=>1]);

     }
   }
   public function deletePhotoInternal($id,Request $request){
    if ($id!= 0) {
      //$publication_images=publication_images::find($id);
      $report=oportunity_images::find($request->input("id_report"));

      if ($request->has("id_report")) {
        Storage::disk('local')->delete('oportunities/'.$id."/".$report->photo);
        oportunity_images::where("id",$request->input("id_report"))->delete();

       // $path = Storage::delete();
      }

      return response()->json(['result'=>1]);

    }
  }
   public function active($id)
   {
     if ($id != 'id') {
     $oportunity= publications::find($id);
     if ($oportunity->active==1) {
       $oportunity->active=0;
       $oportunity->update();
       return response()->json(['result'=>0,"message"=>"Desactivada"]);

     }else{
       $oportunity->active=1;
       $oportunity->update();
       return response()->json(['result'=>1,"message"=>"Publicada"]);

     }

     }else{
       return response()->json(['result'=>-1]);

     }

   }
   public function activePhoto($id=0,Request $request)
   {
       if ($id!= 0) {
         $publication=publications::find($id);
         $publication->avatar=$request->input("photo");
         $publication->save();
         return response()->json(['result'=>$publication->save()]);
 
       }
   }
   public function deletePhoto($id=0,Request $request)
   {
       if ($id!= 0) {
         $photo=$request->input("photo");
         //$publication_images=publication_images::find($id);
         $publication=publications::find($id);
         if ($publication->avatar==$photo) {
           $publication->avatar="";
           $publication->save();
         }
         if ($request->has("id_photo")) {
           publication_images::where("id",$request->input("id_photo"))->delete();

          // $path = Storage::delete();
           Storage::disk('local')->delete('publicaciones/'.$id."/".$photo);
         }

         return response()->json(['result'=>1]);
 
       }
   }

   public function finalize($id)
   {
     if (is_numeric($id)) {
       $oportunity= publications::find($id);
       

       if ($oportunity->categories_id==4 ) {
         $oportunity->categories_id=3;
         $oportunity->finalized=0;
 
       }else{
        $oportunity->last_category=$oportunity->categories_id;
         $oportunity->categories_id=4;
         $oportunity->finalized=1;
       }
       $oportunity->save();

       return response()->json(['result'=>1]);

       }else{
         return response()->json(['result'=>-1]);
 
       }
 
   }

   public function search(Request $request)
   {
     $publications= publications::where('categories_id','=',3)->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();
     return view('admin.publicaciones.oportunity',compact('publications'));
   }
}
