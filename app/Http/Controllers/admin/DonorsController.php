<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;


use App\donors;
use App\publications;
use App\User;
use Illuminate\Validation\Rule;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule as ValidationRule;

class DonorsController extends Controller
{
    public function index($publication_id)
    {
        $donors=donors::with('users')->with('publications')
        ->where('publications_id','=',$publication_id)->simplePaginate(10);
        $publications=publications::where('id','=',$publication_id)->get();
        return view('admin.donors.index')
        ->with('donors',$donors)
        ->with('publications',$publications)
        ->with('publications_id',$publication_id);
    }

    public function edit($publication_id,$donor_id=0)
    {
    
        if ($donor_id==0) {
        return view('admin.donors.create')
        ->with('publications_id',$publication_id);  
          
        }else{

        $donors=donors::with('users')->with('publications')->where('donors.id','=',$donor_id)->get();

      return view('admin.donors.edit')
      ->with('donors',$donors)
      ->with('publications_id',$publication_id);  
        }
      
     }
     public function search($id,Request $request)
     {
       $users= User::where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();
       return view('admin.donors.create')
       ->with('publications_id',$id)
       ->with('users',$users);     
    }
    public function searchDonor($id,Request $request)
    {
      $donors=donors::join('users','donors.users_id','users.id')
      ->where('publications_id','=',$id)
      ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('donors.id','DESC')
      ->with('users')->with('publications')
      ->get();
      $publications=publications::where('id','=',$id)->get();

      return view('admin.donors.index')
      ->with('donors',$donors)
      ->with('publications',$publications)
      ->with('publications_id',$id);
    }
    public function update(Request $request)
    {
      $donor=donors::find($request->input('id'));
      $donor->hearts=$request->input('hearts');
      $donor->observations=$request->input('observations');
      $donor->save();
      return response()->json(['result'=>$donor->id]);


    }
    public function store(Request $request)
    {
      $publication_id=$request->input('publications_id');
      $exist=donors::where("publications_id",'=',$publication_id)
      ->where('users_id','=',$request->input('users_id'))->get()->count();
      if($exist>0){
        return response()->json(['result'=>NULL,"message"=>"El usuario ingresado ya está registrado como donante"]);
      }
      $donor=new donors();
      $donor->hearts=$request->input('hearts');
      $donor->observations=$request->input('observations');
      $donor->users_id=$request->input('users_id');
      $donor->publications_id=$publication_id;
      $donor->save();
      return response()->json(['result'=>$donor->id]);
    }
    public function delete($id){
      if ($id != 'id') {
        $donor= donors::find($id);
        $donor->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }
    public function updatePublications(Request $request)
    {
      dd($request->input('link_donar_online'));
      $publication= publications::find($request->input('id'));
      $publication->link_paypal=$request->input('link_paypal');
      $publication->link_mercado_pago=$request->input('link_mercado_pago');
      $publication->link_donar_online=$request->input('link_donar_online');
      $publication->progress=$request->input('progress');
      $publication->save();
      $id=$request->input('id');
     
    return response()->json(['result'=> $id]);

  }
}
