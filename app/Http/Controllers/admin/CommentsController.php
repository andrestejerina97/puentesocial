<?php


namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\publications;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Laravelista\Comments\Comment;

class CommentsController extends Controller
{
   public function index()
   {
      $publications= Comment::with('commentable')->select(DB::raw('COUNT(*) AS unread,commentable_id,commentable_type'))->where('readed',0)->groupBy('commentable_id','commentable_type')->paginate(10);
      //publications::with('comments')->with(['unreadedComments'=> function ($query) {
        // $query->where('comments.readed','0');
     //}])->with('category')->orderBy('id','DESC')->paginate(8);
      return view('admin.comments.index',compact('publications'));
   }
    public function addLike(Request $request)
         {
             $id=$request->comment;
             $comment=Comment::find($id);
             if($comment->liked()){
                $comment->unlike();
             }else{
                $comment->like();
             }
        
             return response()->json(['result'=> $comment->likeCount]);
         }

   public function view($id=0)
   {
      if($id>0){
         $publication= publications::with(['comments.children'=> function ($query) {
            $query->orderBy('comments.created_at','desc');
        }])->where('id',$id)->firstOrFail();
        $update=Comment::where('commentable_id',$id)->update(['readed'=>1]);

        return view('admin.comments.view',compact('publication'));

      }else{
         abort(404);
      }
   }
   public function delete($comment)
   {     $comment= Comment::where('id',$comment)->firstOrFail();
        if (Config::get('comments.soft_deletes') == true) {
			$comment->delete();
		}
		else {
			$comment->forceDelete();
		}
      return response()->json(['result'=> 1]);

   }

   public function search(Request $request)
   {
      $publications= Comment::whereHasMorph('commentable','App\publications',function(Builder $query, $type) use ($request) {
         $query->where($request->input('filter'),'LIKE','%'.$request->input('search')."%");
      })->select(DB::raw('COUNT(*) AS readed,commentable_id,commentable_type'))->groupBy('commentable_id','commentable_type')->get();
      // $publications= publications::with('comments')->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('id','DESC')->get();
     return view('admin.comments.index',compact('publications'));
   }
}
