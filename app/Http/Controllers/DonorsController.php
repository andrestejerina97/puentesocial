<?php

namespace App\Http\Controllers;

use App\donors;
use Illuminate\Http\Request;

class DonorsController extends Controller
{
    public function index()
    {
        $donors=donors::all();
        return view('admin.donors.index',compact('donors'));
    }
}
