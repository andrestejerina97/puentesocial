<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //uso un solo middleware para comprobar los roles permitidos
    if(\Auth::check()){
        if (isAdmin()|| isBusiness()||isEdit()) {
			return $next($request);
		} else {
			return redirect()->route('home');
		}
    }else{
        return redirect()->route('loginAdmin');

    }
    }
}
