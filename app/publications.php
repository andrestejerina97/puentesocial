<?php

namespace App;

use Conner\Likeable\Likeable;
use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class publications extends Model
{
    use Commentable,Likeable;
    protected $guarded = ['id','active','created_at'];
    public function images()
    {
        return $this->hasMany("App\publication_images");
    }
    public function subcases()
    {
        return $this->hasMany("App\case_has_categories",'publications_id','id');
    }
    public function subproyects()
    {
        return $this->hasMany("App\proyect_has_categories",'publications_id','id');
    } 
    public function suboportunities()
    {
        return $this->hasMany("App\oportunity_has_categories",'publications_id','id');
    } 
    public function category()
    {
        return $this->belongsTo("App\categories",'categories_id','id');
    }
     public function lastCategory()
    {
        return $this->belongsTo("App\categories",'last_category');
    }
    
    public function donors()
    {
        return $this->hasMany('App\donors');
    }

 

}
