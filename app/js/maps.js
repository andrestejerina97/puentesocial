
var latitud=$('#latitud').val();
var longitud=$('#longitud').val();

  mapboxgl.accessToken = 'pk.eyJ1IjoiZmFraHJhd3kiLCJhIjoiY2pscWs4OTNrMmd5ZTNra21iZmRvdTFkOCJ9.15TZ2NtGk_AtUvLd27-8xA';
  var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v11',
  center: [longitud,latitud],
  zoom: 15
  });
 
  var marker = new mapboxgl.Marker({color:'orange'})
  .setLngLat([longitud,latitud])
  .addTo(map);
     
