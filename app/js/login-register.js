  
  $("#login").submit(function(e) {
    e.preventDefault();
    $("#Sesion").attr('disabled',true);
    $("#Sesion").html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>`);

  $.ajax({
    url: this.action,
    data:{email:$("#emailUser").val(),password:$("#passUser").val(),  _token: token},
    type: "POST",
    success: function(response){

      if (response==1) {
        window.location.replace(redirect);
      }else{
        location.reload();
      }
      },
       error : function(data) {
         console.log(data);
         if( data.status === 422 ) {
          $("#Sesion").attr('disabled',false);
          $("#Sesion").html(`Iniciar Sesión`);
            var errors = $.parseJSON(data.responseText);
            //console.log(errors.message)
            if (errors.message=="The given data was invalid.") {
              $("#messageError").html('<small  class="form-text text-danger">Los datos proporcionados no son correctos.Verifique el email o la contraseña.</small>')
            }
          }
      }
    });

  }); 

   $("#register").submit(function(e) {
    e.preventDefault();
    $("#Registrar").attr('disabled',true);
    $("#Registrar").html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>`);
    var role=2;
    var name=$("#registerName").val();
    var email=$("#registerEmail").val();
    var pass=$("#registerPassword").val();
    var pass_confirm=$("#registerPasswordConfirm").val();
    var agente=$("#agente").prop('checked');
    if (agente) {
      role=3;
    }
  $.ajax({
    url: this.action,
    data:{name:name,email:email,password:pass,password_confirmation:pass_confirm,role_id:role,  _token: token},
    type: "POST",
    success: function(response){

//console.log(response)
location.reload();
      },
       error : function(data) {
         //console.log(data);
        if( data.status === 422 ) {
          $("#Registrar").attr('disabled',false);
            var errors = $.parseJSON(data.responseText);
            //console.log(errors)
           


           if (errors.errors.email) {
            $("#messageExistEmail").html('<small  class="form-text text-danger">El email ya ha sido registrado.</small>');
            $("#Registrar").html(`Registrate`);
            }

            if (errors.errors.password) {

              for (mensaje in errors.errors.password){
  //console.log(errors.errors.password[mensaje])

  if (errors.errors.password[mensaje]=="The password must be at least 8 characters.") {
    $("#messageLongPass").html('<small  class="form-text text-danger">La contraseña debe tener al menos 8 caracteres.</small>');
    $("#Registrar").html(`Registrate`);
  }
  if (errors.errors.password[mensaje]=="The password confirmation does not match.") {
    $("#messageConfirmPass").html('<small  class="form-text text-danger">La confirmación de contraseña no coincide.</small>');
    $("#Registrar").html(`Registrate`);
  }


              }
            }

          }
      }
    });

  });