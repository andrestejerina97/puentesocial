<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class donors extends Model
{
    public function publications()
    {
        return $this->belongsTo('App\publications');
    }
    public function users()
    {
        return $this->belongsTo('App\user');
    }
}
