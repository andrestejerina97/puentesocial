<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DonacionMaterialesNotifications extends Notification
{
    use Queueable;
    protected $solicitud;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->greeting('Nueva solicitud de donación!')
                ->subject('Donación de materiales!')
                ->line('DETALLES DE DONACIÓN: ',$this->solicitud['texarea'])
                ->line('DIRECCIÓN: ',$this->solicitud['direccion'])
                ->line('CIUDAD: ',$this->solicitud['ciudad'])
                ->line('PROVINCIA: ',$this->solicitud['provincia'])
                ->line('EMAIL: ',$this->solicitud['mail'])
                ->line('TELÉFONO: ',$this->solicitud['telefono']);

            }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
