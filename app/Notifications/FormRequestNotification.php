<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FormRequestNotification extends Notification
{
    use Queueable;
    protected $body;
    protected $attachments;
    protected $cc;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($body,$attachemnts,$cc="marcelo@puentesocial.org")
    {
        $this->body = $body;
        $this->attachments = $attachemnts;
        $this->cc=$cc;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail= new MailMessage();
        $mail->greeting($this->body['title'])
        ->subject($this->body['subject']);
        $mail->cc($this->cc);
        foreach($this->body['data'] as $key=>$value){
            $mail->line(strtoupper($key)." : ".$value);
        }
        $i=1;

        foreach($this->attachments as $attachemnt){
            $mail->attach($attachemnt);
                
        }
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
