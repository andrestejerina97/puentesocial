<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class events extends Model
{
    public function publications()
    {
        return $this->belongsTo('App\publications');
    }
    public function users()
    {
        return $this->belongsTo('App\user');
    }
    public function modifiedUsers()
    {
        return $this->belongsTo('App\user');
    }
}
